﻿Option Strict Off
Imports IDAutomation.Windows.Forms.LinearBarCode
Imports System.IO

Public Class frmCreteService

    Inherits System.Windows.Forms.Form
    'This line was added to allow printing as a DLL
    'Dim cust As service
    Dim NewBarcode As IDAutomation.Windows.Forms.LinearBarCode.Barcode = New Barcode()

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtDataToEncode As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Barcode1 As IDAutomation.Windows.Forms.LinearBarCode.Barcode
    Friend WithEvents btnSaveJPEG As System.Windows.Forms.Button

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtDataToEncode = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnExit = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnSaveJPEG = New System.Windows.Forms.Button
        Me.Barcode1 = New IDAutomation.Windows.Forms.LinearBarCode.Barcode
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtComent = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPrice = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'txtDataToEncode
        '
        Me.txtDataToEncode.Enabled = False
        Me.txtDataToEncode.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDataToEncode.Location = New System.Drawing.Point(12, 93)
        Me.txtDataToEncode.Name = "txtDataToEncode"
        Me.txtDataToEncode.Size = New System.Drawing.Size(143, 22)
        Me.txtDataToEncode.TabIndex = 1
        Me.txtDataToEncode.WordWrap = False
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 23)
        Me.Label1.TabIndex = 25
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(230, 296)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(128, 55)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "Cancel"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(0, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 23)
        Me.Label4.TabIndex = 26
        '
        'btnSaveJPEG
        '
        Me.btnSaveJPEG.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveJPEG.Location = New System.Drawing.Point(364, 296)
        Me.btnSaveJPEG.Name = "btnSaveJPEG"
        Me.btnSaveJPEG.Size = New System.Drawing.Size(120, 55)
        Me.btnSaveJPEG.TabIndex = 24
        Me.btnSaveJPEG.Text = "Generate Report"
        '
        'Barcode1
        '
        Me.Barcode1.ApplyTilde = True
        Me.Barcode1.BackColor = System.Drawing.Color.White
        Me.Barcode1.BarHeightCM = "1.000"
        Me.Barcode1.BearerBarHorizontal = "0"
        Me.Barcode1.BearerBarVertical = "0"
        Me.Barcode1.CaptionAbove = ""
        Me.Barcode1.CaptionBelow = ""
        Me.Barcode1.CaptionBottomAlignment = System.Drawing.StringAlignment.Center
        Me.Barcode1.CaptionBottomColor = System.Drawing.Color.Black
        Me.Barcode1.CaptionBottomSpace = "0.10"
        Me.Barcode1.CaptionFontAbove = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Barcode1.CaptionFontBelow = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Barcode1.CaptionTopAlignment = System.Drawing.StringAlignment.Center
        Me.Barcode1.CaptionTopColor = System.Drawing.Color.Black
        Me.Barcode1.CaptionTopSpace = "0.10"
        Me.Barcode1.CharacterGrouping = "0"
        Me.Barcode1.CheckCharacter = False
        Me.Barcode1.CheckCharacterInText = True
        Me.Barcode1.CODABARStartChar = "A"
        Me.Barcode1.CODABARStopChar = "B"
        Me.Barcode1.Code128Set = IDAutomation.Windows.Forms.LinearBarCode.Barcode.Code128CharacterSets.[Auto]
        Me.Barcode1.DataToEncode = "12345678"
        Me.Barcode1.DoPaint = True
        Me.Barcode1.FitControlToBarcode = True
        Me.Barcode1.ForeColor = System.Drawing.Color.Black
        Me.Barcode1.LeftMarginCM = "0.200"
        Me.Barcode1.Location = New System.Drawing.Point(12, 12)
        Me.Barcode1.Name = "Barcode1"
        Me.Barcode1.NarrowToWideRatio = "2.0"
        Me.Barcode1.PostnetHeightShort = "0.1270"
        Me.Barcode1.PostnetHeightTall = "0.3226"
        Me.Barcode1.PostnetSpacing = "0.066"
        Me.Barcode1.Resolution = IDAutomation.Windows.Forms.LinearBarCode.Barcode.Resolutions.Printer
        Me.Barcode1.ResolutionCustomDPI = "600.00"
        Me.Barcode1.ResolutionPrinterToUse = ""
        Me.Barcode1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Barcode1.RotationAngle = IDAutomation.Windows.Forms.LinearBarCode.Barcode.RotationAngles.Zero_Degrees
        Me.Barcode1.ShowText = True
        Me.Barcode1.ShowTextLocation = IDAutomation.Windows.Forms.LinearBarCode.Barcode.HRTextPositions.Bottom
        Me.Barcode1.Size = New System.Drawing.Size(143, 75)
        Me.Barcode1.SuppSeparationCM = "0.350"
        Me.Barcode1.SymbologyID = IDAutomation.Windows.Forms.LinearBarCode.Barcode.Symbologies.Code39
        Me.Barcode1.TabIndex = 20
        Me.Barcode1.TextFontColor = System.Drawing.Color.Black
        Me.Barcode1.TextMarginCM = "0.100"
        Me.Barcode1.TopMarginCM = "0.200"
        Me.Barcode1.UPCESystem = "1"
        Me.Barcode1.WhiteBarIncrease = "0"
        Me.Barcode1.XDimensionCM = "0.0400"
        Me.Barcode1.XDimensionMILS = "15.7480"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 132)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(276, 19)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Enter Problem, Ask Customer for Password"
        '
        'txtComent
        '
        Me.txtComent.Location = New System.Drawing.Point(13, 160)
        Me.txtComent.Name = "txtComent"
        Me.txtComent.Size = New System.Drawing.Size(470, 26)
        Me.txtComent.TabIndex = 28
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 206)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 19)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Price"
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(64, 206)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(78, 26)
        Me.txtPrice.TabIndex = 30
        '
        'frmCreteService
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(7, 19)
        Me.ClientSize = New System.Drawing.Size(510, 363)
        Me.Controls.Add(Me.txtPrice)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtComent)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnSaveJPEG)
        Me.Controls.Add(Me.Barcode1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtDataToEncode)
        Me.Controls.Add(Me.btnExit)
        Me.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCreteService"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CustomerReport"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtDataToEncode.Text = frmService.customer.barcode


    End Sub
    Private Sub cmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub




    Private Sub SaveJPEG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveJPEG.Click
        Barcode1.DataToEncode = txtDataToEncode.Text
        Barcode1.Resolution = Barcode.Resolutions.Custom
        'Here we make this 96 DPI, the resolution of the web browser
        Barcode1.ResolutionCustomDPI = 150
        Barcode1.SaveImageAs("SavedBarcode.Jpeg", System.Drawing.Imaging.ImageFormat.Jpeg)
        'Reset the resolution source to the printer (in case we print a barcode)
        Barcode1.Resolution = Barcode.Resolutions.Printer



        'Dim dt As New DataTable()
        'Dim drow As DataRow
        'dt.Columns.Add("SavedBarcode.Jpeg", System.Type.GetType("System.Byte[]"))
        'drow = dt.NewRow
        Dim fs As FileStream
        Dim br As BinaryReader
        fs = New FileStream(AppDomain.CurrentDomain.BaseDirectory + "SavedBarcode.Jpeg", FileMode.Open)
        br = New BinaryReader(fs)
        Dim imgbyte(fs.Length) As Byte
        ''dim imgbyte [] as  byte
        imgbyte = br.ReadBytes(Convert.ToInt32((fs.Length)))

        frmService.customer.picture = imgbyte
        'drow(0) = imgbyte       ' add the image in bytearray
        'dt.Rows.Add(drow)       ' add row into the datatable
        br.Close()              ' close the binary reader
        fs.Close()              ' close the file stream
        'Dim rptobj As New CrystalReport1    ' object of crystal report
        'rptobj.SetDataSource(dt)            ' set the datasource of crystalreport object

        ''CrystalReportViewer1.ReportSource = rptobj  'set the report source
        'Dim ds As New DataSet1
        'Dim t As DataTable = ds.Tables.Add("DataTable1")
        't.Columns.Add("picture", Type.GetType("System.Byte[]"))
        'Dim r As DataRow
        'r = t.NewRow()
        'r("picture") = imgbyte
        'rptobj.SetDataSource(ds)


        'frmRetportView.CrystalReportViewer1.ReportSource = rptobj
        'frmRetportView.CrystalReportViewer1.Refresh()
        frmService.customer.price = txtPrice.Text
        frmService.customer.comment = txtComent.Text
        frmService.dbupdate.addService(frmService.customer)



        Dim ds As New DataSet1
        Dim t As DataTable = ds.Tables.Add("DataTable5")

        t.Columns.Add("cusName", Type.GetType("System.String"))
        t.Columns.Add("cusPhone", Type.GetType("System.String"))
        t.Columns.Add("cusEmail", Type.GetType("System.String"))
        t.Columns.Add("picture", Type.GetType("System.Byte[]"))
        t.Columns.Add("comment", Type.GetType("System.String"))
        t.Columns.Add("price", Type.GetType("System.Double"))
        t.Columns.Add("cusAddress", Type.GetType("System.String"))
        t.Columns.Add("cusPosCode", Type.GetType("System.String"))
        t.Columns.Add("cusCity", Type.GetType("System.String"))
        t.Columns.Add("cusProvince", Type.GetType("System.String"))


        Dim r As DataRow
        r = t.NewRow
        r("cusName") = frmService.customer.cusName
        r("cusPhone") = frmService.customer.cusPhone
        r("cusEmail") = frmService.customer.cusEmail
        r("picture") = frmService.customer.picture

        r("comment") = frmService.customer.comment
        r("price") = frmService.customer.price
        r("cusAddress") = frmService.customer.cusAddress
        r("cusPosCode") = frmService.customer.cusPosCode
        r("cusCity") = frmService.customer.cusCity
        r("cusProvince") = frmService.customer.cusProvince


        t.Rows.Add(r)

        Dim objRpt As New CrystalReport1
        objRpt.SetDataSource(ds.Tables("DataTable5"))
        frmRetportView.CrystalReportViewer1.ReportSource = objRpt
        frmRetportView.CrystalReportViewer1.Refresh()
        frmRetportView.ShowDialog()
    End Sub


    Private Sub txtDataToEncode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDataToEncode.TextChanged

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtComent As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPrice As System.Windows.Forms.TextBox
End Class
