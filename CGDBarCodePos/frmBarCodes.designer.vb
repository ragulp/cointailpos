<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBarCodes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBarCodes))
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.chbShowText = New System.Windows.Forms.CheckBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.chbShowCheckSum = New System.Windows.Forms.CheckBox()
        Me.nudPages = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.EaN13Barcode1 = New CGDBarCodePos.EAN13Barcode()
        Me.btnClose = New System.Windows.Forms.Button()
        CType(Me.nudPages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(127, 204)
        Me.TextBox1.MaxLength = 12
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(125, 20)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.Text = "978020113447"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(30, 201)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Create"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(0, 33)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(33, 47)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Back Color"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Black
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(0, 86)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(33, 47)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Fore Color"
        Me.Button3.UseVisualStyleBackColor = False
        Me.Button3.Visible = False
        '
        'chbShowText
        '
        Me.chbShowText.AutoSize = True
        Me.chbShowText.Checked = True
        Me.chbShowText.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chbShowText.Enabled = False
        Me.chbShowText.Location = New System.Drawing.Point(30, 230)
        Me.chbShowText.Name = "chbShowText"
        Me.chbShowText.Size = New System.Drawing.Size(120, 17)
        Me.chbShowText.TabIndex = 7
        Me.chbShowText.Text = "Show Barcode Text"
        Me.chbShowText.UseVisualStyleBackColor = True
        Me.chbShowText.Visible = False
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(148, 19)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(56, 47)
        Me.btnPrint.TabIndex = 8
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(-1, 139)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(34, 47)
        Me.btnPreview.TabIndex = 9
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        Me.btnPreview.Visible = False
        '
        'chbShowCheckSum
        '
        Me.chbShowCheckSum.AutoSize = True
        Me.chbShowCheckSum.Checked = True
        Me.chbShowCheckSum.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chbShowCheckSum.Enabled = False
        Me.chbShowCheckSum.Location = New System.Drawing.Point(155, 230)
        Me.chbShowCheckSum.Name = "chbShowCheckSum"
        Me.chbShowCheckSum.Size = New System.Drawing.Size(111, 17)
        Me.chbShowCheckSum.TabIndex = 10
        Me.chbShowCheckSum.Text = "Show Check Sum"
        Me.chbShowCheckSum.UseVisualStyleBackColor = True
        Me.chbShowCheckSum.Visible = False
        '
        'nudPages
        '
        Me.nudPages.Location = New System.Drawing.Point(106, 34)
        Me.nudPages.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudPages.Name = "nudPages"
        Me.nudPages.Size = New System.Drawing.Size(36, 20)
        Me.nudPages.TabIndex = 11
        Me.nudPages.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(92, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Number of Pages:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnClose)
        Me.GroupBox1.Controls.Add(Me.btnPrint)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.nudPages)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 272)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(280, 73)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Priniting"
        '
        'EaN13Barcode1
        '
        Me.EaN13Barcode1.BackColor = System.Drawing.Color.White
        Me.EaN13Barcode1.BarHeight = 0.0R
        Me.EaN13Barcode1.BarWidth = 0.33R
        Me.EaN13Barcode1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EaN13Barcode1.Location = New System.Drawing.Point(43, 33)
        Me.EaN13Barcode1.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.EaN13Barcode1.Name = "EaN13Barcode1"
        Me.EaN13Barcode1.ShowBarcodeText = False
        Me.EaN13Barcode1.ShowCheckSum = False
        Me.EaN13Barcode1.Size = New System.Drawing.Size(209, 146)
        Me.EaN13Barcode1.TabIndex = 4
        Me.EaN13Barcode1.Value = "000000000000"
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(210, 19)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(56, 47)
        Me.btnClose.TabIndex = 13
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmBarCodes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(297, 375)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chbShowCheckSum)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.chbShowText)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.EaN13Barcode1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox1)
        Me.Name = "frmBarCodes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.nudPages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents EaN13Barcode1 As EAN13Barcode
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents chbShowText As System.Windows.Forms.CheckBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents PrintPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents chbShowCheckSum As System.Windows.Forms.CheckBox
    Friend WithEvents nudPages As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnClose As System.Windows.Forms.Button

End Class
