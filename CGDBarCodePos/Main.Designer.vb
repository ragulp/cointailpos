﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.lblDiscountAmnt = New System.Windows.Forms.Label()
        Me.lblDiscount = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblPst = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblNet = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnVoid = New System.Windows.Forms.Button()
        Me.btn_Qty = New System.Windows.Forms.Button()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtInput = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TopStatus = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblstatus = New System.Windows.Forms.Label()
        Me.lblTimeDisplay = New System.Windows.Forms.Label()
        Me.lblDateDiplay = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.btnOptions = New System.Windows.Forms.Button()
        Me.btnKeypad = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnSpcial = New System.Windows.Forms.Button()
        Me.btnPreSet = New System.Windows.Forms.Button()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.pnlPreSet = New System.Windows.Forms.Panel()
        Me.pnlKeypad = New System.Windows.Forms.Panel()
        Me.btndot00 = New System.Windows.Forms.Button()
        Me.btnDot = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn7 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btn6 = New System.Windows.Forms.Button()
        Me.btn9 = New System.Windows.Forms.Button()
        Me.btn0 = New System.Windows.Forms.Button()
        Me.btn8 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btn5 = New System.Windows.Forms.Button()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.txtkeypadtotal = New System.Windows.Forms.TextBox()
        Me.pnlSearch = New System.Windows.Forms.Panel()
        Me.btnselSerch = New System.Windows.Forms.Button()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.CloseTabelView = New System.Windows.Forms.Panel()
        Me.btnPrintBill = New System.Windows.Forms.Button()
        Me.lblPayment = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btnPayFinish = New System.Windows.Forms.Button()
        Me.btnPayCancel = New System.Windows.Forms.Button()
        Me.btnPayOther = New System.Windows.Forms.Button()
        Me.btnPayDebit = New System.Windows.Forms.Button()
        Me.btnPayAmex = New System.Windows.Forms.Button()
        Me.btnPayMC = New System.Windows.Forms.Button()
        Me.btnPayVisa = New System.Windows.Forms.Button()
        Me.btnPayCash = New System.Windows.Forms.Button()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.lblChange = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.lblBalanceDue = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.pnlButtons = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TopStatus.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.pnlPreSet.SuspendLayout()
        Me.pnlKeypad.SuspendLayout()
        Me.pnlSearch.SuspendLayout()
        Me.CloseTabelView.SuspendLayout()
        Me.Panel20.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel6, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel8, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.ListView1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(-4, 40)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.53361!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.46639!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(281, 712)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Silver
        Me.Panel6.Controls.Add(Me.lblDiscountAmnt)
        Me.Panel6.Controls.Add(Me.lblDiscount)
        Me.Panel6.Controls.Add(Me.lblTotal)
        Me.Panel6.Controls.Add(Me.Label10)
        Me.Panel6.Controls.Add(Me.lblPst)
        Me.Panel6.Controls.Add(Me.Label11)
        Me.Panel6.Controls.Add(Me.lblNet)
        Me.Panel6.Controls.Add(Me.Label13)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(3, 564)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(275, 145)
        Me.Panel6.TabIndex = 7
        '
        'lblDiscountAmnt
        '
        Me.lblDiscountAmnt.BackColor = System.Drawing.Color.Crimson
        Me.lblDiscountAmnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiscountAmnt.Location = New System.Drawing.Point(165, 24)
        Me.lblDiscountAmnt.Name = "lblDiscountAmnt"
        Me.lblDiscountAmnt.Size = New System.Drawing.Size(101, 18)
        Me.lblDiscountAmnt.TabIndex = 24
        Me.lblDiscountAmnt.Text = "0.00"
        Me.lblDiscountAmnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDiscountAmnt.Visible = False
        '
        'lblDiscount
        '
        Me.lblDiscount.AutoSize = True
        Me.lblDiscount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiscount.Location = New System.Drawing.Point(90, 27)
        Me.lblDiscount.Name = "lblDiscount"
        Me.lblDiscount.Size = New System.Drawing.Size(74, 15)
        Me.lblDiscount.TabIndex = 23
        Me.lblDiscount.Text = "Discounts: $"
        Me.lblDiscount.Visible = False
        '
        'lblTotal
        '
        Me.lblTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(101, 100)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(167, 26)
        Me.lblTotal.TabIndex = 19
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(38, 102)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 24)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Total $"
        '
        'lblPst
        '
        Me.lblPst.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblPst.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPst.Location = New System.Drawing.Point(162, 73)
        Me.lblPst.Name = "lblPst"
        Me.lblPst.Size = New System.Drawing.Size(105, 18)
        Me.lblPst.TabIndex = 17
        Me.lblPst.Text = "0.00"
        Me.lblPst.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(98, 74)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 15)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "HST          $"
        '
        'lblNet
        '
        Me.lblNet.BackColor = System.Drawing.Color.White
        Me.lblNet.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNet.Location = New System.Drawing.Point(162, 48)
        Me.lblNet.Name = "lblNet"
        Me.lblNet.Size = New System.Drawing.Size(105, 18)
        Me.lblNet.TabIndex = 13
        Me.lblNet.Text = "0.00"
        Me.lblNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(96, 51)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(69, 15)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Net Total: $"
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.ColumnCount = 2
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel8.Controls.Add(Me.btnVoid, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.btn_Qty, 0, 0)
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(3, 494)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 1
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(275, 64)
        Me.TableLayoutPanel8.TabIndex = 6
        '
        'btnVoid
        '
        Me.btnVoid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnVoid.Font = New System.Drawing.Font("Comic Sans MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.Location = New System.Drawing.Point(140, 3)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.Size = New System.Drawing.Size(132, 58)
        Me.btnVoid.TabIndex = 6
        Me.btnVoid.Text = "Void Item"
        Me.btnVoid.UseVisualStyleBackColor = True
        '
        'btn_Qty
        '
        Me.btn_Qty.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btn_Qty.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Qty.Location = New System.Drawing.Point(3, 3)
        Me.btn_Qty.Name = "btn_Qty"
        Me.btn_Qty.Size = New System.Drawing.Size(131, 58)
        Me.btn_Qty.TabIndex = 7
        Me.btn_Qty.Text = "+ Qty"
        Me.btn_Qty.UseVisualStyleBackColor = True
        '
        'ListView1
        '
        Me.ListView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView1.FullRowSelect = True
        Me.ListView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ListView1.Location = New System.Drawing.Point(3, 94)
        Me.ListView1.MultiSelect = False
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(275, 394)
        Me.ListView1.TabIndex = 5
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.List
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtInput)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(275, 85)
        Me.Panel1.TabIndex = 0
        '
        'txtInput
        '
        Me.txtInput.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInput.Location = New System.Drawing.Point(3, 54)
        Me.txtInput.Name = "txtInput"
        Me.txtInput.Size = New System.Drawing.Size(272, 26)
        Me.txtInput.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(-2, -2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(275, 50)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'TopStatus
        '
        Me.TopStatus.BackColor = System.Drawing.Color.Transparent
        Me.TopStatus.Controls.Add(Me.lblUser)
        Me.TopStatus.Controls.Add(Me.Label1)
        Me.TopStatus.Controls.Add(Me.lblstatus)
        Me.TopStatus.Controls.Add(Me.lblTimeDisplay)
        Me.TopStatus.Controls.Add(Me.lblDateDiplay)
        Me.TopStatus.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopStatus.Location = New System.Drawing.Point(0, 0)
        Me.TopStatus.Margin = New System.Windows.Forms.Padding(0)
        Me.TopStatus.Name = "TopStatus"
        Me.TopStatus.Size = New System.Drawing.Size(1008, 37)
        Me.TopStatus.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(765, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Ver. 2.7.3"
        '
        'lblstatus
        '
        Me.lblstatus.AutoSize = True
        Me.lblstatus.BackColor = System.Drawing.Color.Transparent
        Me.lblstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstatus.ForeColor = System.Drawing.Color.White
        Me.lblstatus.Location = New System.Drawing.Point(18, 6)
        Me.lblstatus.Name = "lblstatus"
        Me.lblstatus.Size = New System.Drawing.Size(16, 24)
        Me.lblstatus.TabIndex = 5
        Me.lblstatus.Text = "-"
        '
        'lblTimeDisplay
        '
        Me.lblTimeDisplay.AutoSize = True
        Me.lblTimeDisplay.BackColor = System.Drawing.Color.Transparent
        Me.lblTimeDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTimeDisplay.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.lblTimeDisplay.Location = New System.Drawing.Point(872, 1)
        Me.lblTimeDisplay.Name = "lblTimeDisplay"
        Me.lblTimeDisplay.Size = New System.Drawing.Size(43, 20)
        Me.lblTimeDisplay.TabIndex = 7
        Me.lblTimeDisplay.Text = "Time"
        '
        'lblDateDiplay
        '
        Me.lblDateDiplay.AutoSize = True
        Me.lblDateDiplay.BackColor = System.Drawing.Color.Transparent
        Me.lblDateDiplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateDiplay.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.lblDateDiplay.Location = New System.Drawing.Point(881, 19)
        Me.lblDateDiplay.Name = "lblDateDiplay"
        Me.lblDateDiplay.Size = New System.Drawing.Size(37, 16)
        Me.lblDateDiplay.TabIndex = 6
        Me.lblDateDiplay.Text = "Date"
        '
        'Timer1
        '
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TableLayoutPanel2.BackgroundImage = Global.CGDBarCodePos.My.Resources.Resources.inner
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.pnlMain, 0, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(283, 43)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 121.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(713, 706)
        Me.TableLayoutPanel2.TabIndex = 10
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel3.ColumnCount = 6
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.Controls.Add(Me.Button6, 5, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnOptions, 3, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnKeypad, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnPrint, 4, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.btnSearch, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel4, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 588)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(707, 115)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Button6
        '
        Me.Button6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button6.Enabled = False
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(588, 3)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(116, 109)
        Me.Button6.TabIndex = 5
        Me.Button6.UseVisualStyleBackColor = True
        '
        'btnOptions
        '
        Me.btnOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnOptions.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOptions.Location = New System.Drawing.Point(354, 3)
        Me.btnOptions.Name = "btnOptions"
        Me.btnOptions.Size = New System.Drawing.Size(111, 109)
        Me.btnOptions.TabIndex = 3
        Me.btnOptions.Text = "Options"
        Me.btnOptions.UseVisualStyleBackColor = True
        '
        'btnKeypad
        '
        Me.btnKeypad.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnKeypad.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnKeypad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeypad.Location = New System.Drawing.Point(120, 3)
        Me.btnKeypad.Name = "btnKeypad"
        Me.btnKeypad.Size = New System.Drawing.Size(111, 109)
        Me.btnKeypad.TabIndex = 1
        Me.btnKeypad.Text = "Keypad"
        Me.btnKeypad.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(471, 3)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(111, 109)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.Text = "Finish Order"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Location = New System.Drawing.Point(237, 3)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(111, 109)
        Me.btnSearch.TabIndex = 0
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 1
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.btnSpcial, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.btnPreSet, 0, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(111, 109)
        Me.TableLayoutPanel4.TabIndex = 6
        '
        'btnSpcial
        '
        Me.btnSpcial.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnSpcial.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnSpcial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpcial.Location = New System.Drawing.Point(3, 57)
        Me.btnSpcial.Name = "btnSpcial"
        Me.btnSpcial.Size = New System.Drawing.Size(105, 49)
        Me.btnSpcial.TabIndex = 5
        Me.btnSpcial.Text = "Specials"
        Me.btnSpcial.UseVisualStyleBackColor = False
        '
        'btnPreSet
        '
        Me.btnPreSet.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnPreSet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnPreSet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreSet.Location = New System.Drawing.Point(3, 3)
        Me.btnPreSet.Name = "btnPreSet"
        Me.btnPreSet.Size = New System.Drawing.Size(105, 48)
        Me.btnPreSet.TabIndex = 4
        Me.btnPreSet.Text = "Pre-Set Items"
        Me.btnPreSet.UseVisualStyleBackColor = False
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.SystemColors.ControlLight
        Me.pnlMain.Controls.Add(Me.pnlPreSet)
        Me.pnlMain.Controls.Add(Me.pnlKeypad)
        Me.pnlMain.Controls.Add(Me.pnlSearch)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(3, 3)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(707, 579)
        Me.pnlMain.TabIndex = 1
        '
        'pnlPreSet
        '
        Me.pnlPreSet.Controls.Add(Me.pnlButtons)
        Me.pnlPreSet.Location = New System.Drawing.Point(0, 0)
        Me.pnlPreSet.Name = "pnlPreSet"
        Me.pnlPreSet.Size = New System.Drawing.Size(707, 579)
        Me.pnlPreSet.TabIndex = 2
        '
        'pnlKeypad
        '
        Me.pnlKeypad.BackColor = System.Drawing.SystemColors.ControlLight
        Me.pnlKeypad.Controls.Add(Me.btndot00)
        Me.pnlKeypad.Controls.Add(Me.btnDot)
        Me.pnlKeypad.Controls.Add(Me.btn4)
        Me.pnlKeypad.Controls.Add(Me.btn7)
        Me.pnlKeypad.Controls.Add(Me.btn3)
        Me.pnlKeypad.Controls.Add(Me.btn6)
        Me.pnlKeypad.Controls.Add(Me.btn9)
        Me.pnlKeypad.Controls.Add(Me.btn0)
        Me.pnlKeypad.Controls.Add(Me.btn8)
        Me.pnlKeypad.Controls.Add(Me.btn2)
        Me.pnlKeypad.Controls.Add(Me.btnOK)
        Me.pnlKeypad.Controls.Add(Me.btn5)
        Me.pnlKeypad.Controls.Add(Me.btn1)
        Me.pnlKeypad.Controls.Add(Me.btnClear)
        Me.pnlKeypad.Controls.Add(Me.txtkeypadtotal)
        Me.pnlKeypad.Location = New System.Drawing.Point(0, 0)
        Me.pnlKeypad.Name = "pnlKeypad"
        Me.pnlKeypad.Size = New System.Drawing.Size(707, 579)
        Me.pnlKeypad.TabIndex = 1
        '
        'btndot00
        '
        Me.btndot00.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btndot00.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btndot00.ForeColor = System.Drawing.Color.Black
        Me.btndot00.Location = New System.Drawing.Point(396, 375)
        Me.btndot00.Name = "btndot00"
        Me.btndot00.Size = New System.Drawing.Size(90, 90)
        Me.btndot00.TabIndex = 41
        Me.btndot00.Text = ".00"
        Me.btndot00.UseVisualStyleBackColor = True
        '
        'btnDot
        '
        Me.btnDot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDot.Font = New System.Drawing.Font("Times New Roman", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDot.Location = New System.Drawing.Point(191, 375)
        Me.btnDot.Name = "btnDot"
        Me.btnDot.Size = New System.Drawing.Size(90, 90)
        Me.btnDot.TabIndex = 40
        Me.btnDot.Text = "."
        Me.btnDot.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn4.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn4.Location = New System.Drawing.Point(190, 184)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(90, 90)
        Me.btn4.TabIndex = 39
        Me.btn4.Text = "4"
        Me.btn4.UseVisualStyleBackColor = True
        '
        'btn7
        '
        Me.btn7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn7.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn7.Location = New System.Drawing.Point(190, 279)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(90, 90)
        Me.btn7.TabIndex = 38
        Me.btn7.Text = "7"
        Me.btn7.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn3.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn3.Location = New System.Drawing.Point(396, 88)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(90, 90)
        Me.btn3.TabIndex = 36
        Me.btn3.Text = "3"
        Me.btn3.UseVisualStyleBackColor = True
        '
        'btn6
        '
        Me.btn6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn6.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn6.Location = New System.Drawing.Point(396, 184)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(90, 90)
        Me.btn6.TabIndex = 35
        Me.btn6.Text = "6"
        Me.btn6.UseVisualStyleBackColor = True
        '
        'btn9
        '
        Me.btn9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn9.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn9.Location = New System.Drawing.Point(397, 279)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(90, 90)
        Me.btn9.TabIndex = 34
        Me.btn9.Text = "9"
        Me.btn9.UseVisualStyleBackColor = True
        '
        'btn0
        '
        Me.btn0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn0.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn0.Location = New System.Drawing.Point(294, 375)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(90, 90)
        Me.btn0.TabIndex = 33
        Me.btn0.Text = "0"
        Me.btn0.UseVisualStyleBackColor = True
        '
        'btn8
        '
        Me.btn8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn8.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn8.Location = New System.Drawing.Point(294, 279)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(90, 90)
        Me.btn8.TabIndex = 32
        Me.btn8.Text = "8"
        Me.btn8.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn2.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn2.Location = New System.Drawing.Point(294, 88)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(90, 90)
        Me.btn2.TabIndex = 31
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnOK.Font = New System.Drawing.Font("Times New Roman", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Location = New System.Drawing.Point(191, 471)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(297, 90)
        Me.btnOK.TabIndex = 30
        Me.btnOK.Text = "Enter"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btn5
        '
        Me.btn5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn5.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold)
        Me.btn5.Location = New System.Drawing.Point(294, 184)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(90, 90)
        Me.btn5.TabIndex = 29
        Me.btn5.Text = "5"
        Me.btn5.UseVisualStyleBackColor = True
        '
        'btn1
        '
        Me.btn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn1.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn1.Location = New System.Drawing.Point(190, 88)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(90, 90)
        Me.btn1.TabIndex = 28
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnClear.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(387, 33)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(99, 49)
        Me.btnClear.TabIndex = 27
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'txtkeypadtotal
        '
        Me.txtkeypadtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtkeypadtotal.Location = New System.Drawing.Point(190, 38)
        Me.txtkeypadtotal.MaxLength = 7
        Me.txtkeypadtotal.Name = "txtkeypadtotal"
        Me.txtkeypadtotal.Size = New System.Drawing.Size(191, 38)
        Me.txtkeypadtotal.TabIndex = 26
        '
        'pnlSearch
        '
        Me.pnlSearch.Controls.Add(Me.btnselSerch)
        Me.pnlSearch.Controls.Add(Me.ListView2)
        Me.pnlSearch.Controls.Add(Me.txtSearch)
        Me.pnlSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlSearch.Location = New System.Drawing.Point(0, 0)
        Me.pnlSearch.Name = "pnlSearch"
        Me.pnlSearch.Size = New System.Drawing.Size(707, 579)
        Me.pnlSearch.TabIndex = 0
        '
        'btnselSerch
        '
        Me.btnselSerch.Location = New System.Drawing.Point(522, 11)
        Me.btnselSerch.Name = "btnselSerch"
        Me.btnselSerch.Size = New System.Drawing.Size(122, 52)
        Me.btnselSerch.TabIndex = 2
        Me.btnselSerch.Text = "Enter"
        Me.btnselSerch.UseVisualStyleBackColor = True
        '
        'ListView2
        '
        Me.ListView2.FullRowSelect = True
        Me.ListView2.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ListView2.Location = New System.Drawing.Point(27, 76)
        Me.ListView2.MultiSelect = False
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(641, 472)
        Me.ListView2.TabIndex = 1
        Me.ListView2.UseCompatibleStateImageBehavior = False
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(59, 22)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(442, 26)
        Me.txtSearch.TabIndex = 0
        '
        'CloseTabelView
        '
        Me.CloseTabelView.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.CloseTabelView.Controls.Add(Me.btnPrintBill)
        Me.CloseTabelView.Controls.Add(Me.lblPayment)
        Me.CloseTabelView.Controls.Add(Me.Button1)
        Me.CloseTabelView.Controls.Add(Me.Button4)
        Me.CloseTabelView.Controls.Add(Me.btnPayFinish)
        Me.CloseTabelView.Controls.Add(Me.btnPayCancel)
        Me.CloseTabelView.Controls.Add(Me.btnPayOther)
        Me.CloseTabelView.Controls.Add(Me.btnPayDebit)
        Me.CloseTabelView.Controls.Add(Me.btnPayAmex)
        Me.CloseTabelView.Controls.Add(Me.btnPayMC)
        Me.CloseTabelView.Controls.Add(Me.btnPayVisa)
        Me.CloseTabelView.Controls.Add(Me.btnPayCash)
        Me.CloseTabelView.Controls.Add(Me.Label49)
        Me.CloseTabelView.Controls.Add(Me.Panel20)
        Me.CloseTabelView.Controls.Add(Me.Label48)
        Me.CloseTabelView.Controls.Add(Me.Panel19)
        Me.CloseTabelView.Controls.Add(Me.Label39)
        Me.CloseTabelView.Location = New System.Drawing.Point(0, 37)
        Me.CloseTabelView.Name = "CloseTabelView"
        Me.CloseTabelView.Size = New System.Drawing.Size(1008, 715)
        Me.CloseTabelView.TabIndex = 49
        Me.CloseTabelView.Visible = False
        '
        'btnPrintBill
        '
        Me.btnPrintBill.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintBill.Location = New System.Drawing.Point(598, 574)
        Me.btnPrintBill.Name = "btnPrintBill"
        Me.btnPrintBill.Size = New System.Drawing.Size(185, 100)
        Me.btnPrintBill.TabIndex = 22
        Me.btnPrintBill.Text = "Finish /w Bill"
        Me.btnPrintBill.UseVisualStyleBackColor = True
        '
        'lblPayment
        '
        Me.lblPayment.AutoSize = True
        Me.lblPayment.Location = New System.Drawing.Point(542, 401)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(10, 13)
        Me.lblPayment.TabIndex = 21
        Me.lblPayment.Text = "-"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(874, 281)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 100)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "Store Credit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(874, 166)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(100, 100)
        Me.Button4.TabIndex = 19
        Me.Button4.Text = "Gift Card"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btnPayFinish
        '
        Me.btnPayFinish.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayFinish.Location = New System.Drawing.Point(406, 574)
        Me.btnPayFinish.Name = "btnPayFinish"
        Me.btnPayFinish.Size = New System.Drawing.Size(183, 100)
        Me.btnPayFinish.TabIndex = 15
        Me.btnPayFinish.Text = "Finish No Bill"
        Me.btnPayFinish.UseVisualStyleBackColor = True
        '
        'btnPayCancel
        '
        Me.btnPayCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayCancel.Location = New System.Drawing.Point(791, 574)
        Me.btnPayCancel.Name = "btnPayCancel"
        Me.btnPayCancel.Size = New System.Drawing.Size(183, 100)
        Me.btnPayCancel.TabIndex = 14
        Me.btnPayCancel.Text = "Cancel Payment"
        Me.btnPayCancel.UseVisualStyleBackColor = True
        '
        'btnPayOther
        '
        Me.btnPayOther.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayOther.Location = New System.Drawing.Point(643, 281)
        Me.btnPayOther.Name = "btnPayOther"
        Me.btnPayOther.Size = New System.Drawing.Size(100, 100)
        Me.btnPayOther.TabIndex = 12
        Me.btnPayOther.Text = "Other Payment"
        Me.btnPayOther.UseVisualStyleBackColor = True
        '
        'btnPayDebit
        '
        Me.btnPayDebit.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayDebit.Location = New System.Drawing.Point(531, 281)
        Me.btnPayDebit.Name = "btnPayDebit"
        Me.btnPayDebit.Size = New System.Drawing.Size(100, 100)
        Me.btnPayDebit.TabIndex = 11
        Me.btnPayDebit.Text = "Debit"
        Me.btnPayDebit.UseVisualStyleBackColor = True
        '
        'btnPayAmex
        '
        Me.btnPayAmex.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayAmex.Location = New System.Drawing.Point(758, 281)
        Me.btnPayAmex.Name = "btnPayAmex"
        Me.btnPayAmex.Size = New System.Drawing.Size(100, 100)
        Me.btnPayAmex.TabIndex = 10
        Me.btnPayAmex.Text = "Amex"
        Me.btnPayAmex.UseVisualStyleBackColor = True
        '
        'btnPayMC
        '
        Me.btnPayMC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayMC.Location = New System.Drawing.Point(758, 166)
        Me.btnPayMC.Name = "btnPayMC"
        Me.btnPayMC.Size = New System.Drawing.Size(100, 100)
        Me.btnPayMC.TabIndex = 9
        Me.btnPayMC.Text = "Master Card"
        Me.btnPayMC.UseVisualStyleBackColor = True
        '
        'btnPayVisa
        '
        Me.btnPayVisa.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayVisa.Location = New System.Drawing.Point(643, 166)
        Me.btnPayVisa.Name = "btnPayVisa"
        Me.btnPayVisa.Size = New System.Drawing.Size(100, 100)
        Me.btnPayVisa.TabIndex = 8
        Me.btnPayVisa.Text = "Visa"
        Me.btnPayVisa.UseVisualStyleBackColor = True
        '
        'btnPayCash
        '
        Me.btnPayCash.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayCash.Location = New System.Drawing.Point(531, 166)
        Me.btnPayCash.Name = "btnPayCash"
        Me.btnPayCash.Size = New System.Drawing.Size(100, 100)
        Me.btnPayCash.TabIndex = 7
        Me.btnPayCash.Text = "Cash"
        Me.btnPayCash.UseVisualStyleBackColor = True
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label49.Location = New System.Drawing.Point(636, 72)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(233, 37)
        Me.Label49.TabIndex = 6
        Me.Label49.Text = "Payment Type"
        '
        'Panel20
        '
        Me.Panel20.BackColor = System.Drawing.Color.Black
        Me.Panel20.Controls.Add(Me.lblChange)
        Me.Panel20.Location = New System.Drawing.Point(48, 420)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(379, 121)
        Me.Panel20.TabIndex = 5
        '
        'lblChange
        '
        Me.lblChange.BackColor = System.Drawing.Color.Transparent
        Me.lblChange.Font = New System.Drawing.Font("Microsoft Sans Serif", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChange.ForeColor = System.Drawing.Color.Maroon
        Me.lblChange.Location = New System.Drawing.Point(-14, -10)
        Me.lblChange.Margin = New System.Windows.Forms.Padding(0)
        Me.lblChange.Name = "lblChange"
        Me.lblChange.Size = New System.Drawing.Size(436, 119)
        Me.lblChange.TabIndex = 2
        Me.lblChange.Text = "$0000.00"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label48.Location = New System.Drawing.Point(50, 380)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(135, 37)
        Me.Label48.TabIndex = 4
        Me.Label48.Text = "Change"
        '
        'Panel19
        '
        Me.Panel19.BackColor = System.Drawing.Color.Black
        Me.Panel19.Controls.Add(Me.lblBalanceDue)
        Me.Panel19.Location = New System.Drawing.Point(48, 112)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(379, 121)
        Me.Panel19.TabIndex = 3
        '
        'lblBalanceDue
        '
        Me.lblBalanceDue.BackColor = System.Drawing.Color.Transparent
        Me.lblBalanceDue.Font = New System.Drawing.Font("Microsoft Sans Serif", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceDue.ForeColor = System.Drawing.Color.White
        Me.lblBalanceDue.Location = New System.Drawing.Point(-14, -10)
        Me.lblBalanceDue.Margin = New System.Windows.Forms.Padding(0)
        Me.lblBalanceDue.Name = "lblBalanceDue"
        Me.lblBalanceDue.Size = New System.Drawing.Size(436, 119)
        Me.lblBalanceDue.TabIndex = 2
        Me.lblBalanceDue.Text = "$0000.00"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label39.Location = New System.Drawing.Point(50, 72)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(191, 37)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Bill Amount"
        '
        'Timer2
        '
        '
        'pnlButtons
        '
        Me.pnlButtons.BackColor = System.Drawing.SystemColors.ControlLight
        Me.pnlButtons.Location = New System.Drawing.Point(3, 3)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(701, 579)
        Me.pnlButtons.TabIndex = 5
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.BackColor = System.Drawing.Color.Transparent
        Me.lblUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.ForeColor = System.Drawing.Color.White
        Me.lblUser.Location = New System.Drawing.Point(651, 6)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(12, 16)
        Me.lblUser.TabIndex = 9
        Me.lblUser.Text = "-"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.CGDBarCodePos.My.Resources.Resources.grap1
        Me.ClientSize = New System.Drawing.Size(1008, 752)
        Me.ControlBox = False
        Me.Controls.Add(Me.CloseTabelView)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.TopStatus)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(1024, 768)
        Me.MinimumSize = New System.Drawing.Size(1024, 768)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TopStatus.ResumeLayout(False)
        Me.TopStatus.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlPreSet.ResumeLayout(False)
        Me.pnlKeypad.ResumeLayout(False)
        Me.pnlKeypad.PerformLayout()
        Me.pnlSearch.ResumeLayout(False)
        Me.pnlSearch.PerformLayout()
        Me.CloseTabelView.ResumeLayout(False)
        Me.CloseTabelView.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        Me.Panel19.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TopStatus As System.Windows.Forms.Panel
    Friend WithEvents lblstatus As System.Windows.Forms.Label
    Friend WithEvents lblTimeDisplay As System.Windows.Forms.Label
    Friend WithEvents lblDateDiplay As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lblDiscountAmnt As System.Windows.Forms.Label
    Friend WithEvents lblDiscount As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblPst As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblNet As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel8 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnVoid As System.Windows.Forms.Button
    Friend WithEvents btn_Qty As System.Windows.Forms.Button
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents txtInput As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents btnPreSet As System.Windows.Forms.Button
    Friend WithEvents btnOptions As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnKeypad As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents pnlSearch As System.Windows.Forms.Panel
    Friend WithEvents pnlPreSet As System.Windows.Forms.Panel
    Friend WithEvents pnlKeypad As System.Windows.Forms.Panel
    Friend WithEvents btndot00 As System.Windows.Forms.Button
    Friend WithEvents btnDot As System.Windows.Forms.Button
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn7 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents btn6 As System.Windows.Forms.Button
    Friend WithEvents btn9 As System.Windows.Forms.Button
    Friend WithEvents btn0 As System.Windows.Forms.Button
    Friend WithEvents btn8 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btn5 As System.Windows.Forms.Button
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents txtkeypadtotal As System.Windows.Forms.TextBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents ListView2 As System.Windows.Forms.ListView
    Friend WithEvents CloseTabelView As System.Windows.Forms.Panel
    Friend WithEvents btnPayFinish As System.Windows.Forms.Button
    Friend WithEvents btnPayCancel As System.Windows.Forms.Button
    Friend WithEvents btnPayOther As System.Windows.Forms.Button
    Friend WithEvents btnPayDebit As System.Windows.Forms.Button
    Friend WithEvents btnPayAmex As System.Windows.Forms.Button
    Friend WithEvents btnPayMC As System.Windows.Forms.Button
    Friend WithEvents btnPayVisa As System.Windows.Forms.Button
    Friend WithEvents btnPayCash As System.Windows.Forms.Button
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Panel20 As System.Windows.Forms.Panel
    Friend WithEvents lblChange As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Panel19 As System.Windows.Forms.Panel
    Friend WithEvents lblBalanceDue As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents btnselSerch As System.Windows.Forms.Button
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents btnPrintBill As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnSpcial As System.Windows.Forms.Button
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents pnlButtons As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblUser As System.Windows.Forms.Label

End Class
