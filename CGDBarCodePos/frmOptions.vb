﻿Option Strict Off
Public Class frmOptions

    Private Sub btnAddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddItem.Click
        frmAddProduct.txtBarcode.Text = ""
        frmAddProduct.txtBarcode2.Text = ""
        frmAddProduct.txtName.Text = ""
        frmAddProduct.txtManufacture.Text = ""
        frmAddProduct.txtCost.Text = ""
        frmAddProduct.txtDesc.Text = ""
        frmAddProduct.txtPrice.Text = ""
        frmAddProduct.txtQty.Text = ""
        frmAddProduct.chkRefurb.CheckState = CheckState.Unchecked
        frmAddProduct.chkTaxable.CheckState = CheckState.Unchecked
        frmAddProduct.btnAdd.Visible = True
        frmAddProduct.btnUpdate.Visible = False
        frmAddProduct.txtQty.ReadOnly = False
        frmAddProduct.ShowDialog()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        frmMain.lblstatus.Text = ""
        Me.Close()

    End Sub

    Private Sub btnAddStock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddStock.Click
        frmAddStock.btnPrint.Visible = False
        frmAddStock.ListView2.Clear()

        frmAddStock.Label1.Visible = True
        frmAddStock.lblName.Visible = True
        frmAddStock.lblPrID.Visible = True
        frmAddStock.lblName.Text = "Product ID -            Increase Qty By :"
        frmAddStock.btnUpdate.Visible = True
        frmAddStock.btndelet.Visible = False
        frmAddStock.txtqtyIncrease.Visible = True
        frmAddStock.txtSearch.Visible = True
        frmAddStock.ShowDialog()

    End Sub

    Private Sub btnDele_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDele.Click
        frmAddStock.btnPrint.Visible = False
        frmAddStock.ListView2.Clear()

        frmAddStock.txtSearch.Text = ""
        frmAddStock.Label1.Visible = True
        frmAddStock.lblName.Visible = True
        frmAddStock.lblPrID.Visible = True
        frmAddStock.lblName.Text = "Product ID -            Will be Deleted  "
        frmAddStock.btnUpdate.Visible = False
        frmAddStock.btndelet.Visible = True
        frmAddStock.txtqtyIncrease.Visible = False
        frmAddStock.txtSearch.Visible = True
        frmAddStock.ShowDialog()

    End Sub

    Private Sub btnInventory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInventory.Click
        frmAddStock.btnPrint.Visible = True
        frmAddStock.txtqtyIncrease.Text = ""
        frmAddStock.txtSearch.Text = ""
        frmAddStock.ListView2.Clear()


        frmAddStock.Label1.Visible = False
        frmAddStock.lblName.Visible = False
        frmAddStock.lblPrID.Visible = False
        frmAddStock.btndelet.Visible = False
        frmAddStock.btnUpdate.Visible = False
        frmAddStock.txtqtyIncrease.Visible = False
        frmAddStock.txtSearch.Visible = False


        Dim txtser As New CGDsql
        Dim searchArray As ArrayList
        Dim searchItem As New ProductItem
        searchArray = txtser.getlowesStock()
        'MsgBox(txtSearch.Text & " array size is " & searchArray.Count)

        frmAddStock.ListView2.Items.Clear()

        For x = 0 To searchArray.Count - 1


            searchItem = searchArray.Item(x)
            'MsgBox(searchItem.ProductName)
            Dim lvitem As New ListViewItem
            lvitem.Text = x + 1
            lvitem.SubItems.Add(searchItem.StockQty)
            lvitem.SubItems.Add(searchItem.ProductName)
            lvitem.SubItems.Add(searchItem.ProductId)
            lvitem.SubItems.Add("$ " & (FormatNumber(searchItem.Price, 2)))
            frmAddStock.ListView2.Items.Add(lvitem)
            'txtkeypadtotal.Text = ""
            'totalAmt = totalAmt + 2.05
        Next
        frmAddStock.ShowDialog()



    End Sub


    Private Sub btnDiscount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDiscount.Click
        Dim userMsg As String
        Dim num As Double
        userMsg = InputBox("Enter Amount", "Enter Discount Amount", 0)
        Try
            num = userMsg
            frmMain.discount = num
            frmMain.lblDiscountAmnt.Text = num
            If num > 0 Then
                frmMain.lblDiscountAmnt.Visible = True
                frmMain.lblDiscount.Visible = True
                frmMain.Pricing()

            Else
                frmMain.lblDiscountAmnt.Visible = False
                frmMain.lblDiscount.Visible = False
            End If
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub btnDrawer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrawer.Click
        'frmMain.prtCashDrawer()
        frmMain.cashDrawerOpen()
    End Sub

    Private Sub btnDailySale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDailySale.Click
        Dim today, tommrow As Date
        Dim day1, day2 As String
        Dim retbill As New CGDsql

        today = Now.Date
        tommrow = today.AddDays(1)

        day1 = today.ToString("yyyy-MM-dd") & " 06:00:00"
        day2 = tommrow.ToString("yyyy-MM-dd") & " 06:00:00"

        getsales(retbill.getTodaySale(day1, day2), retbill.getSalesTotal(day1, day2))

    End Sub

    Private Sub btnMonthlySale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMonthlySale.Click
        frmDateChoose.btnReport.Visible = True
        frmDateChoose.Button2.Visible = True
        frmDateChoose.btnOk.Visible = False
        frmDateChoose.ShowDialog()
    End Sub
    Public Sub getsales(ByVal listArray As ArrayList, ByVal paymentType As orders)
        frmReport.ListView1.Clear()

        frmReport.ListView1.Columns.Add("OrderID", 60)
        frmReport.ListView1.Columns.Add("Subtotal", 70)
        frmReport.ListView1.Columns.Add("HST", 50)
        frmReport.ListView1.Columns.Add("Discount", 70)
        frmReport.ListView1.Columns.Add("Total", 70)
        frmReport.ListView1.Columns.Add("Date", 160)
        'ListView1.f
        frmReport.ListView1.View = View.Details
        'today = DateTimePicker1.Value.Date

        'Dim totalOrder As New orders
        Dim bill As ArrayList = listArray
        For x = 0 To bill.Count - 1
            Dim biOrders As New orders
            biOrders = bill.Item(x)

            Dim lvItem As New ListViewItem
            lvItem.Text = biOrders.OrderId
            lvItem.SubItems.Add(FormatNumber(biOrders.subtotal, 2))
            lvItem.SubItems.Add(FormatNumber(biOrders.hst, 2))
            lvItem.SubItems.Add(FormatNumber(biOrders.discount, 2))
            lvItem.SubItems.Add(FormatNumber(biOrders.total, 2))
            lvItem.SubItems.Add((biOrders.dateDay))
            'lvItem.SubItems.Add(billnum.ProductName)
            'lvItem.SubItems.Add("$ " & (FormatNumber(billnum.Price * billnum.qty, 2)))
            frmReport.ListView1.Items.Add(lvItem)

            'totalOrder.discount += biOrders.discount
            'totalOrder.hst += biOrders.hst
            'totalOrder.subtotal += biOrders.subtotal
            'totalOrder.total += biOrders.total
            'totalOrder.master += biOrders.master
            'totalOrder.cash += biOrders.cash
            'totalOrder.visa += biOrders.visa
            'totalOrder.amex += biOrders.amex
            'totalOrder.other += biOrders.other
            'totalOrder.debit += biOrders.debit
            'totalOrder.GiftCert += biOrders.GiftCert
            'totalOrder.StoreCredit += biOrders.StoreCredit


        Next
        If bill.Count > 1 Then
            Dim orderdate1 As New orders
            orderdate1 = bill.Item(0)

            Dim orderdate2 As New orders
            orderdate2 = bill.Item(bill.Count - 1)


        End If
        frmReport.Label1.Text = ""

        frmReport.Label1.Text = ("Net Total: " & vbTab & vbTab & paymentType.subtotal & vbNewLine &
                                 "Total HST : " & vbTab & vbTab & paymentType.hst & vbNewLine &
                                 "Total Discount : " & vbTab & vbTab & paymentType.discount & vbNewLine &
                                 "Grand Total : " & vbTab & vbTab & paymentType.total)
        frmReport.printReportRange.Subtotal = paymentType.subtotal
        frmReport.printReportRange.HST = paymentType.hst
        frmReport.printReportRange.Total = paymentType.total
        frmReport.printReportRange.cash = paymentType.cash
        frmReport.printReportRange.visa = paymentType.visa
        frmReport.printReportRange.master = paymentType.master
        frmReport.printReportRange.amex = paymentType.amex
        frmReport.printReportRange.other = paymentType.other
        frmReport.printReportRange.debit = paymentType.debit
        frmReport.printReportRange.GiftCert = paymentType.GiftCert
        frmReport.printReportRange.StoreCredit = paymentType.StoreCredit
        frmReport.printReportRange.discAmount = paymentType.discount



        frmReport.ShowDialog()
    End Sub

    Private Sub btnexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnexit.Click
        End
    End Sub

    Private Sub btnDayCls_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDayCls.Click

        Dim reply As DialogResult = MessageBox.Show("Would you like to Close day now", "Coin Tail Pos",
             MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
        If reply = DialogResult.Yes Then
            frmMain.cashDrawerOpen()
            frmCashCount.ShowDialog()
            Application.Exit()

        End If







    End Sub

    Private Sub btnChgPassword_Click(sender As Object, e As EventArgs) Handles btnChgPassword.Click
        Dim sqlCon As New CGDsql

        frmKeyPad.lblstatus.Text = "Enter Current Password"
        frmKeyPad.txtQuant.PasswordChar = "•"
        frmKeyPad.txtQuant.Text = ""
        frmKeyPad.ShowDialog()
        Dim value As String = frmKeyPad.txtQuant.Text

        If value = frmMain.password Then
            frmKeyPad.lblstatus.Text = "Enter New Password"
            frmKeyPad.txtQuant.PasswordChar = "•"
            frmKeyPad.txtQuant.Text = ""
            frmKeyPad.ShowDialog()
            Dim newpassword As String = frmKeyPad.txtQuant.Text
            If (newpassword.Length > 0 And (Not newpassword.Equals("0"))) Then
                sqlCon.updateSettingValue(1, newpassword)
                frmMain.password = newpassword
            Else
                frmMain.lblstatus.Text = "Password not changed please choose a new password."
            End If
        Else
            MsgBox("Incorrect Password")
        End If
    End Sub

    Private Sub btnBarCode_Click(sender As Object, e As EventArgs) Handles btnBarCode.Click
        'Process.Start("barcode.exe")
        frmBarCodes.ShowDialog()

    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        frmSettings.ShowDialog()

    End Sub

    Private Sub btnSoldItems_Click(sender As Object, e As EventArgs) Handles btnSoldItems.Click
        frmDateChoose.btnReport.Visible = False
        frmDateChoose.Button2.Visible = False
        frmDateChoose.btnOk.Visible = True
        frmDateChoose.ShowDialog()

        frmAddStock.btnPrint.Visible = True

        frmAddStock.txtqtyIncrease.Text = ""
        frmAddStock.txtSearch.Text = ""
        frmAddStock.ListView2.Clear()
        frmAddStock.Label1.Visible = False
        frmAddStock.lblName.Visible = False
        frmAddStock.lblPrID.Visible = False
        frmAddStock.btndelet.Visible = False
        frmAddStock.btnUpdate.Visible = False
        frmAddStock.txtqtyIncrease.Visible = False
        frmAddStock.txtSearch.Visible = False

        Dim txtser As New CGDsql
        Dim searchArray As ArrayList
        Dim searchItem As New ProductItem
        searchArray = txtser.getSoldItems(frmDateChoose.date1, frmDateChoose.date2)
        'MsgBox(txtSearch.Text & " array size is " & searchArray.Count)

        frmAddStock.ListView2.Items.Clear()

        For x = 0 To searchArray.Count - 1


            searchItem = searchArray.Item(x)
            'MsgBox(searchItem.ProductName)
            Dim lvitem As New ListViewItem
            lvitem.Text = x + 1
            lvitem.SubItems.Add(searchItem.StockQty)
            lvitem.SubItems.Add(searchItem.ProductName)
            lvitem.SubItems.Add(searchItem.ProductId)
            lvitem.SubItems.Add("$ " & (FormatNumber(searchItem.Price, 2)))
            frmAddStock.ListView2.Items.Add(lvitem)
            'txtkeypadtotal.Text = ""
            'totalAmt = totalAmt + 2.05
        Next
        frmAddStock.ShowDialog()

    End Sub

    Private Sub btnUpdatedItem_Click(sender As Object, e As EventArgs) Handles btnUpdatedItem.Click
        frmDateChoose.btnReport.Visible = False
        frmDateChoose.Button2.Visible = False
        frmDateChoose.btnOk.Visible = True
        frmDateChoose.ShowDialog()

        frmAddStock.btnPrint.Visible = True

        frmAddStock.txtqtyIncrease.Text = ""
        frmAddStock.txtSearch.Text = ""
        frmAddStock.ListView2.Clear()
        frmAddStock.Label1.Visible = False
        frmAddStock.lblName.Visible = False
        frmAddStock.lblPrID.Visible = False
        frmAddStock.btndelet.Visible = False
        frmAddStock.btnUpdate.Visible = False
        frmAddStock.txtqtyIncrease.Visible = False
        frmAddStock.txtSearch.Visible = False

        Dim txtser As New CGDsql
        Dim searchArray As ArrayList
        Dim searchItem As New ProductItem
        searchArray = txtser.getUpdatedItems(frmDateChoose.date1, frmDateChoose.date2)
        'MsgBox(txtSearch.Text & " array size is " & searchArray.Count)

        frmAddStock.ListView2.Items.Clear()

        For x = 0 To searchArray.Count - 1


            searchItem = searchArray.Item(x)
            'MsgBox(searchItem.ProductName)
            Dim lvitem As New ListViewItem
            lvitem.Text = x + 1
            lvitem.SubItems.Add(searchItem.StockQty)
            lvitem.SubItems.Add(searchItem.ProductName)
            lvitem.SubItems.Add(searchItem.ProductId)
            lvitem.SubItems.Add("$ " & (FormatNumber(searchItem.Price, 2)))
            frmAddStock.ListView2.Items.Add(lvitem)
            'txtkeypadtotal.Text = ""
            'totalAmt = totalAmt + 2.05
        Next
        frmAddStock.ShowDialog()
    End Sub

    Private Sub btnCategUpdate_Click(sender As Object, e As EventArgs) Handles btnCategUpdate.Click
        frmModifyCateg.ShowDialog()

    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        frmMain.pnlButtons.BringToFront()
        frmMain.pnlButtons.Controls.Clear()
        frmMain.btnVoid.Enabled = False
        frmMain.btn_Qty.Enabled = False
        frmMain.btnPreSet.Enabled = False
        frmMain.btnSpcial.Enabled = False
        frmMain.btnKeypad.Enabled = False
        frmMain.btnSearch.Enabled = False
        frmMain.btnOptions.Enabled = False
        frmMain.btnPrint.Text = "Log In"
        frmMain.lblUser.Tag = ""
        frmMain.lblUser.Text = ""
        Me.Close()

    End Sub
End Class