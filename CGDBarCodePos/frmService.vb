﻿Public Class frmService
    Public customer As service
    Public dbupdate As New CGDsql

    Private Sub btnRpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRpt.Click
        If txtCustomerID.Text.Length < 3 Then
            MsgBox("no customer found")
        Else
            frmCreteService.ShowDialog()
        End If



    End Sub

    Private Sub btnNewCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewCustomer.Click
        customer.cusName = txtName.Text
        customer.cusAddress = txtAddress.Text
        customer.cusCity = txtCity.Text
        customer.cusEmail = txtEmail.Text
        customer.cusPhone = txtPhone.Text
        customer.cusPosCode = txtPostal.Text
        customer.cusProvince = txtProvince.Text
        If txtName.Text.Length > 2 And txtPhone.Text.Length > 6 And txtEmail.Text.Length > 4 Then
            dbupdate.addCustomer(customer)
            customer.cusID = dbupdate.getLastCustomer
            customer.serID = (dbupdate.getLastService + 1)
            customer.barcode = frmMain.storeInitial + customer.cusID.ToString + customer.serID.ToString
            frmCreteService.ShowDialog()


        Else
            MsgBox("Please Check Email, Phone, Name field")
        End If
    End Sub
End Class