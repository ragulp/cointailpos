Option Strict Off
Public Class frmBarCodes


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            EaN13Barcode1.Value = TextBox1.Text
            'MessageBox.Show("The Check Sum is : " + EaN13Barcode1.CheckSum.ToString)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If (ColorDialog1.ShowDialog = Windows.Forms.DialogResult.OK) Then
            Button2.BackColor = ColorDialog1.Color
            EaN13Barcode1.BackColor = ColorDialog1.Color
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (ColorDialog1.ShowDialog = Windows.Forms.DialogResult.OK) Then
            Button3.BackColor = ColorDialog1.Color
            EaN13Barcode1.ForeColor = ColorDialog1.Color
        End If
    End Sub

    Private Sub chbShowText_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chbShowText.CheckedChanged
        EaN13Barcode1.ShowBarcodeText = chbShowText.Checked
    End Sub

    Dim pageNumber As Integer = 1
    Dim numberOfPages As Integer = 1
    Dim i As Integer = 0

    Private Sub PrintDocument1_PrintPage(sender As System.Object, e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        numberOfPages = Integer.Parse(nudPages.Value.ToString())
        Dim x As Integer = 20
        Dim y As Integer = -23

        For j As Integer = 0 To 1
            'For m As Integer = 0 To 6

            Dim drawRect As New Rectangle(x, y, EaN13Barcode1.Width / 3, EaN13Barcode1.Height)
            EaN13Barcode1.PrintToGraphics(e.Graphics, drawRect)
            'x = x + EaN13Barcode1.Width
            'y = y + EaN13Barcode1.Height
            'Next
            x = x + (EaN13Barcode1.Width / 2) + 15
            'y = 20
        Next


        If (pageNumber < numberOfPages) Then
            e.HasMorePages = True
            i = i + 1
            pageNumber = pageNumber + 1
        Else
            e.HasMorePages = False
        End If

    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        If PrintDialog1.ShowDialog() = DialogResult.OK Then
            PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
            PrintDocument1.Print()
        End If

    End Sub

    Private Sub btnPreview_Click(sender As System.Object, e As System.EventArgs) Handles btnPreview.Click
        PrintPreviewDialog1 = New PrintPreviewDialog()
        PrintPreviewDialog1.Document = PrintDocument1
        PrintPreviewDialog1.Show()
    End Sub

    Private Sub chbShowCheckSum_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chbShowCheckSum.CheckedChanged
        EaN13Barcode1.ShowCheckSum = chbShowCheckSum.Checked
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        EaN13Barcode1.ShowBarcodeText = True
        EaN13Barcode1.ShowCheckSum = chbShowCheckSum.Checked
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
