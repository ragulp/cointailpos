﻿Option Strict Off
Public Class frmChgPrice
    Dim showitem As BillItems

    Dim prices As costNdPrice

    Private Sub frmChgPrice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim take1 As New CGDsql


        showitem = frmMain.billList.Item(frmMain.ListView1.SelectedItems.Item(0).Text - 1)
        Label1.Text = showitem.ProductName + " " + showitem.Price.ToString + " " + showitem.ProductId.ToString
        prices = (take1.getCostPrice(showitem.ProductId))
        Label2.Text = prices.costPrice
        rbDollar.Select()
        txtPercent.Text = showitem.Price
        txtRemark.Text = showitem.txt

        showitem.Price = prices.price

        ListView1.Columns.Add("Item", 190)
        ListView1.Columns.Add("Price", 60)
        'ListView1.f
        ListView1.Items.Clear()

        ListView1.View = View.Details
        'Dim lvItem As New ListViewItem

        Dim billHistory As ArrayList
        billHistory = take1.getSoldHistory(showitem.ProductId)


        For x = 0 To billHistory.Count - 1
            Dim billHisItems As New BillItems
            billHisItems = billHistory.Item(x)
            Dim lvItem As New ListViewItem
            lvItem.Text = billHisItems.ProductName
            lvItem.SubItems.Add(billHisItems.Price)

            ListView1.Items.Add(lvItem)
        Next

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim price1, price2, finalPrice As Double

        If txtPercent.Text.Length = 0 And txtRemark.Text.Length = 0 Then
            MsgBox("Please Enter Valid Data")
        ElseIf txtPercent.Text.Length = 0 Then
            showitem.txt = txtRemark.Text
            frmMain.billList.RemoveAt(frmMain.ListView1.SelectedItems.Item(0).Text - 1)
            frmMain.billList.Add(showitem)
            frmMain.Pricing()
            Me.Close()

        Else
            price1 = (showitem.Price * txtPercent.Text / 100)
            price2 = txtPercent.Text

            If rbDollar.Checked Then
                finalPrice = price2
            ElseIf rbPer.Checked Then
                finalPrice = showitem.Price - price1
            End If
            showitem.Price = finalPrice
            showitem.txt = txtRemark.Text
            If finalPrice < prices.costPrice Then
                MsgBox("Priec Exceed Cost Price")
            Else
                frmMain.billList.RemoveAt(frmMain.ListView1.SelectedItems.Item(0).Text - 1)
                frmMain.billList.Add(showitem)
                frmMain.Pricing()
                Me.Close()
            End If

        End If





    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub


End Class