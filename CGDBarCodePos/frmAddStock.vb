﻿Option Strict Off
Public Class frmAddStock



    Private Sub frmAddStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListView2.Columns.Add("#", 35)
        ListView2.Columns.Add("inHand", 40)
        ListView2.Columns.Add("Item", 435)
        ListView2.Columns.Add("ID", 45)
        ListView2.Columns.Add("Price", 80)
        'ListView1.f
        ListView2.View = View.Details
        lblsts.Text = "___"
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text.Length >= 3 Then

            lblsts.Text = "___"
            Dim txtser As New CGDsql
            Dim searchArray As ArrayList
            Dim searchItem As New ProductItem
            searchArray = txtser.searchByKeyword(txtSearch.Text)
            'MsgBox(txtSearch.Text & " array size is " & searchArray.Count)

            ListView2.Items.Clear()

            For x = 0 To searchArray.Count - 1


                searchItem = searchArray.Item(x)
                'MsgBox(searchItem.ProductName)
                Dim lvitem As New ListViewItem
                lvitem.Text = x + 1
                lvitem.SubItems.Add(searchItem.StockQty)
                lvitem.SubItems.Add(searchItem.ProductName)
                lvitem.SubItems.Add(searchItem.ProductId)
                lvitem.SubItems.Add("$ " & (FormatNumber(searchItem.Price, 2)))
                ListView2.Items.Add(lvitem)
                'txtkeypadtotal.Text = ""
                'totalAmt = totalAmt + 2.05
            Next

        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim stUpd As New CGDsql
        Dim productId, stock_qty As Integer
        Try
            productId = lblPrID.Text
            stock_qty = txtqtyIncrease.Text

            stUpd.updateQuantity(productId, stock_qty)
            stUpd.addInventoryLog(productId, stock_qty)
            lblsts.Text = "Sucessfully Updated"
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub ListView2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView2.SelectedIndexChanged
        If ListView2.SelectedItems.Count > 0 Then
            lblsts.Text = "___"
            lblPrID.Text = ListView2.SelectedItems(0).SubItems(3).Text
        End If

    End Sub

    Private Sub btndelet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelet.Click
        Dim stUpd As New CGDsql
        Dim productId As Integer
        Try
            productId = lblPrID.Text


            stUpd.updateQuantity(productId)
            lblsts.Text = "Sucessfully Deleted"
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub
    Private Sub ListView2_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListView2.MouseDoubleClick
        Dim prodID As Integer = (ListView2.SelectedItems(0).SubItems(3).Text)
        Dim getProduct As New CGDsql
        Dim product As ProductItem
        product = getProduct.getProductItemEdit(prodID)
        frmAddProduct.lblProdID.Text = prodID
        frmAddProduct.txtName.Text = product.ProductName
        frmAddProduct.txtManufacture.Text = product.ManufactureName
        frmAddProduct.txtBarcode.Text = product.BarCode
        frmAddProduct.txtBarcode2.Text = product.SecondBarCode
        frmAddProduct.txtPrice.Text = product.Price
        frmAddProduct.txtCost.Text = product.CostPrice
        frmAddProduct.txtQty.Text = product.StockQty
        frmAddProduct.txtDesc.Text = product.Description
        frmAddProduct.cmbRemark.Text = product.remark
        frmAddProduct.txteof.Text = product.eof
        frmAddProduct.lblCategory.Text = product.Category
        If product.Refurb Then
            frmAddProduct.chkRefurb.CheckState = CheckState.Checked
        Else
            frmAddProduct.chkRefurb.CheckState = CheckState.Unchecked
        End If
        If product.Taxable Then
            frmAddProduct.chkTaxable.CheckState = CheckState.Checked
        Else
            frmAddProduct.chkTaxable.CheckState = CheckState.Unchecked
        End If



        'frmAddProduct.txtCateg.SelectedIndex = product.Category


        frmAddProduct.btnAdd.Visible = False
        frmAddProduct.btnUpdate.Visible = True
        frmAddProduct.txtQty.ReadOnly = True
        frmAddProduct.ShowDialog()



    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Dim P As New PrinterClass(Application.StartupPath)

        With P
            'Printing Logo
            .RTL = False
            '.PrintLogo()

            .AlignCenter()
            .BigFont()
            '.Bold = True
            .WriteLine(frmMain.storName)
            If (frmMain.storName2.Length() > 0) Then
                .WriteLine(frmMain.storName2)
            End If
            .WriteLine("Inventory")
            .NormalFont()

            .AlignLeft()

            'Printing Date
            .FeedPaper(1)
            '.GotoSixth(1)
            .WriteLine("   Date:  " & DateTime.Now.ToString)
            .WriteLine("")
            .WriteLine("------------------------------------------------------------------------------------------")
            .GotoSixth(1)
            .WriteChars("  Qty")
            .GotoCol(6)
            .WriteChars("Item")
            .GotoSixth(9)
            .WriteChars("Price")
            .WriteLine("")
            .WriteLine("------------------------------------------------------------------------------------------")

            For x = 0 To ListView2.Items.Count - 1
                .GotoSixth(1)
                Dim intqty As Integer = ListView2.Items(x).SubItems(1).Text
                If intqty >= 100 Then
                    .WriteChars(" " + ListView2.Items(x).SubItems(1).Text)
                ElseIf intqty >= 10 Then
                    .WriteChars("   " + ListView2.Items(x).SubItems(1).Text)
                Else
                    .WriteChars("     " + ListView2.Items(x).SubItems(1).Text)
                End If

                .GotoCol(6)
                .WriteChars(ListView2.Items(x).SubItems(2).Text)
                .GotoSixth(8)
                Dim price As Double = ListView2.Items(x).SubItems(4).Text
                Dim upriceStr As String
                .AlignRight()
                upriceStr = price.ToString("####0.00      -")

                .WriteLine(upriceStr)
                '.WriteLine("")
            Next
            .AlignCenter()
            .WriteLine("End Report!")
            .WriteLine("")
            .CutPaper() ' Can be used with real printer to cut the paper.
            .EndDoc()
        End With
    End Sub

    Private Sub pnlSearch_Paint(sender As Object, e As PaintEventArgs) Handles pnlSearch.Paint

    End Sub
End Class