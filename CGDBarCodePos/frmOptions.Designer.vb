﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAddItem = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.btnDiscount = New System.Windows.Forms.Button()
        Me.btnAddStock = New System.Windows.Forms.Button()
        Me.btnInventory = New System.Windows.Forms.Button()
        Me.btnDailySale = New System.Windows.Forms.Button()
        Me.btnMonthlySale = New System.Windows.Forms.Button()
        Me.btnDele = New System.Windows.Forms.Button()
        Me.btnDrawer = New System.Windows.Forms.Button()
        Me.btnexit = New System.Windows.Forms.Button()
        Me.btnDayCls = New System.Windows.Forms.Button()
        Me.btnChgPassword = New System.Windows.Forms.Button()
        Me.btnBarCode = New System.Windows.Forms.Button()
        Me.btnSettings = New System.Windows.Forms.Button()
        Me.btnSoldItems = New System.Windows.Forms.Button()
        Me.btnUpdatedItem = New System.Windows.Forms.Button()
        Me.btnCategUpdate = New System.Windows.Forms.Button()
        Me.btnLogOut = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnAddItem
        '
        Me.btnAddItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddItem.Location = New System.Drawing.Point(12, 53)
        Me.btnAddItem.Name = "btnAddItem"
        Me.btnAddItem.Size = New System.Drawing.Size(106, 57)
        Me.btnAddItem.TabIndex = 0
        Me.btnAddItem.Text = "Add Items"
        Me.btnAddItem.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(236, 378)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(106, 57)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(135, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(71, 20)
        Me.lblTitle.TabIndex = 13
        Me.lblTitle.Text = "Options"
        '
        'btnDiscount
        '
        Me.btnDiscount.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDiscount.Location = New System.Drawing.Point(124, 53)
        Me.btnDiscount.Name = "btnDiscount"
        Me.btnDiscount.Size = New System.Drawing.Size(106, 57)
        Me.btnDiscount.TabIndex = 14
        Me.btnDiscount.Text = "Discount"
        Me.btnDiscount.UseVisualStyleBackColor = True
        '
        'btnAddStock
        '
        Me.btnAddStock.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddStock.Location = New System.Drawing.Point(12, 189)
        Me.btnAddStock.Name = "btnAddStock"
        Me.btnAddStock.Size = New System.Drawing.Size(106, 57)
        Me.btnAddStock.TabIndex = 15
        Me.btnAddStock.Text = "Update Stock"
        Me.btnAddStock.UseVisualStyleBackColor = True
        '
        'btnInventory
        '
        Me.btnInventory.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInventory.Location = New System.Drawing.Point(12, 252)
        Me.btnInventory.Name = "btnInventory"
        Me.btnInventory.Size = New System.Drawing.Size(106, 57)
        Me.btnInventory.TabIndex = 16
        Me.btnInventory.Text = "View Stock"
        Me.btnInventory.UseVisualStyleBackColor = True
        '
        'btnDailySale
        '
        Me.btnDailySale.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDailySale.Location = New System.Drawing.Point(236, 53)
        Me.btnDailySale.Name = "btnDailySale"
        Me.btnDailySale.Size = New System.Drawing.Size(106, 57)
        Me.btnDailySale.TabIndex = 17
        Me.btnDailySale.Text = "Today Sale"
        Me.btnDailySale.UseVisualStyleBackColor = True
        '
        'btnMonthlySale
        '
        Me.btnMonthlySale.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMonthlySale.Location = New System.Drawing.Point(236, 121)
        Me.btnMonthlySale.Name = "btnMonthlySale"
        Me.btnMonthlySale.Size = New System.Drawing.Size(106, 57)
        Me.btnMonthlySale.TabIndex = 18
        Me.btnMonthlySale.Text = "Date Range Sale"
        Me.btnMonthlySale.UseVisualStyleBackColor = True
        '
        'btnDele
        '
        Me.btnDele.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDele.Location = New System.Drawing.Point(12, 121)
        Me.btnDele.Name = "btnDele"
        Me.btnDele.Size = New System.Drawing.Size(106, 57)
        Me.btnDele.TabIndex = 19
        Me.btnDele.Text = "Delete Item"
        Me.btnDele.UseVisualStyleBackColor = True
        '
        'btnDrawer
        '
        Me.btnDrawer.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrawer.Location = New System.Drawing.Point(124, 121)
        Me.btnDrawer.Name = "btnDrawer"
        Me.btnDrawer.Size = New System.Drawing.Size(106, 57)
        Me.btnDrawer.TabIndex = 20
        Me.btnDrawer.Text = "Open Cash Drawer"
        Me.btnDrawer.UseVisualStyleBackColor = True
        '
        'btnexit
        '
        Me.btnexit.Location = New System.Drawing.Point(12, 409)
        Me.btnexit.Name = "btnexit"
        Me.btnexit.Size = New System.Drawing.Size(82, 26)
        Me.btnexit.TabIndex = 21
        Me.btnexit.Text = "Exit"
        Me.btnexit.UseVisualStyleBackColor = True
        '
        'btnDayCls
        '
        Me.btnDayCls.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDayCls.Location = New System.Drawing.Point(124, 252)
        Me.btnDayCls.Name = "btnDayCls"
        Me.btnDayCls.Size = New System.Drawing.Size(106, 57)
        Me.btnDayCls.TabIndex = 22
        Me.btnDayCls.Text = "Close Day"
        Me.btnDayCls.UseVisualStyleBackColor = True
        '
        'btnChgPassword
        '
        Me.btnChgPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChgPassword.Location = New System.Drawing.Point(124, 189)
        Me.btnChgPassword.Name = "btnChgPassword"
        Me.btnChgPassword.Size = New System.Drawing.Size(106, 57)
        Me.btnChgPassword.TabIndex = 23
        Me.btnChgPassword.Text = "Change Password"
        Me.btnChgPassword.UseVisualStyleBackColor = True
        '
        'btnBarCode
        '
        Me.btnBarCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBarCode.Location = New System.Drawing.Point(236, 189)
        Me.btnBarCode.Name = "btnBarCode"
        Me.btnBarCode.Size = New System.Drawing.Size(106, 57)
        Me.btnBarCode.TabIndex = 24
        Me.btnBarCode.Text = "Bar Code"
        Me.btnBarCode.UseVisualStyleBackColor = True
        '
        'btnSettings
        '
        Me.btnSettings.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSettings.Location = New System.Drawing.Point(236, 252)
        Me.btnSettings.Name = "btnSettings"
        Me.btnSettings.Size = New System.Drawing.Size(106, 57)
        Me.btnSettings.TabIndex = 25
        Me.btnSettings.Text = "Advance Settings"
        Me.btnSettings.UseVisualStyleBackColor = True
        '
        'btnSoldItems
        '
        Me.btnSoldItems.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSoldItems.Location = New System.Drawing.Point(12, 315)
        Me.btnSoldItems.Name = "btnSoldItems"
        Me.btnSoldItems.Size = New System.Drawing.Size(106, 57)
        Me.btnSoldItems.TabIndex = 26
        Me.btnSoldItems.Text = "Sold Items"
        Me.btnSoldItems.UseVisualStyleBackColor = True
        '
        'btnUpdatedItem
        '
        Me.btnUpdatedItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdatedItem.Location = New System.Drawing.Point(124, 315)
        Me.btnUpdatedItem.Name = "btnUpdatedItem"
        Me.btnUpdatedItem.Size = New System.Drawing.Size(106, 57)
        Me.btnUpdatedItem.TabIndex = 27
        Me.btnUpdatedItem.Text = "Updated Items"
        Me.btnUpdatedItem.UseVisualStyleBackColor = True
        '
        'btnCategUpdate
        '
        Me.btnCategUpdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCategUpdate.Location = New System.Drawing.Point(236, 315)
        Me.btnCategUpdate.Name = "btnCategUpdate"
        Me.btnCategUpdate.Size = New System.Drawing.Size(106, 57)
        Me.btnCategUpdate.TabIndex = 28
        Me.btnCategUpdate.Text = "Update Category"
        Me.btnCategUpdate.UseVisualStyleBackColor = True
        '
        'btnLogOut
        '
        Me.btnLogOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogOut.Location = New System.Drawing.Point(124, 378)
        Me.btnLogOut.Name = "btnLogOut"
        Me.btnLogOut.Size = New System.Drawing.Size(106, 57)
        Me.btnLogOut.TabIndex = 29
        Me.btnLogOut.Text = "Log Out"
        Me.btnLogOut.UseVisualStyleBackColor = True
        '
        'frmOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(357, 444)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnLogOut)
        Me.Controls.Add(Me.btnCategUpdate)
        Me.Controls.Add(Me.btnUpdatedItem)
        Me.Controls.Add(Me.btnSoldItems)
        Me.Controls.Add(Me.btnSettings)
        Me.Controls.Add(Me.btnBarCode)
        Me.Controls.Add(Me.btnChgPassword)
        Me.Controls.Add(Me.btnDayCls)
        Me.Controls.Add(Me.btnexit)
        Me.Controls.Add(Me.btnDrawer)
        Me.Controls.Add(Me.btnDele)
        Me.Controls.Add(Me.btnMonthlySale)
        Me.Controls.Add(Me.btnDailySale)
        Me.Controls.Add(Me.btnInventory)
        Me.Controls.Add(Me.btnAddStock)
        Me.Controls.Add(Me.btnDiscount)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAddItem)
        Me.Name = "frmOptions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAddItem As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnDiscount As System.Windows.Forms.Button
    Friend WithEvents btnAddStock As System.Windows.Forms.Button
    Friend WithEvents btnInventory As System.Windows.Forms.Button
    Friend WithEvents btnDailySale As System.Windows.Forms.Button
    Friend WithEvents btnMonthlySale As System.Windows.Forms.Button
    Friend WithEvents btnDele As System.Windows.Forms.Button
    Friend WithEvents btnDrawer As System.Windows.Forms.Button
    Friend WithEvents btnexit As System.Windows.Forms.Button
    Friend WithEvents btnDayCls As System.Windows.Forms.Button
    Friend WithEvents btnChgPassword As System.Windows.Forms.Button
    Friend WithEvents btnBarCode As System.Windows.Forms.Button
    Friend WithEvents btnSettings As System.Windows.Forms.Button
    Friend WithEvents btnSoldItems As System.Windows.Forms.Button
    Friend WithEvents btnUpdatedItem As System.Windows.Forms.Button
    Friend WithEvents btnCategUpdate As System.Windows.Forms.Button
    Friend WithEvents btnLogOut As System.Windows.Forms.Button
End Class
