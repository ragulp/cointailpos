﻿Option Strict Off
Imports MySql.Data.MySqlClient
Public Class CGDsql

    'Public connstring As String = "server=" & "localhost" & ";" _
    '& "user id=" & "root" & ";" _
    '& "password=" & "123123" & ";" _
    '& "database=itecposc_itec"
    Public connstring As String = "server=localhost; user id=root; password=123123; database=itecposc_demo"
    'Public connstring As String = "server=38.132.37.221; user id=barcodeposdemo; password=barcodeposdemo; database=itecposc_demo"
    Public demoPeriodEnds As Date = New Date(2017, 2, 27)
    'Public connstring As String = "Server=38.132.37.221;Port=3306;Database=itecposc_itecnew;Uid=krish;Pwd=123123;"

    'Public connstring As String = "server=" & "itecpos.com" & ";" _
    '& "user id=" & "cointail_kural" & ";" _
    '& "password=" & "kural123" & ";" _
    ' & "database=cointail_itecomputers"

    'Public connstring As String = ("Data Source=ragulp-pc,1433;Network Library=DBMSSOCN;Integrated Security=False;database=Res_db;user id=sa;password=123456;connection timeout =120")
    'Public connstring As String = ("Data Source=Gillie-pc,1433;Network Library=DBMSSOCN;Integrated Security=False;database=Res_db;user id=sa;password=abcd1234;connection timeout =120")

    'Public connstring As String = ("Data Source=simcoe-svr\sqlexpress;Integrated Security=False;database=Res_db;user id=sa;password=123456;connection timeout =120")
    'Public connstring As String = ("Data Source=Tom-pc;Integrated Security=False;database=Res_db;user id=sa;password=123456;connection timeout =120")
    'Public connstring As String = ("Data Source= 66.49.143.25,1433\sqlexpress;Integrated Security=False;database=Res_db;user id=sa;password=123456;connection timeout =120")
    'Public connstring As String = ("Data Source=192.168.1.103;Network Library=DBMSSOCN;Initial Catalog=Res_db;User ID=sa;Password=123456;connection timeout =120")
    Public conn As MySqlConnection = New MySqlConnection(connstring)
    Public rdr As MySqlDataReader
    Dim buttonsArray As Button
   
    Public Sub addProduct(ByVal product As ProductItem)


        Dim InsertSQL As String = "Insert into products (ProductName, ProductBarCode, Price, Taxable, Stock_qty, ManufactureName, StoreBarCode, CostPrice, Category, Description, itemNew, ref, remark) Values (?ProductName, ?ProductBarCode, ?Price, ?Taxable, ?Stock_qty, ?ManufactureName, ?StoreBarCode, ?CostPrice, ?Category, ?Description, ?itemNew, ?ref, ?remark)"
        'Dim InsertSQL2 As String = "update bill set Quantity = Quantity+@P_Qty, mods = @mods where OrderID = @OrderID and FoodItem_ID = @FoodItem_ID "
        'ragul updated also the database string to varchar and set the type here also
        'Dim InsertSQL2 As String = "update bill set Quantity = Quantity+@P_Qty, mods= @mods, Voids=Voids + @Voids where OrderID = @OrderID and FoodItem_ID = @FoodItem_ID and Splitno = @splitno"
        Try
            'For j = 0 To Splitarray.Count - 1
            Dim items As New ProductItem
            'foodarray = Splitarray.Item(j)
            items = product
            'conn.Close()
            'MsgBox(items.ProductName)

            Dim cmd1 As MySqlCommand = New MySqlCommand(InsertSQL, conn)
            'add parameters to the command for statements
            cmd1.Parameters.Add("?ProductName", MySqlDbType.VarChar)
            cmd1.Parameters("?ProductName").Value = items.ProductName
            'cmd1.Parameters.Add("?ProductName", items.ProductName)

            cmd1.Parameters.Add("?ProductBarCode", MySqlDbType.VarChar)
            cmd1.Parameters("?ProductBarCode").Value = items.BarCode

            cmd1.Parameters.Add("?Price", MySqlDbType.Decimal)
            cmd1.Parameters("?Price").Value = items.Price

            cmd1.Parameters.Add("?Taxable", MySqlDbType.Bit)
            cmd1.Parameters("?Taxable").Value = items.Taxable

            cmd1.Parameters.Add("?Stock_qty", MySqlDbType.Int16)
            cmd1.Parameters("?Stock_qty").Value = items.StockQty

            cmd1.Parameters.Add("?ManufactureName", MySqlDbType.VarChar)
            cmd1.Parameters("?ManufactureName").Value = items.ManufactureName

            cmd1.Parameters.Add("?StoreBarCode", MySqlDbType.VarChar)
            cmd1.Parameters("?StoreBarCode").Value = items.SecondBarCode

            cmd1.Parameters.Add("?CostPrice", MySqlDbType.Decimal)
            cmd1.Parameters("?CostPrice").Value = items.CostPrice

            cmd1.Parameters.Add("?Category", MySqlDbType.VarChar)
            cmd1.Parameters("?Category").Value = items.Category

            cmd1.Parameters.Add("?Description", MySqlDbType.VarChar)
            cmd1.Parameters("?Description").Value = items.Description

            cmd1.Parameters.Add("?itemNew", MySqlDbType.Bit)
            cmd1.Parameters("?itemNew").Value = items.Refurb

            cmd1.Parameters.Add("?ref", MySqlDbType.Decimal)
            cmd1.Parameters("?ref").Value = items.eof

            cmd1.Parameters.Add("?remark", MySqlDbType.VarChar)
            cmd1.Parameters("?remark").Value = items.remark

            'cmd1.Parameters.Add("@mods", SqlDbType.VarChar, 50)
            'cmd1.Parameters("@mods").Value = mods

            'cmd1.Parameters.Add("@Splitno", SqlDbType.Int)
            'cmd1.Parameters("@Splitno").Value = j
            conn.Open()
            Try
                cmd1.ExecuteNonQuery()
            Catch ex As Exception

                MsgBox("Error Occurred: new bill" & ex.ToString)
            Finally
                ' Close connection
                conn.Close()
            End Try


            'End If
            'Next
            'Next
        Catch ex As Exception

            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            ' Close connection
            conn.Close()
        End Try

        'objFoodsArray.Clear()
        'ListView1.Items.Clear()
        'Pricing()
        'Catch ex1 As Exception
        '    report_error(ex1.ToString)
        ' End Try
    End Sub

    Public Sub updateProduct(ByVal product As ProductItem)


        Dim InsertSQL As String = "update products set ProductName = ?ProductName, ProductBarCode = ?ProductBarCode, Price = ?Price, Taxable = ?Taxable, Stock_qty = ?Stock_qty, ManufactureName = ?ManufactureName, StoreBarCode = ?StoreBarCode, CostPrice = ?CostPrice, Category = ?Category, Description = ?Description, itemNew = ?itemNew, ref = ?ref, remark = ?remark where ProductID = ?ProductID"
        'Dim InsertSQL2 As String = "update bill set Quantity = Quantity+@P_Qty, mods = @mods where OrderID = @OrderID and FoodItem_ID = @FoodItem_ID "
        'ragul updated also the database string to varchar and set the type here also
        'Dim InsertSQL2 As String = "update bill set Quantity = Quantity+@P_Qty, mods= @mods, Voids=Voids + @Voids where OrderID = @OrderID and FoodItem_ID = @FoodItem_ID and Splitno = @splitno"
        Try
            'For j = 0 To Splitarray.Count - 1
            Dim items As New ProductItem
            'foodarray = Splitarray.Item(j)
            items = product
            'conn.Close()
            'MsgBox(items.ProductName)

            Dim cmd1 As MySqlCommand = New MySqlCommand(InsertSQL, conn)
            'add parameters to the command for statements
            cmd1.Parameters.Add("?ProductName", MySqlDbType.VarChar)
            cmd1.Parameters("?ProductName").Value = items.ProductName
            'cmd1.Parameters.Add("?ProductName", items.ProductName)

            cmd1.Parameters.Add("?ProductBarCode", MySqlDbType.VarChar)
            cmd1.Parameters("?ProductBarCode").Value = items.BarCode

            cmd1.Parameters.Add("?Price", MySqlDbType.Decimal)
            cmd1.Parameters("?Price").Value = items.Price

            cmd1.Parameters.Add("?Taxable", MySqlDbType.Bit)
            cmd1.Parameters("?Taxable").Value = items.Taxable

            cmd1.Parameters.Add("?Stock_qty", MySqlDbType.Int16)
            cmd1.Parameters("?Stock_qty").Value = items.StockQty

            cmd1.Parameters.Add("?ManufactureName", MySqlDbType.VarChar)
            cmd1.Parameters("?ManufactureName").Value = items.ManufactureName

            cmd1.Parameters.Add("?StoreBarCode", MySqlDbType.VarChar)
            cmd1.Parameters("?StoreBarCode").Value = items.SecondBarCode

            cmd1.Parameters.Add("?CostPrice", MySqlDbType.Decimal)
            cmd1.Parameters("?CostPrice").Value = items.CostPrice

            cmd1.Parameters.Add("?Category", MySqlDbType.VarChar)
            cmd1.Parameters("?Category").Value = items.Category

            cmd1.Parameters.Add("?Description", MySqlDbType.VarChar)
            cmd1.Parameters("?Description").Value = items.Description

            cmd1.Parameters.Add("?itemNew", MySqlDbType.Bit)
            cmd1.Parameters("?itemNew").Value = items.Refurb

            cmd1.Parameters.Add("?ref", MySqlDbType.Decimal)
            cmd1.Parameters("?ref").Value = items.eof

            cmd1.Parameters.Add("?remark", MySqlDbType.VarChar)
            cmd1.Parameters("?remark").Value = items.remark

            cmd1.Parameters.Add("?ProductID", MySqlDbType.Int16)
            cmd1.Parameters("?ProductID").Value = items.ProductId

            'cmd1.Parameters.Add("@mods", SqlDbType.VarChar, 50)
            'cmd1.Parameters("@mods").Value = mods

            'cmd1.Parameters.Add("@Splitno", SqlDbType.Int)
            'cmd1.Parameters("@Splitno").Value = j
            conn.Open()
            Try
                cmd1.ExecuteNonQuery()
            Catch ex As Exception

                MsgBox("Error Occurred: new bill" & ex.ToString)
            Finally
                ' Close connection
                conn.Close()
            End Try


            'End If
            'Next
            'Next
        Catch ex As Exception

            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            ' Close connection
            conn.Close()
        End Try

        'objFoodsArray.Clear()
        'ListView1.Items.Clear()
        'Pricing()
        'Catch ex1 As Exception
        '    report_error(ex1.ToString)
        ' End Try
    End Sub
    Public Function searchProduct(ByVal productBarCode As String) As ProductItem

        Dim selItem As New ProductItem
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "Select ProductName, Price, Taxable, ProductID, remark, ref from products where ProductBarCode = ?productbarcode"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?ProductBarCode", MySqlDbType.VarChar, 30)
            cmd2.Parameters("?ProductBarCode").Value = productBarCode

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                selItem.ProductName = rdr(0)
                selItem.Price = rdr(1)
                selItem.Taxable = rdr(2)
                selItem.ProductId = rdr(3)
                selItem.remark = rdr(4)
                selItem.eof = rdr(5)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return selItem

    End Function
    Public Function searchByKeyword(ByVal keyword As String) As ArrayList

        'MsgBox("from sql" & keyword)
        Dim productlist As New ArrayList
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "Select ProductName, Price, Taxable, Stock_qty, ProductID, remark, ref from products where ProductName LIKE '%'  ?keyword  '%' or ProductBarCode=?keyword order by ProductName"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?keyword", MySqlDbType.VarChar)
            cmd2.Parameters("?keyword").Value = keyword

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                Dim selItem As New ProductItem
                selItem.ProductName = rdr(0)
                selItem.Price = rdr(1)
                selItem.Taxable = rdr(2)
                selItem.StockQty = rdr(3)
                selItem.ProductId = rdr(4)
                selItem.remark = rdr(5)
                selItem.eof = rdr(6)
                productlist.Add(selItem)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return productlist

    End Function
    Public Sub SqlserverQuery()

        Dim sql1 As String
        Dim prodName As String
        Dim prodPrice As Double
        Dim taxable As Boolean
        Dim productid As Integer

        sql1 = "Select ProductName, Price, Taxable, ProductID from products where ProductBarCode = 'NoBarcode'"
        frmMain.pnlButtons.AutoScroll = True
        Try
            conn.Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql1, conn)
            cmd.Prepare()
            rdr = cmd.ExecuteReader
            frmMain.pnlButtons.Controls.Clear()

            While (rdr.Read)
                prodName = rdr(0)
                prodPrice = rdr(1)
                taxable = rdr(2)
                productid = rdr(3)
                buttonsArray = New Button
                buttonsArray.Size = New Size(131, 58)
                buttonsArray.Text = prodName.Trim
                buttonsArray.Tag = prodPrice
                buttonsArray.AccessibleName = taxable
                buttonsArray.AccessibleDescription = productid
                buttonsArray.TextAlign = ContentAlignment.MiddleCenter

                buttonsArray.BackColor = Color.WhiteSmoke
                frmMain.pnlButtons.Controls.Add(buttonsArray)
                AddHandler buttonsArray.Click, AddressOf mybutton_Click

            End While

            rdr.Close()


        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

    End Sub

    Public Sub getSpecials()

        Dim sql1 As String
        Dim prodName As String
        Dim prodPrice As Double
        Dim taxable As Boolean
        Dim productid As Integer

        sql1 = "Select ProductName, StoreBarCode, Taxable, ProductID from products where ProductBarCode = 'NoBarcode' AND itemNew = 1"
        frmMain.pnlButtons.AutoScroll = True
        Try
            conn.Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql1, conn)
            cmd.Prepare()
            rdr = cmd.ExecuteReader
            frmMain.pnlButtons.Controls.Clear()

            While (rdr.Read)
                prodName = rdr(0)
                prodPrice = rdr(1)
                taxable = rdr(2)
                productid = rdr(3)
                buttonsArray = New Button
                buttonsArray.Size = New Size(131, 58)
                buttonsArray.Text = prodName.Trim
                buttonsArray.Tag = prodPrice
                buttonsArray.AccessibleName = taxable
                buttonsArray.AccessibleDescription = productid
                buttonsArray.TextAlign = ContentAlignment.MiddleCenter

                buttonsArray.BackColor = Color.WhiteSmoke
                frmMain.pnlButtons.Controls.Add(buttonsArray)
                AddHandler buttonsArray.Click, AddressOf mybutton_Click

            End While

            rdr.Close()


        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

    End Sub

    Private Sub mybutton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MsgBox("hi it works")
        Dim thisbutton As Button = DirectCast(sender, Button)
        Dim billitem As BillItems
        billitem.Price = thisbutton.Tag
        billitem.ProductId = thisbutton.AccessibleDescription
        billitem.ProductName = thisbutton.Text
        billitem.qty = 1
        billitem.txt = ""
        frmMain.billList.Add(billitem)

        frmMain.Pricing()

    End Sub
    Public Sub updateQuantity(ByVal ProductID As Integer, ByVal Stock_qty As Integer)


        Dim InsertSQL As String = "update products set Stock_qty = Stock_qty+?Stock_qty where ProductID =?ProductID "
         Try
            conn.Open()
            Dim cmd1 As MySqlCommand = New MySqlCommand(InsertSQL, conn)
            cmd1.Parameters.Add("?ProductID", MySqlDbType.Int16)
            cmd1.Parameters("?ProductID").Value = ProductID

            cmd1.Parameters.Add("?Stock_qty", MySqlDbType.Int16)
            cmd1.Parameters("?Stock_qty").Value = Stock_qty

            Try
                cmd1.ExecuteNonQuery()
            Catch ex As Exception

                MsgBox("Error Occurred: new bill" & ex.ToString)
            Finally
                conn.Close()
            End Try
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
              conn.Close()
        End Try
    End Sub
    Public Sub updateQuantity(ByVal ProductID As Integer)


        Dim InsertSQL As String = "Delete from products where ProductID = ?ProductID"
        Try
            conn.Open()
            Dim cmd1 As MySqlCommand = New MySqlCommand(InsertSQL, conn)
            cmd1.Parameters.Add("?ProductID", MySqlDbType.Int16)
            cmd1.Parameters("?ProductID").Value = ProductID
            Try
                cmd1.ExecuteNonQuery()
            Catch ex As Exception

                MsgBox("Error Occurred: new bill" & ex.ToString)
            Finally
                conn.Close()
            End Try
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
    End Sub
    Public Function getlowesStock() As ArrayList

        'MsgBox("from sql" & keyword)
        Dim productlist As New ArrayList
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "Select ProductName, Price, Stock_qty, ProductID from products order by Stock_qty"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            
            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                Dim selItem As New ProductItem
                selItem.ProductName = rdr(0)
                selItem.Price = rdr(1)
                'selItem.Taxable = rdr(2)
                selItem.StockQty = rdr(2)
                selItem.ProductId = rdr(3)
                productlist.Add(selItem)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return productlist

    End Function

    Public Function getSoldItems(ByVal date1 As String, ByVal date2 As String) As ArrayList

        'MsgBox("from sql" & keyword)
        Dim productlist As New ArrayList
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT ProductName, Amount, sum(Quantity), ProductID FROM bill " & _
        "left join orders on orders.OrderID = bill.OrderID " & _
"where orders.date between '" + date1 + "' and '" + date2 + "' " & _
"group by ProductID, Amount Order by ProductName"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                Dim selItem As New ProductItem
                selItem.ProductName = rdr(0)
                selItem.Price = rdr(1)
                'selItem.Taxable = rdr(2)
                selItem.StockQty = rdr(2)
                selItem.ProductId = rdr(3)
                productlist.Add(selItem)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return productlist

    End Function

    Public Function getUpdatedItems(ByVal date1 As String, ByVal date2 As String) As ArrayList

        'MsgBox("from sql" & keyword)
        Dim productlist As New ArrayList
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT ProductName, Price, sum(qty), inventorylog.ProductID FROM inventorylog " & _
        "left join products on inventorylog.ProductID = products.ProductID " & _
"where inventorylog.updateDate between '" + date1 + "' and '" + date2 + "' " & _
"group by ProductID Order by ProductName"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                Dim selItem As New ProductItem
                selItem.ProductName = rdr(0)
                selItem.Price = rdr(1)
                'selItem.Taxable = rdr(2)
                selItem.StockQty = rdr(2)
                selItem.ProductId = rdr(3)
                productlist.Add(selItem)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return productlist

    End Function

    Public Function getorderid() As Integer
        Dim con As MySqlConnection = New MySqlConnection(connstring)
        Dim sql1 As String = "select max(OrderID) from orders"
        Dim ORderid As Integer
        Try
            'Open connection
            con.Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql1, con)
            cmd.Prepare()
            rdr = cmd.ExecuteReader

            While (rdr.Read)
                ORderid = rdr(0)
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            con.Close()
        End Try
        Return ORderid
    End Function
    Public Sub DeleteBill(ByVal OrderID As Integer)

        Dim con As MySqlConnection = New MySqlConnection(connstring)
        Dim SQL As String = "Delete bill where OrderID = ?OrderID"
        'create connection
        Try
            'Open connection
            con.Open()
            Dim cmd1 As MySqlCommand = New MySqlCommand(SQL, con)

            cmd1.Parameters.Add("?OrderID", MySqlDbType.Int16)
            cmd1.Parameters("?OrderID").Value = OrderID
            cmd1.ExecuteNonQuery()

        Catch ex As Exception

            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            ' Close connection
            con.Close()
        End Try
    End Sub
    Public Sub CreateBill(ByVal orderId As Integer, ByVal Bill As ArrayList)

        Dim CreateOrderSQL As String = "Insert into bill (OrderID, ProductID, ProductName, Quantity, Amount, text) Values (?OrderID, ?ProductID, ?ProductName, ?Quantity, ?Amount, ?text)"
        'create connection
        Dim array As ArrayList = Bill
        'Dim orId As Integer = orderId
        'orId += 1
        'MsgBox(orderId)
        For i = 0 To array.Count - 1
            Dim addingBill As BillItems
            addingBill = array.Item(i)
            'Dim date_time As Date = Now
            Try
                'Open connection
                'Dialog3.ShowDialog()

                conn.Open()


                Dim cmd1 As MySqlCommand = New MySqlCommand(CreateOrderSQL, conn)
                'add parameters to the command for statements
                cmd1.Parameters.Add("?OrderID", MySqlDbType.Int16)
                cmd1.Parameters("?OrderID").Value = orderId

                cmd1.Parameters.Add("?ProductID", MySqlDbType.Int16)
                cmd1.Parameters("?ProductID").Value = addingBill.ProductId

                cmd1.Parameters.Add("?ProductName", MySqlDbType.VarChar, 100)
                cmd1.Parameters("?ProductName").Value = addingBill.ProductName

                cmd1.Parameters.Add("?Quantity", MySqlDbType.Int16)
                cmd1.Parameters("?Quantity").Value = addingBill.qty

                cmd1.Parameters.Add("?Amount", MySqlDbType.Decimal)
                cmd1.Parameters("?Amount").Value = addingBill.Price

                cmd1.Parameters.Add("?text", MySqlDbType.VarChar, 100)
                cmd1.Parameters("?text").Value = addingBill.txt

                cmd1.ExecuteNonQuery()

            Catch ex As Exception

                MsgBox("Error Occurred:" & ex.ToString)
            Finally
                ' Close connection
                conn.Close()
            End Try
        Next

    End Sub
    Public Sub FinishOrder(ByVal orderId As Integer, ByVal clsBill As closeBill)

        Dim CreateOrderSQL As String = "Insert into orders (OrderID, date, Total, Cash, VisaCard, MasterCard, AmEx, OtherPayment, debit, GiftCert, StoreCredit, HST, Subtotal, Discount, user) Values (?OrderID, ?date, ?Total, ?Cash, ?VisaCard, ?MasterCard, ?AmEx, ?OtherPayment, ?debit, ?GiftCert, ?StoreCredit, ?HST, ?Subtotal, ?Discount, ?user)"

        Dim clsoingBill As closeBill
        clsoingBill = clsBill
        Dim date_time As Date = Now
        Try
            conn.Open()
            Dim cmd1 As MySqlCommand = New MySqlCommand(CreateOrderSQL, conn)
            'add parameters to the command for statements
            cmd1.Parameters.Add("?OrderID", MySqlDbType.Int16)
            cmd1.Parameters("?OrderID").Value = orderId

            cmd1.Parameters.Add("?date", MySqlDbType.DateTime)
            cmd1.Parameters("?date").Value = date_time

            cmd1.Parameters.Add("?Total", MySqlDbType.Decimal)
            cmd1.Parameters("?Total").Value = clsoingBill.Total

            cmd1.Parameters.Add("?Cash", MySqlDbType.Decimal)
            cmd1.Parameters("?Cash").Value = clsoingBill.cash

            cmd1.Parameters.Add("?VisaCard", MySqlDbType.Decimal)
            cmd1.Parameters("?VisaCard").Value = clsoingBill.visa

            cmd1.Parameters.Add("?MasterCard", MySqlDbType.Decimal)
            cmd1.Parameters("?MasterCard").Value = clsoingBill.master

            cmd1.Parameters.Add("?AmEx", MySqlDbType.Decimal)
            cmd1.Parameters("?AmEx").Value = clsoingBill.amex

            cmd1.Parameters.Add("?OtherPayment", MySqlDbType.Decimal)
            cmd1.Parameters("?OtherPayment").Value = clsoingBill.other

            cmd1.Parameters.Add("?debit", MySqlDbType.Decimal)
            cmd1.Parameters("?debit").Value = clsoingBill.debit

            cmd1.Parameters.Add("?GiftCert", MySqlDbType.Decimal)
            cmd1.Parameters("?GiftCert").Value = clsoingBill.GiftCert

            cmd1.Parameters.Add("?StoreCredit", MySqlDbType.Decimal)
            cmd1.Parameters("?StoreCredit").Value = clsoingBill.StoreCredit

            cmd1.Parameters.Add("?HST", MySqlDbType.Decimal)
            cmd1.Parameters("?HST").Value = clsoingBill.HST

            cmd1.Parameters.Add("?Subtotal", MySqlDbType.Decimal)
            cmd1.Parameters("?Subtotal").Value = clsoingBill.Subtotal

            cmd1.Parameters.Add("?Discount", MySqlDbType.Decimal)
            cmd1.Parameters("?Discount").Value = clsoingBill.discAmount

            cmd1.Parameters.Add("?user", MySqlDbType.Int16)
            cmd1.Parameters("?user").Value = clsoingBill.user

            cmd1.ExecuteNonQuery()

        Catch ex As Exception

            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            ' Close connection
            conn.Close()
        End Try
        'Next

    End Sub
    Public Function getTodaySale(ByVal day1 As String, ByVal day2 As String) As ArrayList


        Dim bills As New ArrayList
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "Select OrderID, date, Total, HST, Subtotal, Discount from orders where date between ?day1 and ?day2"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?day1", MySqlDbType.VarChar)
            cmd2.Parameters("?day1").Value = day1

            cmd2.Parameters.Add("?day2", MySqlDbType.VarChar)
            cmd2.Parameters("?day2").Value = day2

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                Dim selItem As New orders
                selItem.OrderId = rdr(0)
                selItem.dateDay = rdr(1)
                selItem.total = rdr(2)
                selItem.hst = rdr(3)
                selItem.subtotal = rdr(4)
                selItem.discount = rdr(5)
                bills.Add(selItem)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
        Return bills

    End Function


    Public Function getCashSale(ByVal day1 As String, ByVal day2 As String) As Double


        Dim sales As New Double
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT SUM( Cash ) from orders where date between ?day1 and ?day2"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?day1", MySqlDbType.VarChar)
            cmd2.Parameters("?day1").Value = day1

            cmd2.Parameters.Add("?day2", MySqlDbType.VarChar)
            cmd2.Parameters("?day2").Value = day2

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                sales = rdr(0)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
        Return sales

    End Function

    Public Function getSalesTotal(ByVal day1 As String, ByVal day2 As String) As orders


        Dim saleTotall As New orders
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT SUM(Total), SUM(HST), SUM(Subtotal), SUM(Cash), SUM(VisaCard), SUM(AmEx), SUM(OtherPayment), SUM(debit), SUM(GiftCert), SUM(StoreCredit), SUM(Discount) , SUM(MasterCard) from orders where date between ?day1 and ?day2"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?day1", MySqlDbType.VarChar)
            cmd2.Parameters("?day1").Value = day1

            cmd2.Parameters.Add("?day2", MySqlDbType.VarChar)
            cmd2.Parameters("?day2").Value = day2

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                saleTotall.total = rdr(0)
                saleTotall.hst = rdr(1)
                saleTotall.subtotal = rdr(2)
                saleTotall.cash = rdr(3)
                saleTotall.visa = rdr(4)
                saleTotall.amex = rdr(5)
                saleTotall.other = rdr(6)
                saleTotall.debit = rdr(7)
                saleTotall.GiftCert = rdr(8)
                saleTotall.StoreCredit = rdr(9)
                saleTotall.discount = rdr(10)
                saleTotall.master = rdr(11)

            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
        Return saleTotall

    End Function

    Public Function getLastTillAmount() As Double


        Dim sales As New Double
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT cashTotal FROM daytable ORDER BY Day_no DESC LIMIT 1"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                sales = rdr(0)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
        Return sales
    End Function

    Public Function getCostPrice(ByVal ProductId As Integer) As costNdPrice


        Dim cPrice As New costNdPrice
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT CostPrice, Price FROM products WHERE ProductID = ?ProductId"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?ProductId", MySqlDbType.Int16)
            cmd2.Parameters("?ProductId").Value = ProductId

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                cPrice.costPrice = rdr(0)
                cPrice.price = rdr(1)

            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return cPrice

    End Function
    Public Function getDayStatus() As String
        Dim Status As String = "null"

        Dim sql1 As String = "SELECT STATUS FROM daytable ORDER BY Time_In DESC LIMIT 1"

        Dim con As MySqlConnection = New MySqlConnection(connstring)

        Try
            'Open connection
            con.Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql1, con)
            cmd.Prepare()
            rdr = cmd.ExecuteReader

            While (rdr.Read)
                Status = rdr(0)
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            con.Close()
        End Try
        Return Status
    End Function
    Public Sub openCloseDay(ByVal clsDay As closeDay)

        Dim CreateOrderSQL As String = "Insert into daytable (Userid, Time_In, Status, bill100, bill50, bill20, bill10, bill5, coin2, coin1, coin25, coin10, coin5, cashTotal, total, subtotal, hst, cash, visa, master, amex, other, debit, giftcert, store_credit, discount)" & _
                                      " Values (?Userid, ?Time_In, ?Status, ?bill100, ?bill50, ?bill20, ?bill10, ?bill5, ?coin2, ?coin1, ?coin25, ?coin10, ?coin5, ?cashTotal, ?total, ?subtotal, ?hst, ?cash, ?visa, ?master, ?amex, ?other, ?debit, ?giftcert, ?store_credit, ?discount)"

        Dim closingDay As closeDay
        Dim date_time As Date = Now
        closingDay = clsDay
        Try
            conn.Open()
            Dim cmd1 As MySqlCommand = New MySqlCommand(CreateOrderSQL, conn)
            'add parameters to the command for statements
            cmd1.Parameters.Add("?Userid", MySqlDbType.Int16)
            cmd1.Parameters("?Userid").Value = closingDay.Userid

            cmd1.Parameters.Add("?Time_In", MySqlDbType.DateTime)
            cmd1.Parameters("?Time_In").Value = date_time

            cmd1.Parameters.Add("?Status", MySqlDbType.String)
            cmd1.Parameters("?Status").Value = closingDay.Status

            cmd1.Parameters.Add("?bill100", MySqlDbType.Int16)
            cmd1.Parameters("?bill100").Value = closingDay.bill100

            cmd1.Parameters.Add("?bill50", MySqlDbType.Int16)
            cmd1.Parameters("?bill50").Value = closingDay.bill50

            cmd1.Parameters.Add("?bill20", MySqlDbType.Int16)
            cmd1.Parameters("?bill20").Value = closingDay.bill20

            cmd1.Parameters.Add("?bill10", MySqlDbType.Int16)
            cmd1.Parameters("?bill10").Value = closingDay.bill10

            cmd1.Parameters.Add("?bill5", MySqlDbType.Int16)
            cmd1.Parameters("?bill5").Value = closingDay.bill5

            cmd1.Parameters.Add("?coin2", MySqlDbType.Int16)
            cmd1.Parameters("?coin2").Value = closingDay.coin2

            cmd1.Parameters.Add("?coin1", MySqlDbType.Int16)
            cmd1.Parameters("?coin1").Value = closingDay.coin1

            cmd1.Parameters.Add("?coin25", MySqlDbType.Int16)
            cmd1.Parameters("?coin25").Value = closingDay.coin25

            cmd1.Parameters.Add("?coin10", MySqlDbType.Int16)
            cmd1.Parameters("?coin10").Value = closingDay.coin10

            cmd1.Parameters.Add("?coin5", MySqlDbType.Int16)
            cmd1.Parameters("?coin5").Value = closingDay.coin5

            cmd1.Parameters.Add("?cashTotal", MySqlDbType.Decimal)
            cmd1.Parameters("?cashTotal").Value = closingDay.cashTotal


            cmd1.Parameters.Add("?total", MySqlDbType.Decimal)
            cmd1.Parameters("?total").Value = closingDay.total

            cmd1.Parameters.Add("?subtotal", MySqlDbType.Decimal)
            cmd1.Parameters("?subtotal").Value = closingDay.subtotal

            cmd1.Parameters.Add("?hst", MySqlDbType.Decimal)
            cmd1.Parameters("?hst").Value = closingDay.hst

            cmd1.Parameters.Add("?cash", MySqlDbType.Decimal)
            cmd1.Parameters("?cash").Value = closingDay.cash

            cmd1.Parameters.Add("?visa", MySqlDbType.Decimal)
            cmd1.Parameters("?visa").Value = closingDay.visa

            cmd1.Parameters.Add("?master", MySqlDbType.Decimal)
            cmd1.Parameters("?master").Value = closingDay.master

            cmd1.Parameters.Add("?amex", MySqlDbType.Decimal)
            cmd1.Parameters("?amex").Value = closingDay.amex

            cmd1.Parameters.Add("?other", MySqlDbType.Decimal)
            cmd1.Parameters("?other").Value = closingDay.other

            cmd1.Parameters.Add("?debit", MySqlDbType.Decimal)
            cmd1.Parameters("?debit").Value = closingDay.debit

            cmd1.Parameters.Add("?giftcert", MySqlDbType.Decimal)
            cmd1.Parameters("?giftcert").Value = closingDay.giftcert

            cmd1.Parameters.Add("?store_credit", MySqlDbType.Decimal)
            cmd1.Parameters("?store_credit").Value = closingDay.store_credit

            cmd1.Parameters.Add("?discount", MySqlDbType.Decimal)
            cmd1.Parameters("?discount").Value = closingDay.discount

            cmd1.ExecuteNonQuery()

        Catch ex As Exception

            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            ' Close connection
            conn.Close()
        End Try
        'Next

    End Sub
    Public Sub addCustomer(ByVal customer As service)

        Dim date_time As Date = Now
        Dim InsertSQL As String = "Insert into customer (cusName, cusPhone, cusEmail, date, cusAddress, cusPosCode, cusCity, cusProvince)" & _
                               " Values (?cusName, ?cusPhone, ?cusEmail, ?date, ?cusAddress, ?cusPosCode, ?cusCity, ?cusProvince)"
        Try
            'For j = 0 To Splitarray.Count - 1
            Dim cust As New service
            'foodarray = Splitarray.Item(j)
            cust = customer
            'conn.Close()
            'MsgBox(items.ProductName)

            Dim cmd1 As MySqlCommand = New MySqlCommand(InsertSQL, conn)
            'add parameters to the command for statements
            cmd1.Parameters.Add("?cusName", MySqlDbType.VarChar)
            cmd1.Parameters("?cusName").Value = cust.cusName

            cmd1.Parameters.Add("?cusPhone", MySqlDbType.VarChar)
            cmd1.Parameters("?cusPhone").Value = cust.cusPhone

            cmd1.Parameters.Add("?cusEmail", MySqlDbType.VarChar)
            cmd1.Parameters("?cusEmail").Value = cust.cusEmail

            cmd1.Parameters.Add("?date", MySqlDbType.DateTime)
            cmd1.Parameters("?date").Value = date_time

            cmd1.Parameters.Add("?cusAddress", MySqlDbType.VarChar)
            cmd1.Parameters("?cusAddress").Value = cust.cusAddress

            cmd1.Parameters.Add("?cusPosCode", MySqlDbType.VarChar)
            cmd1.Parameters("?cusPosCode").Value = cust.cusPosCode

            cmd1.Parameters.Add("?cusCity", MySqlDbType.VarChar)
            cmd1.Parameters("?cusCity").Value = cust.cusCity

            cmd1.Parameters.Add("?cusProvince", MySqlDbType.VarChar)
            cmd1.Parameters("?cusProvince").Value = cust.cusProvince

            conn.Open()
            Try
                cmd1.ExecuteNonQuery()
            Catch ex As Exception

                MsgBox("Error Occurred: new bill" & ex.ToString)
            Finally
                conn.Close()
            End Try
        Catch ex As Exception

            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

    End Sub
    Public Sub addService(ByVal customer As service)

        Dim date_time As Date = Now
        Dim InsertSQL As String = "Insert into service (serviceID, customerID, comment, price, barcode, date)" & _
                               " Values (?serviceID, ?customerID, ?comment, ?price, ?barcode, ?date)"
        Try
            'For j = 0 To Splitarray.Count - 1
            Dim cust As New service
            'foodarray = Splitarray.Item(j)
            cust = customer
            'conn.Close()
            'MsgBox(items.ProductName)

            Dim cmd1 As MySqlCommand = New MySqlCommand(InsertSQL, conn)
            'add parameters to the command for statements
            cmd1.Parameters.Add("?serviceID", MySqlDbType.Int32)
            cmd1.Parameters("?serviceID").Value = cust.serID

            cmd1.Parameters.Add("?customerID", MySqlDbType.Int32)
            cmd1.Parameters("?customerID").Value = cust.cusID

            cmd1.Parameters.Add("?comment", MySqlDbType.VarChar)
            cmd1.Parameters("?comment").Value = cust.comment

            cmd1.Parameters.Add("?price", MySqlDbType.Decimal)
            cmd1.Parameters("?price").Value = cust.price

            cmd1.Parameters.Add("?barcode", MySqlDbType.VarChar)
            cmd1.Parameters("?barcode").Value = cust.barcode

            cmd1.Parameters.Add("?date", MySqlDbType.DateTime)
            cmd1.Parameters("?date").Value = date_time

            conn.Open()
            Try
                cmd1.ExecuteNonQuery()
            Catch ex As Exception

                MsgBox("Error Occurred: new bill" & ex.ToString)
            Finally
                conn.Close()
            End Try
        Catch ex As Exception

            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

    End Sub
    Public Function getLastCustomer() As Integer


        Dim number As Integer = 0
        Dim sql1 As String = "SELECT cusID FROM customer ORDER BY cusID Desc LIMIT 1"

        Dim con As MySqlConnection = New MySqlConnection(connstring)

        Try
            'Open connection
            con.Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql1, con)
            cmd.Prepare()
            rdr = cmd.ExecuteReader

            While (rdr.Read)
                number = rdr(0)
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            con.Close()
        End Try
        Return number
    End Function
    Public Function getLastService() As Integer


        Dim number As Integer = 0
        Dim sql1 As String = "SELECT serviceID FROM service ORDER BY serviceID Desc LIMIT 1"

        Dim con As MySqlConnection = New MySqlConnection(connstring)

        Try
            'Open connection
            con.Open()

            Dim cmd As MySqlCommand = New MySqlCommand(sql1, con)
            cmd.Prepare()
            rdr = cmd.ExecuteReader

            While (rdr.Read)
                number = rdr(0)
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            con.Close()
        End Try
        Return number
    End Function
    Public Function getServicePrice(ByVal barcode As String) As service


        Dim toRtn As New service

        Dim checkBillSQL As String = "SELECT price, comment FROM service where barcode = ?barcode"

        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?barcode", MySqlDbType.String)
            cmd2.Parameters("?barcode").Value = barcode

            rdr = cmd2.ExecuteReader
            While (rdr.Read)

                toRtn.price = rdr(0)
                toRtn.comment = rdr(1)
                toRtn.barcode = barcode

            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
        Return toRtn

    End Function
    Public Function getProductItem(ByVal ProductId As Integer) As ProductItem


        Dim product As New ProductItem
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT ProductName, ProductBarCode, Price, Taxable, Stock_qty, ManufactureName, StoreBarCode, CostPrice, Category, Description, itemNew, ref, remark FROM products WHERE ProductID = ?ProductId"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?ProductId", MySqlDbType.Int16)
            cmd2.Parameters("?ProductId").Value = ProductId

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                product.ProductName = rdr(0)
                product.BarCode = rdr(1)
                product.Price = rdr(2)
                product.Taxable = rdr(3)
                product.StockQty = rdr(4)
                product.ManufactureName = rdr(5)
                product.SecondBarCode = rdr(6)
                product.CostPrice = rdr(7)
                product.Category = rdr(8)
                product.Description = rdr(9)
                product.Refurb = rdr(10)
                product.eof = rdr(11)
                product.remark = rdr(12)


            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return product

    End Function
    Public Function getBill(ByVal OrderID As Integer) As ArrayList
        Dim bills As New ArrayList
        Dim checkBillSQL As String = "Select ProductName, Quantity, Amount, text from bill where OrderID = ?OrderID"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?OrderID", MySqlDbType.Int16)
            cmd2.Parameters("?OrderID").Value = OrderID
            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                Dim selItem As New BillItems
                selItem.ProductName = rdr(0)
                selItem.qty = rdr(1)
                selItem.Price = rdr(2)
                selItem.txt = rdr(3)
                bills.Add(selItem)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
        Return bills
    End Function

    Public Function getCategory() As ArrayList


        Dim checkBillSQL As String = "SELECT title FROM category ORDER BY title"
        Dim CategoryArray As New ArrayList
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            'Open connection
            conn.Open()

            cmd2.Prepare()
            rdr = cmd2.ExecuteReader

            While (rdr.Read)
                CategoryArray.Add(rdr(0))
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return CategoryArray
    End Function

    Public Function getCategorywithID() As ArrayList


        Dim checkBillSQL As String = "SELECT id, title FROM category ORDER BY title"
        Dim CategoryArray As New ArrayList
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            'Open connection
            conn.Open()

            cmd2.Prepare()
            rdr = cmd2.ExecuteReader

            While (rdr.Read)
                Dim category As New cate
                category.catid = rdr(0)
                category.cateogry = rdr(1)
                CategoryArray.Add(category)
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return CategoryArray
    End Function

    Public Function getCategorySub(ByVal title As String) As ArrayList


        Dim checkBillSQL As String = "select title, id from cats where categoryid = (select id from category where title = ?title) ORDER BY title"
        Dim CategoryArray As New ArrayList
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            'Open connection
            conn.Open()

            cmd2.Prepare()
            cmd2.Parameters.Add("?title", MySqlDbType.String)
            cmd2.Parameters("?title").Value = title
            rdr = cmd2.ExecuteReader

            While (rdr.Read)
                Dim catitem As cate
                catitem.cateogry = title
                catitem.catid = rdr(1)
                catitem.subcategory = rdr(0)
                CategoryArray.Add(catitem)
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return CategoryArray
    End Function

    Public Function getSubCategoryByID(ByVal catID As Integer) As ArrayList


        Dim checkBillSQL As String = "select id, title from cats where categoryid = ?categoryid ORDER BY title"
        Dim CategoryArray As New ArrayList
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            'Open connection
            conn.Open()

            cmd2.Prepare()
            cmd2.Parameters.Add("?categoryid", MySqlDbType.Int32)
            cmd2.Parameters("?categoryid").Value = catID
            rdr = cmd2.ExecuteReader

            While (rdr.Read)
                Dim catitem As cate
                catitem.cateogry = catID
                catitem.catid = rdr(0)
                catitem.subcategory = rdr(1)
                CategoryArray.Add(catitem)
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return CategoryArray
    End Function
    Public Function getSoldHistory(ByVal ProductId As Integer) As ArrayList


        Dim itemHistory As New BillItems
        Dim itemHist As New ArrayList
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT ProductName, Amount FROM  bill where ProductID = ?ProductId ORDER BY OrderID DESC LIMIT 10"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?ProductId", MySqlDbType.Int16)
            cmd2.Parameters("?ProductId").Value = ProductId

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                itemHistory.ProductName = rdr(0)
                itemHistory.Price = rdr(1)
                itemHist.Add(itemHistory)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return itemHist

    End Function

    Public Function getItemCat(ByVal SubCatID As Integer) As cate

        Dim catitem As New cate
        Dim checkBillSQL As String = "Select cats.title, category.title from cats, category where cats.id=?SubCatID AND category.id =(select categoryid from cats where id =?SubCatID)"
        'Dim CategoryArray As New ArrayList
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            'Open connection
            conn.Open()

            cmd2.Prepare()
            cmd2.Parameters.Add("?SubCatID", MySqlDbType.Int16)
            cmd2.Parameters("?SubCatID").Value = SubCatID
            rdr = cmd2.ExecuteReader

            While (rdr.Read)
                catitem.subcategory = rdr(0)
                catitem.cateogry = rdr(1)
            End While

            rdr.Close()

        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return catitem
    End Function

    Public Sub addSupplier(ByVal supplier As supplier)
        Dim InsertSQL As String = "Insert into supplier (company, address, telephone, notes)" & _
                          " Values (?company, ?address, ?telephone, ?notes)"
        If supplier.id > 0 Then
            InsertSQL = "update supplier set company = ?company, address =?address, telephone=?telephone, notes=?notes where id = ?id"
        End If
        Dim cmd1 As MySqlCommand = New MySqlCommand(InsertSQL, conn)
        'add parameters to the command for statements
        cmd1.Parameters.Add("?company", MySqlDbType.VarChar)
        cmd1.Parameters("?company").Value = supplier.name

        cmd1.Parameters.Add("?address", MySqlDbType.VarChar)
        cmd1.Parameters("?address").Value = supplier.address

        cmd1.Parameters.Add("?telephone", MySqlDbType.VarChar)
        cmd1.Parameters("?telephone").Value = supplier.telephon

        cmd1.Parameters.Add("?notes", MySqlDbType.VarChar)
        cmd1.Parameters("?notes").Value = supplier.notes

        If supplier.id > 0 Then
            cmd1.Parameters.Add("?id", MySqlDbType.Int16)
            cmd1.Parameters("?id").Value = supplier.id
        End If

        conn.Open()
        Try
            cmd1.ExecuteNonQuery()
        Catch ex As Exception

            MsgBox("Error Occurred: new bill" & ex.ToString)
        Finally
            conn.Close()
        End Try
    End Sub

    Public Function getSupplier() As ArrayList


        Dim itemHistory As New supplier
        Dim itemHist As New ArrayList
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "SELECT id, company, address, telephone, notes FROM  supplier ORDER BY company ASC"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                itemHistory.id = rdr(0)
                itemHistory.name = rdr(1)
                itemHistory.address = rdr(2)
                itemHistory.telephon = rdr(3)
                itemHistory.notes = rdr(4)
                itemHist.Add(itemHistory)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return itemHist

    End Function



    Public Sub addSupplierBill(ByVal inv As supplierInv, ByVal supItem As ArrayList)

        Dim CreateOrderSQL As String = "Insert into supplierInvoice (date, total, hst, net, invoiceid, supplierID)" & _
                                        "select ?date, ?total, ?hst, ?net, ?invoiceid, supplier.id from supplier where company = ?supplierID"

        Try
            'Dim items As New ProductItem
            ' items = product
            Dim cmd1 As MySqlCommand = New MySqlCommand(CreateOrderSQL, conn)


            cmd1.Parameters.Add("?date", MySqlDbType.Date)
            cmd1.Parameters("?date").Value = inv.buyDate

            cmd1.Parameters.Add("?total", MySqlDbType.Decimal)
            cmd1.Parameters("?total").Value = inv.Total

            cmd1.Parameters.Add("?hst", MySqlDbType.Decimal)
            cmd1.Parameters("?hst").Value = inv.HST

            cmd1.Parameters.Add("?net", MySqlDbType.Decimal)
            cmd1.Parameters("?net").Value = inv.net

            cmd1.Parameters.Add("?invoiceid", MySqlDbType.Int16)
            cmd1.Parameters("?invoiceid").Value = inv.Invoiceid

            cmd1.Parameters.Add("?supplierID", MySqlDbType.VarChar)
            cmd1.Parameters("?supplierID").Value = inv.supplierName
            conn.Open()
            cmd1.ExecuteNonQuery()
           
            conn.Close()
        Catch ex As Exception

            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            ' Close connection
            conn.Close()
        End Try

        For x = 0 To supItem.Count - 1

            Dim xitem As supplierItem
            xitem = supItem.Item(x)

            Dim CreateOrderSQL2 As String = "insert into  supplierItem(qty, item, price, itemID, supplierInvoiceID)" & _
                                            "select ?qty, ?item, ?price, ?itemID, max(supplierInvoice.id) from supplierInvoice"

            Try
                'Dim items As New ProductItem
                ' items = product
                Dim cmd1 As MySqlCommand = New MySqlCommand(CreateOrderSQL2, conn)


                cmd1.Parameters.Add("?qty", MySqlDbType.Int16)
                cmd1.Parameters("?qty").Value = xitem.qty

                cmd1.Parameters.Add("?item", MySqlDbType.VarChar)
                cmd1.Parameters("?item").Value = xitem.item

                cmd1.Parameters.Add("?price", MySqlDbType.Decimal)
                cmd1.Parameters("?price").Value = xitem.price

                cmd1.Parameters.Add("?itemID", MySqlDbType.Int16)
                cmd1.Parameters("?itemID").Value = xitem.itemID

                conn.Open()
                cmd1.ExecuteNonQuery()

                conn.Close()
            Catch ex As Exception

                MsgBox("Error Occurred:" & ex.ToString)
            Finally
                ' Close connection
                conn.Close()
            End Try
        Next


    End Sub

    Public Function getSettingValue(ByVal id As Integer) As String
        Dim toReturn As String = ""
        'MsgBox(productBarCode)
        Dim querySQL As String = "SELECT value from setting where id = ?id"
        Dim cmd2 As MySqlCommand = New MySqlCommand(querySQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?id", MySqlDbType.Int32)
            cmd2.Parameters("?id").Value = id
            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                toReturn = rdr(0)
            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
        Return toReturn
    End Function

    Public Sub updateSettingValue(ByVal id As Integer, ByVal value As String)


        Dim querySQL As String = "update setting set value = ?value where id =?id"
        Try
            conn.Open()
            Dim cmd1 As MySqlCommand = New MySqlCommand(querySQL, conn)
            cmd1.Parameters.Add("?id", MySqlDbType.Int32)
            cmd1.Parameters("?id").Value = id

            cmd1.Parameters.Add("?value", MySqlDbType.Text)
            cmd1.Parameters("?value").Value = value

            Try
                cmd1.ExecuteNonQuery()
            Catch ex As Exception

                MsgBox("Error Occurred: update settings" & ex.ToString)
            Finally
                conn.Close()
            End Try
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try
    End Sub

    Public Function getProductItemEdit(ByVal prodID As Integer) As ProductItem

        Dim selItem As New ProductItem
        'MsgBox(productBarCode)
        Dim checkBillSQL As String = "Select ProductID, ProductName, ProductBarCode, Price, Taxable, Stock_qty, ManufactureName, StoreBarCode, CostPrice, Category, Description, itemNew, ref, remark from products where ProductID = ?ProductID"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?ProductID", MySqlDbType.VarChar, 30)
            cmd2.Parameters("?ProductID").Value = prodID

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                selItem.ProductId = rdr(0)
                selItem.ProductName = rdr(1)
                selItem.BarCode = rdr(2)
                selItem.Price = rdr(3)
                selItem.Taxable = rdr(4)
                selItem.StockQty = rdr(5)
                selItem.ManufactureName = rdr(6)
                selItem.SecondBarCode = rdr(7)
                selItem.CostPrice = rdr(8)
                selItem.Category = rdr(9)
                selItem.Description = rdr(10)
                selItem.Refurb = rdr(11)
                selItem.remark = rdr(12)


            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return selItem

    End Function

    Public Sub addInventoryLog(ByVal ProductID As Integer, ByVal Stock_qty As Integer)
        Dim InsertSQL As String = "Insert into inventorylog (ProductID, qty, updateDate)" & _
                          " Values (?ProductID, ?qty, ?updateDate)"

        Dim cmd1 As MySqlCommand = New MySqlCommand(InsertSQL, conn)
        cmd1.Parameters.Add("?ProductID", MySqlDbType.Int32)
        cmd1.Parameters("?ProductID").Value = ProductID

        cmd1.Parameters.Add("?qty", MySqlDbType.Int32)
        cmd1.Parameters("?qty").Value = Stock_qty

        cmd1.Parameters.Add("?updateDate", MySqlDbType.DateTime)
        cmd1.Parameters("?updateDate").Value = Now

        conn.Open()
        Try
            cmd1.ExecuteNonQuery()
        Catch ex As Exception

            MsgBox("Error Occurred: new bill" & ex.ToString)
        Finally
            conn.Close()
        End Try
    End Sub

    Public Function DeleteSubCategory(ByVal CatId As Integer) As Boolean
        Dim toReturn As Boolean = False
        Dim count As Integer = 0
        Dim querySQL As String = "SELECT count(*) FROM products where Category = ?Category"
        Dim cmd2 As MySqlCommand = New MySqlCommand(querySQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?Category", MySqlDbType.Int32)
            cmd2.Parameters("?Category").Value = CatId
            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                count = rdr(0)

            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try


        If (count = 0) Then
            Dim querySQL1 As String = "DELETE FROM cats WHERE id = ?id"
            Dim cmd1 As MySqlCommand = New MySqlCommand(querySQL1, conn)
            cmd1.Parameters.Add("?id", MySqlDbType.Int32)
            cmd1.Parameters("?id").Value = CatId

            conn.Open()
            Try
                cmd1.ExecuteNonQuery()
                toReturn = True
            Catch ex As Exception

                MsgBox("Error Occurred: new bill" & ex.ToString)
            Finally
                conn.Close()
            End Try
        End If


        Return toReturn
    End Function

    Public Function DeleteCategory(ByVal CatId As Integer) As Boolean
        Dim toReturn As Boolean = False
        Dim count As Integer = 0
        Dim querySQL As String = "SELECT count(*) FROM cats where categoryid = ?categoryid"
        Dim cmd2 As MySqlCommand = New MySqlCommand(querySQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?categoryid", MySqlDbType.Int32)
            cmd2.Parameters("?categoryid").Value = CatId
            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                count = rdr(0)

            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try


        If (count = 0) Then
            Dim querySQL1 As String = "DELETE FROM category WHERE id = ?id"
            Dim cmd1 As MySqlCommand = New MySqlCommand(querySQL1, conn)
            cmd1.Parameters.Add("?id", MySqlDbType.Int32)
            cmd1.Parameters("?id").Value = CatId

            conn.Open()
            Try
                cmd1.ExecuteNonQuery()
                toReturn = True
            Catch ex As Exception

                MsgBox("Error Occurred: new bill" & ex.ToString)
            Finally
                conn.Close()
            End Try
        End If


        Return toReturn
    End Function


    Public Function UpdateCategory(ByVal Category As cate) As Boolean
        Dim toReturn As Boolean = False

        Dim querySQL As String = "UPDATE category SET title = ?title WHERE id = ?id"
        Dim cmd2 As MySqlCommand = New MySqlCommand(querySQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?id", MySqlDbType.Int32)
            cmd2.Parameters("?id").Value = Category.catid

            cmd2.Parameters.Add("?title", MySqlDbType.String)
            cmd2.Parameters("?title").Value = Category.cateogry

            cmd2.ExecuteNonQuery()
            toReturn = True
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return toReturn
    End Function

    Public Function UpdateSubCategory(ByVal Category As cate) As Boolean
        Dim toReturn As Boolean = False

        Dim querySQL As String = "UPDATE cats SET title = ?title WHERE id = ?id"
        Dim cmd2 As MySqlCommand = New MySqlCommand(querySQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?id", MySqlDbType.Int32)
            cmd2.Parameters("?id").Value = Category.catid

            cmd2.Parameters.Add("?title", MySqlDbType.String)
            cmd2.Parameters("?title").Value = Category.subcategory

            cmd2.ExecuteNonQuery()
            toReturn = True
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return toReturn
    End Function

    Public Function InsertCategory(ByVal Category As cate) As Boolean
        Dim toReturn As Boolean = False

        Dim querySQL As String = "INSERT INTO category (title) VALUES (?title)"
        Dim cmd2 As MySqlCommand = New MySqlCommand(querySQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()

            cmd2.Parameters.Add("?title", MySqlDbType.String)
            cmd2.Parameters("?title").Value = Category.cateogry

            cmd2.ExecuteNonQuery()
            toReturn = True
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return toReturn
    End Function

    Public Function InsertSubCategory(ByVal Category As cate) As Boolean
        Dim toReturn As Boolean = False

        Dim querySQL As String = "INSERT INTO cats (categoryid, title) VALUES (?categoryid, ?title)"
        Dim cmd2 As MySqlCommand = New MySqlCommand(querySQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?categoryid", MySqlDbType.Int32)
            cmd2.Parameters("?categoryid").Value = Category.catid

            cmd2.Parameters.Add("?title", MySqlDbType.String)
            cmd2.Parameters("?title").Value = Category.subcategory

            cmd2.ExecuteNonQuery()
            toReturn = True
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return toReturn
    End Function

    Public Function getUser(ByVal Password As String) As User


        Dim user As New User
        Dim checkBillSQL As String = "SELECT id, ulevel, name, password, active  FROM users WHERE password = ?password"
        Dim cmd2 As MySqlCommand = New MySqlCommand(checkBillSQL, conn)
        Try
            conn.Open()
            cmd2.Prepare()
            cmd2.Parameters.Add("?password", MySqlDbType.String)
            cmd2.Parameters("?password").Value = Password

            rdr = cmd2.ExecuteReader
            While (rdr.Read)
                user.id = rdr(0)
                user.ulevel = rdr(1)
                user.name = rdr(2)
                user.password = rdr(3)
                user.active = rdr(4)

            End While
            rdr.Close()
        Catch ex As Exception
            MsgBox("Error Occurred:" & ex.ToString)
        Finally
            conn.Close()
        End Try

        Return user

    End Function
End Class


