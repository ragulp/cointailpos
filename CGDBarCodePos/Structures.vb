﻿Public Structure ProductItem
    'Public members
    Public ProductName As String
    Public ManufactureName As String
    Public BarCode As String
    Public SecondBarCode As String
    Public Price As Double
    Public CostPrice As Double
    Public Taxable As Boolean
    Public StockQty As Integer
    Public ProductId As Integer
    Public Description As String
    Public Refurb As Boolean
    Public Category As String
    Public remark As String
    Public eof As Double
End Structure
Public Structure BillItems
    'Public members
    Public ProductName As String
    Public Price As Double
    Public ProductId As Integer
    Public qty As Integer
    Public txt As String
End Structure
Public Structure orders
    'Public members
    Public dateDay As String
    Public subtotal As Double
    Public hst As Double
    Public total As Double
    Public discount As Double
    Public OrderId As Integer

    Public cash As Double
    Public visa As Double
    Public master As Double
    Public amex As Double
    Public other As Double
    Public debit As Double
    Public GiftCert As Double
    Public StoreCredit As Double

End Structure
Public Structure closeBill
    Public cash As Double
    Public visa As Double
    Public master As Double
    Public amex As Double
    Public other As Double
    Public debit As Double
    Public GiftCert As Double
    Public StoreCredit As Double
    Public HST As Double
    'Public Liq As Double
    Public Subtotal As Double
    Public Total As Double
    'Public OwingCash As Double
    Public discAmount As Double
    Public cashtend As Double
    Public orderid As Integer
    Public user As Integer
End Structure
Public Structure closeDay
    Public Userid As Integer
    Public Status As String
    Public bill100 As Integer
    Public bill50 As Integer
    Public bill20 As Integer
    Public bill10 As Integer
    Public bill5 As Integer
    Public coin2 As Integer
    Public coin1 As Integer
    Public coin25 As Integer
    Public coin10 As Integer
    Public coin5 As Integer
    Public cashTotal As Double
    Public total As Double
    Public subtotal As Double
    Public hst As Double
    Public cash As Double
    Public visa As Double
    Public master As Double
    Public amex As Double
    Public other As Double
    Public debit As Double
    Public giftcert As Double
    Public store_credit As Double
    Public discount As Double
End Structure
Public Structure costNdPrice
    Public costPrice As Double
    Public price As Double
End Structure
Public Structure service
    Public picture As Byte()
    Public cusName As String
    Public cusPhone As String
    Public cusEmail As String
    Public cusAddress As String
    Public cusPosCode As String
    Public cusCity As String
    Public cusProvince As String
    Public comment As String
    Public price As Double
    Public barcode As String
    Public cusID As Integer
    Public serID As Integer
End Structure
Public Structure cate
    Public cateogry As String
    Public subcategory As String
    Public catid As Integer
End Structure
Public Structure supplier
    Public address As String
    Public telephon As String
    Public notes As String
    Public name As String
    Public id As Integer
End Structure
Public Structure supplierInv
    Public Invoiceid As Integer
    Public buyDate As Date
    Public HST As Double
    'Public Liq As Double
    Public net As Double
    Public Total As Double
    Public supplierName As String
End Structure
Public Structure supplierItem
    Public itemID As Integer
    Public qty As Integer
    Public item As String
    Public price As Double
    Public invID As Integer
End Structure
Public Structure User
    Public id As Integer
    Public ulevel As Integer
    Public name As String
    Public password As String
    Public active As Boolean
End Structure