﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddStock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlSearch = New System.Windows.Forms.Panel()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btndelet = New System.Windows.Forms.Button()
        Me.lblsts = New System.Windows.Forms.Label()
        Me.txtqtyIncrease = New System.Windows.Forms.TextBox()
        Me.lblPrID = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.pnlSearch.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlSearch
        '
        Me.pnlSearch.Controls.Add(Me.btnPrint)
        Me.pnlSearch.Controls.Add(Me.btndelet)
        Me.pnlSearch.Controls.Add(Me.lblsts)
        Me.pnlSearch.Controls.Add(Me.txtqtyIncrease)
        Me.pnlSearch.Controls.Add(Me.lblPrID)
        Me.pnlSearch.Controls.Add(Me.lblName)
        Me.pnlSearch.Controls.Add(Me.btnClose)
        Me.pnlSearch.Controls.Add(Me.btnUpdate)
        Me.pnlSearch.Controls.Add(Me.Label1)
        Me.pnlSearch.Controls.Add(Me.ListView2)
        Me.pnlSearch.Controls.Add(Me.txtSearch)
        Me.pnlSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlSearch.Location = New System.Drawing.Point(0, 0)
        Me.pnlSearch.Name = "pnlSearch"
        Me.pnlSearch.Size = New System.Drawing.Size(700, 643)
        Me.pnlSearch.TabIndex = 1
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(275, 567)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(119, 64)
        Me.btnPrint.TabIndex = 10
        Me.btnPrint.Text = "Print Stock"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'btndelet
        '
        Me.btndelet.Location = New System.Drawing.Point(400, 567)
        Me.btndelet.Name = "btndelet"
        Me.btndelet.Size = New System.Drawing.Size(119, 64)
        Me.btndelet.TabIndex = 9
        Me.btndelet.Text = "Delete Item"
        Me.btndelet.UseVisualStyleBackColor = True
        '
        'lblsts
        '
        Me.lblsts.AutoSize = True
        Me.lblsts.Location = New System.Drawing.Point(24, 618)
        Me.lblsts.Name = "lblsts"
        Me.lblsts.Size = New System.Drawing.Size(25, 13)
        Me.lblsts.TabIndex = 8
        Me.lblsts.Text = "___"
        '
        'txtqtyIncrease
        '
        Me.txtqtyIncrease.Location = New System.Drawing.Point(211, 578)
        Me.txtqtyIncrease.Name = "txtqtyIncrease"
        Me.txtqtyIncrease.Size = New System.Drawing.Size(57, 20)
        Me.txtqtyIncrease.TabIndex = 7
        '
        'lblPrID
        '
        Me.lblPrID.AutoSize = True
        Me.lblPrID.Location = New System.Drawing.Point(84, 581)
        Me.lblPrID.Name = "lblPrID"
        Me.lblPrID.Size = New System.Drawing.Size(31, 13)
        Me.lblPrID.TabIndex = 6
        Me.lblPrID.Text = "0000"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(24, 581)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(181, 13)
        Me.lblName.TabIndex = 5
        Me.lblName.Text = "Product ID -            Increase Qty By :"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(549, 567)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(119, 64)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(414, 567)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(119, 64)
        Me.btnUpdate.TabIndex = 3
        Me.btnUpdate.Text = "Update Qty"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(182, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Search By Barcode or Product Name"
        '
        'ListView2
        '
        Me.ListView2.FullRowSelect = True
        Me.ListView2.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ListView2.Location = New System.Drawing.Point(27, 76)
        Me.ListView2.MultiSelect = False
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(641, 472)
        Me.ListView2.TabIndex = 1
        Me.ListView2.UseCompatibleStateImageBehavior = False
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(226, 28)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(442, 26)
        Me.txtSearch.TabIndex = 0
        '
        'frmAddStock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 643)
        Me.Controls.Add(Me.pnlSearch)
        Me.Name = "frmAddStock"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmAddStock"
        Me.pnlSearch.ResumeLayout(False)
        Me.pnlSearch.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlSearch As System.Windows.Forms.Panel
    Friend WithEvents ListView2 As System.Windows.Forms.ListView
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtqtyIncrease As System.Windows.Forms.TextBox
    Friend WithEvents lblPrID As System.Windows.Forms.Label
    Friend WithEvents lblsts As System.Windows.Forms.Label
    Friend WithEvents btndelet As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
End Class
