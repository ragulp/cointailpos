﻿Option Strict Off
Public Class frmModifyCateg
    Dim sqlcon As New CGDsql

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        lvCateg.Items.Clear()
        Dim cateArray As ArrayList = sqlcon.getCategorywithID()
        For x = 0 To cateArray.Count - 1
            Dim category As cate = cateArray.Item(x)
            Dim lvitem As New ListViewItem
            lvitem.Text = category.catid
            lvitem.SubItems.Add(category.cateogry)
            lvCateg.Items.Add(lvitem)
        Next
    End Sub



    Private Sub frmModifyCateg_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lvCateg.Columns.Add("ID", 30)
        lvCateg.Columns.Add("Category", 240)
        lvCateg.View = View.Details

        lvSubCateg.Columns.Add("ID", 30)
        lvSubCateg.Columns.Add("Sub-Category", 202)
        lvSubCateg.View = View.Details
        btnRefresh_Click(sender, e)
        'l()
    End Sub

    Private Sub lvCateg_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvCateg.SelectedIndexChanged
        If lvCateg.SelectedItems.Count = 1 Then
            lvSubCateg.Items.Clear()
            Dim cateSubArray As ArrayList = sqlcon.getSubCategoryByID(lvCateg.SelectedItems(0).Text)
            txtCateg.Text = lvCateg.SelectedItems(0).SubItems(1).Text
            For x = 0 To cateSubArray.Count - 1
                Dim subcate As cate = cateSubArray.Item(x)
                Dim lvitem As New ListViewItem
                lvitem.Text = subcate.catid
                lvitem.SubItems.Add(subcate.subcategory)

                lvSubCateg.Items.Add(lvitem)
            Next


            'getCategorySub()
        End If
    End Sub

    Private Sub lvSubCateg_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvSubCateg.SelectedIndexChanged
        If lvSubCateg.SelectedItems.Count = 1 Then
            txtSubCateg.Text = lvSubCateg.SelectedItems(0).SubItems(1).Text
        End If
    End Sub

    Private Sub btnSubDelete_Click(sender As Object, e As EventArgs) Handles btnSubDelete.Click
        If lvSubCateg.SelectedItems.Count = 1 Then
            Dim bol As Boolean = sqlcon.DeleteSubCategory(lvSubCateg.SelectedItems(0).Text)
            If (bol = False) Then
                MsgBox("Error on delete, Verify there is no product using this Sub-Category.")
            Else
                MsgBox("Sub-Category deleted sucessfully")
            End If
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If lvCateg.SelectedItems.Count = 1 Then
            Dim bol As Boolean = sqlcon.DeleteCategory(lvCateg.SelectedItems(0).Text)
            If (bol = False) Then
                MsgBox("Error on delete, Verify there is no Sub-Category for this Category.")
            Else
                MsgBox("Category deleted sucessfully")
            End If
        End If
    End Sub

    Private Sub btnSubUpdate_Click(sender As Object, e As EventArgs) Handles btnSubUpdate.Click
        If lvSubCateg.SelectedItems.Count = 1 And txtSubCateg.Text.Length > 0 Then
            Dim category As New cate
            category.catid = lvSubCateg.SelectedItems(0).Text
            category.subcategory = txtSubCateg.Text
            Dim bol As Boolean = sqlcon.UpdateSubCategory(category)
            If (bol) Then
                MsgBox("Sub-Category Updated sucessfully.")
            Else
                MsgBox("Error on update")
            End If
        End If
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        If lvCateg.SelectedItems.Count = 1 And txtCateg.Text.Length > 0 Then
            Dim category As New cate
            category.catid = lvCateg.SelectedItems(0).Text
            category.cateogry = txtCateg.Text
            Dim bol As Boolean = sqlcon.UpdateCategory(category)
            If (bol) Then
                MsgBox("Category Updated sucessfully.")
            Else
                MsgBox("Error on update")
            End If
        End If
    End Sub

    Private Sub btnSubAdd_Click(sender As Object, e As EventArgs) Handles btnSubAdd.Click
        If lvCateg.SelectedItems.Count = 1 And txtSubCateg.Text.Length > 0 Then
            Dim category As New cate
            category.catid = lvCateg.SelectedItems(0).Text
            category.subcategory = txtSubCateg.Text
            Dim bol As Boolean = sqlcon.InsertSubCategory(category)
            If (bol) Then
                MsgBox("Sub-Category Added sucessfully.")
            Else
                MsgBox("Error on update")
            End If
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If txtCateg.Text.Length > 0 Then
            Dim category As New cate
            category.cateogry = txtCateg.Text
            Dim bol As Boolean = sqlcon.InsertCategory(category)
            If (bol) Then
                MsgBox("Category Added sucessfully.")
            Else
                MsgBox("Error on update")
            End If
        End If
    End Sub
End Class