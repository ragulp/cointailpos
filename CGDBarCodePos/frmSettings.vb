﻿Option Strict Off
Public Class frmSettings
    Dim sqlCon As New CGDsql
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub

    Private Sub cmbChooseSetting_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbChooseSetting.SelectedIndexChanged
        Dim settingid As String = cmbChooseSetting.SelectedItem
        If (settingid = "Footer Line 1") Then
            txtSettingValue.Text = sqlCon.getSettingValue(2)
        ElseIf (settingid = "Footer Line 2") Then
            txtSettingValue.Text = sqlCon.getSettingValue(3)
        ElseIf (settingid = "Footer Line 3") Then
            txtSettingValue.Text = sqlCon.getSettingValue(4)
        End If
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim settingid As String = cmbChooseSetting.SelectedItem
        If (settingid = "Footer Line 1") Then
            sqlCon.updateSettingValue(2, txtSettingValue.Text)
            frmMain.lblstatus.Text = "Footer Line 1 Updated Suessfully"
        ElseIf (settingid = "Footer Line 2") Then
            sqlCon.updateSettingValue(3, txtSettingValue.Text)
            frmMain.lblstatus.Text = "Footer Line 2 Updated Suessfully"
        ElseIf (settingid = "Footer Line 3") Then
            sqlCon.updateSettingValue(4, txtSettingValue.Text)
            frmMain.lblstatus.Text = "Footer Line 3 Updated Suessfully"
        End If
    End Sub
End Class