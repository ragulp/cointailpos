﻿Option Strict Off
Public Class frmReport
    Public printReportRange As closeBill
    Public printReportDate As String
    Private Sub ListView1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListView1.MouseDoubleClick
        Dim orderID As Integer = (ListView1.SelectedItems(0).SubItems(0).Text)
        Dim gettingItem As New CGDsql
        frmTextbox.ListView1.Clear()
        frmTextbox.ListView1.Columns.Add("Qty", 40)
        frmTextbox.ListView1.Columns.Add("Item", 200)
        frmTextbox.ListView1.Columns.Add("Price", 70)
        frmTextbox.ListView1.Columns.Add("Remark", 200)
        'ListView1.f
        frmTextbox.ListView1.View = View.Details
        'today = DateTimePicker1.Value.Date

        Dim totalOrder As New orders
        Dim bills As New ArrayList
        bills = gettingItem.getBill(orderID)
        frmTextbox.billPrint = bills
        frmTextbox.billprintID = orderID

        frmTextbox.billdate = ListView1.SelectedItems(0).SubItems(5).Text
        frmTextbox.billdiscount = ListView1.SelectedItems(0).SubItems(3).Text
        frmTextbox.billhst = ListView1.SelectedItems(0).SubItems(1).Text
        frmTextbox.billnet = ListView1.SelectedItems(0).SubItems(2).Text
        frmTextbox.billtotal = ListView1.SelectedItems(0).SubItems(4).Text
        For x = 0 To bills.Count - 1
            Dim bill As New BillItems
            bill = bills.Item(x)

            Dim lvItem As New ListViewItem
            lvItem.Text = bill.qty
            lvItem.SubItems.Add(bill.ProductName)
            lvItem.SubItems.Add(FormatNumber(bill.Price, 2))
            lvItem.SubItems.Add(bill.txt)

            frmTextbox.ListView1.Items.Add(lvItem)


        Next

        'Dim orderdate1 As New orders
        'orderdate1 = bills.Item(0)

        'Dim orderdate2 As New orders
        'orderdate2 = bills.Item(bills.Count - 1)
        'printReportDate = orderdate1.dateDay & " - " & orderdate2.dateDay

        'printReportRange.Subtotal = totalOrder.subtotal
        'printReportRange.HST = totalOrder.hst
        'printReportRange.Total = totalOrder.total

        frmTextbox.ShowDialog()
    End Sub


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        frmMain.printerPrintSales(printReportRange, printReportDate)
    End Sub
End Class