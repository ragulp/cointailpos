﻿Option Strict Off


Public Class frmMain
    'notes
    ''http://social.msdn.microsoft.com/Forums/en-US/vssmartdevicesvbcs/thread/7214173e-7fe3-4d12-8936-96355b29a649
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            Const CS_DBLCLKS As Int32 = &H8
            Const CS_NOCLOSE As Int32 = &H200
            cp.ClassStyle = CS_DBLCLKS Or CS_NOCLOSE
            Return cp
        End Get

    End Property
    Public password As String = "9"             ' 1
    Public footer2 As String = ""               ' 2
    Public footer3 As String = "Thank You!"     ' 3
    Public footer4 As String = ""               ' 4
    Public storeInitial As String = "ITEC"      ' 6
    Public serviceReport As Boolean = False     ' 7
    Public slideimages As New ArrayList
    Public cusDisplayType As Integer = 1        ' 8
    Public cusDisplayScreen As Integer = 1      ' 9 (0 = scrren 1) 
    Public currentimg As Integer = 0
    Public printerName As String = "Receipt"    '10
    'Public storName As String = "Urban Print Ruban"
    'Public storeAddress As String = "151 Nashdene Rd, Unit 50"
    'Public storeAddress2 As String = "Scarborough ON. M1V 4C4"
    'Public storePhone As String = "(416) 792 4144"
    'Public storeHST As String = "HST  #848070942"

    'Public storName As String = "DEMO PoS"
    'Public storeAddress As String = ""
    'Public storeAddress2 As String = ""
    'Public storePhone As String = ""
    'Public storeHST As String = ""

    'Public storName As String = "Happy Way"
    'Public storeAddress As String = ""
    'Public storeAddress2 As String = ""
    'Public storePhone As String = ""
    'Public storeHST As String = ""

    'Public storName As String = "ITEC Computers & Elec."
    'Public storeAddress As String = "1847 Lawrence Ave. East"
    'Public storeAddress2 As String = "Scarborough ON M1R 2Y3"
    'Public storePhone As String = "(647) 348-6999"
    'Public storeHST As String = "HST #818202384"

    Public storName As String = "Marhaba Book Store"
    Public storName2 As String = "and Hijab Fashion"
    Public storeAddress As String = "1010 Dream Crest Road #11"
    Public storeAddress2 As String = "Mississauga ON L5V 3A4"
    Public storePhone As String = "(905) 812-2574"
    Public storeHST As String = ""



    Public billList As New ArrayList
    Public billitem As New BillItems
    Public store As Integer = 2
    Public totalAmt As Double = 0
    'Dim NewDynButton As DynamicButton

    Private Sub txtInput_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtInput.KeyUp
        'Dim lvItem As New ListViewItem
        Dim serItem As New CGDsql
        Dim searchItem As ProductItem
        Dim billitm As New BillItems


        If e.KeyCode = Keys.Enter Then

            If txtInput.Text.Length >= storeInitial.Length Then
                If (storeInitial.Length > 0 And (txtInput.Text.Substring(0, storeInitial.Length) = storeInitial)) Then
                    Dim takeService As service
                    takeService = serItem.getServicePrice(txtInput.Text)
                    If takeService.price = 0 And takeService.comment.Length < 1 Then
                        lblstatus.Text = "The Item Does Not Exists on System"
                    Else
                        billitm.qty = 1
                        billitm.ProductName = "Services " + takeService.barcode
                        billitm.ProductId = 10106
                        billitm.Price = takeService.price
                        billitm.txt = takeService.comment
                        lblstatus.Text = ""
                        billList.Add(billitm)
                        txtkeypadtotal.Text = ""
                        Pricing()
                    End If


                Else
                    searchItem = serItem.searchProduct(txtInput.Text)
                    If searchItem.Price = 0 Then
                        lblstatus.Text = "The Item Does Not Exists on System"
                    Else
                        billitm.qty = 1
                        billitm.ProductName = searchItem.ProductName.Trim
                        billitm.ProductId = searchItem.ProductId
                        billitm.Price = searchItem.Price
                        'billitm.
                        'MsgBox(searchItem.remark)
                        billitm.txt = searchItem.remark



                        lblstatus.Text = ""
                        billList.Add(billitm)
                        If searchItem.eof > 0 Then
                            billitm.qty = 1
                            billitm.ProductName = "Oes"
                            billitm.ProductId = 10107
                            billitm.Price = searchItem.eof
                            billList.Add(billitm)
                            'billitm.
                            'MsgBox(searchItem.remark)
                            'billitm.txt = searchItem.remark
                        End If
                        txtkeypadtotal.Text = ""
                        Pricing()
                    End If

                End If
            End If



            txtInput.Text = ""
            txtInput.Focus()

        End If

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblTimeDisplay.Text = Now.ToLongTimeString
        lblDateDiplay.Text = Now.ToShortDateString
        'NewShift.lblTimeDisplay.Text = Now.ToLongTimeString
        'NewShift.lblDateDiplay.Text = Now.ToShortDateString
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim chkstatus As New CGDsql

        If (chkstatus.getDayStatus.Trim = "open") Then
            frmOpenday.btnResume.Visible = True

        ElseIf (chkstatus.getDayStatus.Trim = "close") Then
            frmOpenday.btnOpen.Visible = True

        End If
        frmOpenday.lblStoreName.Text = storName + " " + storName2
        password = chkstatus.getSettingValue(1)
        footer2 = chkstatus.getSettingValue(2)
        footer3 = chkstatus.getSettingValue(3)
        footer4 = chkstatus.getSettingValue(4)
        storeInitial = chkstatus.getSettingValue(6)
        serviceReport = chkstatus.getSettingValue(7)
        cusDisplayType = chkstatus.getSettingValue(8)
        cusDisplayScreen = chkstatus.getSettingValue(9)
        printerName = chkstatus.getSettingValue(10)
        If serviceReport Then
            Button6.Text = "Services"
            Button6.Enabled = True
        End If
        frmOpenday.ShowDialog()

        'txtInput.Focused.
        ListView1.Columns.Add("#", 20)
        ListView1.Columns.Add("Qty", 28)
        ListView1.Columns.Add("Item", 145)
        ListView1.Columns.Add("Price", 60)
        'ListView1.f
        ListView1.View = View.Details




        ListView2.Columns.Add("#", 35)
        ListView2.Columns.Add("inHand", 49)
        ListView2.Columns.Add("Item", 428)
        ListView2.Columns.Add("ID", 45)
        ListView2.Columns.Add("Price", 80)
        'ListView2.Columns.Add("Remark", 80)

        'ListView1.f
        ListView2.View = View.Details

        Timer1.Enabled = True
        Timer1.Interval = 10
        Try
            PictureBox1.BackgroundImage = Image.FromFile(".\images\icon\logo.jpg")
        Catch ex As Exception
            '    MsgBox("No Logo image loaded")

        End Try
        If cusDisplayType > 0 Then


            Dim di As New IO.DirectoryInfo(".\images\")
            Dim diar1 As IO.FileInfo() = di.GetFiles()
            Dim dra As IO.FileInfo
            'list the names of all files in the specified directory
            For Each dra In diar1
                ' ListBox1.Items.Add(dra)
                Dim str As String = dra.ToString
                Dim strCheck As String = str.Substring(str.Length - 3)
                If strCheck = "jpg" Or strCheck = "JPG" Or strCheck = "png" Or strCheck = "PNG" Then
                    slideimages.Add(str)
                End If
            Next
            Timer2.Interval = 3000
            Timer2.Start()
            Try
                Dim screen As Screen
                ' We want to display a form on screen 1
                screen = screen.AllScreens(cusDisplayScreen)
                ' Set the StartPosition to Manual otherwise the system will assign an automatic start position
                frmCusDisplay.StartPosition = FormStartPosition.Manual
                ' Set the form location so it appears at Location (100, 100) on the screen 1
                frmCusDisplay.Location = screen.Bounds.Location + New Point(1, 1)
                ' Show the form
                frmCusDisplay.Show(Me)
                frmCusDisplay.WindowState = FormWindowState.Maximized
                'cusdisplay = True
                'pgs += 1
                'Dim rowcount As New itecSQL
                'Dim itemRows As Integer = rowcount.getrowcount
                'Dim pgCount As Integer = Math.Ceiling(itemRows / 66)
                'If pgs >= pgCount Then
                '    pgs = 0
                '    'MsgBox("rows are " & itemRows.ToString & " pgs are " & pgCount.ToString)
                'End If
                'rowcount.SqlserverQuery(pgs)
            Catch ex As Exception
            End Try
            'TODO: This line of code loads data into the 'Inventory_DatabaseDataSet.table1' table. You can move, or remove it, as needed.
            'Me.Table1TableAdapter.Fill(Me.Inventory_DatabaseDataSet.table1)

            frmCusDisplay.ListView2.Columns.Add("#", 20)
            frmCusDisplay.ListView2.Columns.Add("Qty", 28)
            frmCusDisplay.ListView2.Columns.Add("Item", 145)
            frmCusDisplay.ListView2.Columns.Add("Price", 60)
            'ListView1.f
            frmCusDisplay.ListView2.View = View.Details
        End If

        btnVoid.Enabled = False
        btn_Qty.Enabled = False
        btnPreSet.Enabled = False
        btnSpcial.Enabled = False
        btnKeypad.Enabled = False
        btnSearch.Enabled = False
        btnOptions.Enabled = False
        btnPrint.Text = "Log In"
        lblUser.Tag = ""
        lblUser.Text = ""

    End Sub


    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblstatus.Text = ""
        pnlSearch.BringToFront()
        btnSearch.Enabled = False
        btnPreSet.Enabled = True
        btnKeypad.Enabled = True
        btnSpcial.Enabled = True
        txtSearch.Focus()

    End Sub
    'Dim addingNumber As Integer = 100
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        'Dim lvItem As New ListViewItem
        Dim billitm As New BillItems
        'addingNumber += 1
        Dim numprice As Double
        numprice = txtkeypadtotal.Text

        'objNewFood2 = objFoodsArray.Item(objFoodsArray.Count - 1)
        ' netP = netP + (objNewFood2.Price)
        'xnet = xnet + 1
        If numprice = 0 Or numprice = Nothing Then
            lblstatus.Text = "Incorrect Price Entered"
        ElseIf numprice >= 0 Then
            billitm.qty = 1
            billitm.ProductId = 1
            billitm.Price = numprice
            billitm.ProductName = "Miscelneous"
            billitm.txt = ""

            billList.Add(billitm)
            'lvItem.Text = 1
            'lvItem.SubItems.Add(1)
            'lvItem.SubItems.Add("Miscelneous")
            'lvItem.SubItems.Add("$ " & (FormatNumber(numprice, 2)))
            'ListView1.Items.Add(lvItem)
            'billList.Add(
            txtkeypadtotal.Text = ""
            'totalAmt = totalAmt + numprice
            Pricing()
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtkeypadtotal.Text = ""
    End Sub

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "1"
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "2"
    End Sub



    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "3"
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "4"
    End Sub

    Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn5.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "5"
    End Sub

    Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn6.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "6"
    End Sub

    Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "7"
    End Sub

    Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn8.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "8"
    End Sub

    Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn9.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "9"
    End Sub

    Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "0"
    End Sub

    Private Sub btnDot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDot.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + "."
    End Sub

    Private Sub btndot00_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndot00.Click
        txtkeypadtotal.Text = txtkeypadtotal.Text + ".00"
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeypad.Click
        lblstatus.Text = ""
        pnlKeypad.BringToFront()
        btnKeypad.Enabled = False
        btnSearch.Enabled = True
        btnPreSet.Enabled = True
        btnSpcial.Enabled = True

        txtkeypadtotal.Focus()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreSet.Click


        lblstatus.Text = ""
        Dim loadbutton As New CGDsql
        loadbutton.SqlserverQuery()

        pnlPreSet.BringToFront()

        btnPreSet.Enabled = False
        btnSearch.Enabled = True
        btnKeypad.Enabled = True
        btnSpcial.Enabled = True
        txtInput.Focus()
    End Sub

    Private Sub btnSpcial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpcial.Click
        lblstatus.Text = ""
        'MsgBox("hi")

        'Dim PrintDoc As New Printing.PrintDocument

        'Dim PrintDlg As New PrintDialog ' This allows you to choose a printer rather than to print on system default printer
        'PrintDlg.Document = PrintDoc
        'PrintDlg.PrinterSettings = PrintDoc.PrinterSettings

        ''If PrintDlg.ShowDialog <> DialogResult.OK Then
        ''    Exit Sub
        ''End If

        'AddHandler PrintDoc.PrintPage, AddressOf PrintHandler
        'PrintDoc.Print()
        'RemoveHandler PrintDoc.PrintPage, AddressOf PrintHandler


        'Specials New future
        Dim loadbutton As New CGDsql
        loadbutton.getSpecials()

        pnlPreSet.BringToFront()
        btnSpcial.Enabled = False
        btnSearch.Enabled = True
        btnKeypad.Enabled = True
        btnPreSet.Enabled = True
        txtInput.Focus()
    End Sub

    Private Sub PrintHandler(ByVal sender As Object, ByVal args As Printing.PrintPageEventArgs)
        Dim barcode As New STROKESCRIBELib.StrokeScribe


        barcode.Alphabet = STROKESCRIBELib.enumAlphabet.CODE128A
        barcode.Text = "ragul"

        Dim ag As Graphics = args.Graphics
        ag.PageUnit = GraphicsUnit.Pixel
        Dim bounds = ag.VisibleClipBounds

        Dim bar_w As Integer = barcode.BitmapW ' The minimum amount of pixels required to store the barcode image
        Dim bar_h As Integer = bar_w / 2 ' For linear barcodes, you can choose any value for the height
        ' For 2D barcodes (QR Code, Data Matrix, Aztec), you must use the same value for width and height (i.e. bar_h=bar_w)

        Dim olepicture As stdole.StdPicture
        olepicture = barcode.GetPictureHandle(STROKESCRIBELib.enumFormats.BMP, bar_w, bar_h)

        If barcode.Error Then
            MsgBox(barcode.ErrorDescription)
            Return
        End If

        Dim img As Image
        img = Image.FromHbitmap(olepicture.Handle)

        ' The barcode is centered vertically and horizontally on the label
        Dim x As Integer = bounds.Left + bounds.Width / 2 - bar_w / 2
        Dim y As Integer = bounds.Top + bounds.Height / 2 - bar_h / 2
        ag.DrawImage(img, x, y, bar_w, bar_h)

        Dim fnt As New Font("Arial", 10)
        ag.DrawString("A CODE 128 LABEL", fnt, Brushes.Black, bounds.Left, bounds.Top)

        args.HasMorePages = False ' Change this if you need to print multiple labels at once
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        frmService.ShowDialog()

    End Sub


    Private Sub btnOptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOptions.Click
        lblstatus.Text = ""
        Dim sqlCon As New CGDsql
        frmKeyPad.lblstatus.Text = "Enter Password"
        frmKeyPad.txtQuant.PasswordChar = "•"
        frmKeyPad.txtQuant.Text = ""
        frmKeyPad.ShowDialog()
        Dim value As String = frmKeyPad.txtQuant.Text

        'value = InputBox("Enter Password", "Coin Tail Pos")
        If value = password Then
            'MsgBox("Log in")
            frmOptions.btnAddItem.Visible = True ' 010
            frmOptions.btnDiscount.Visible = True ' 020
            frmOptions.btnDailySale.Visible = True ' 030
            frmOptions.btnDele.Visible = True ' 040
            frmOptions.btnDrawer.Visible = True ' 050
            frmOptions.btnMonthlySale.Visible = True ' 060
            frmOptions.btnAddStock.Visible = True ' 070
            frmOptions.btnChgPassword.Visible = True ' 080
            frmOptions.btnBarCode.Visible = True ' 090
            frmOptions.btnInventory.Visible = True ' 100
            frmOptions.btnDayCls.Visible = True ' 110
            frmOptions.btnSettings.Visible = True ' 120
            frmOptions.btnSoldItems.Visible = True ' 130
            frmOptions.btnexit.Visible = True ' 140
            frmOptions.ShowDialog()
        ElseIf value = "0" Then
            Dim guestUser As String = sqlCon.getSettingValue(5)

            frmOptions.btnAddItem.Visible = False ' 010
            frmOptions.btnDiscount.Visible = False ' 020
            frmOptions.btnDailySale.Visible = False ' 030
            frmOptions.btnDele.Visible = False ' 040
            frmOptions.btnDrawer.Visible = False ' 050
            frmOptions.btnMonthlySale.Visible = False ' 060
            frmOptions.btnAddStock.Visible = False ' 070
            frmOptions.btnChgPassword.Visible = False ' 080
            frmOptions.btnBarCode.Visible = False ' 090
            frmOptions.btnInventory.Visible = False ' 100
            frmOptions.btnDayCls.Visible = False ' 110
            frmOptions.btnSettings.Visible = False ' 120
            frmOptions.btnSoldItems.Visible = False ' 130
            frmOptions.btnUpdatedItem.Visible = False ' 140
            frmOptions.btnCategUpdate.Visible = False '150

            frmOptions.btnexit.Visible = False ' 991

            For x = 0 To ((guestUser.Length / 3) - 1)
                Dim strval As String = guestUser.Substring(x * 3, 3)
                If strval = "011" Then
                    frmOptions.btnAddItem.Visible = True ' 010
                ElseIf strval = "021" Then
                    frmOptions.btnDiscount.Visible = True ' 020
                ElseIf strval = "031" Then
                    frmOptions.btnDailySale.Visible = True ' 030
                ElseIf strval = "041" Then
                    frmOptions.btnDele.Visible = True ' 040
                ElseIf strval = "051" Then
                    frmOptions.btnDrawer.Visible = True ' 050
                ElseIf strval = "061" Then
                    frmOptions.btnMonthlySale.Visible = True ' 060
                ElseIf strval = "071" Then
                    frmOptions.btnAddStock.Visible = True ' 070
                ElseIf strval = "081" Then
                    frmOptions.btnChgPassword.Visible = True ' 080
                ElseIf strval = "091" Then
                    frmOptions.btnBarCode.Visible = True ' 090
                ElseIf strval = "101" Then
                    frmOptions.btnInventory.Visible = True ' 100
                ElseIf strval = "111" Then
                    frmOptions.btnDayCls.Visible = True ' 110
                ElseIf strval = "121" Then
                    frmOptions.btnSettings.Visible = True ' 120
                ElseIf strval = "131" Then
                    frmOptions.btnSoldItems.Visible = True ' 130
                ElseIf strval = "141" Then
                    frmOptions.btnUpdatedItem.Visible = True ' 140
                ElseIf strval = "151" Then
                    frmOptions.btnCategUpdate.Visible = True '150
                    'ElseIf strval = "141" Then
                ElseIf strval = "991" Then
                    frmOptions.btnexit.Visible = True ' 991
                End If
            Next
            frmOptions.ShowDialog()
        ElseIf value = "" Then            
        Else
            lblstatus.Text = "Incorrect Password"
        End If
        txtInput.Focus()
    End Sub


    Public Sub Pricing()
        ''buttonsArray.Text = prodName.Trim
        ''buttonsArray.Tag = prodPrice
        ''buttonsArray.AccessibleName = taxable
        ''buttonsArray.AccessibleDescription = productid
        ListView1.Items.Clear()

        Dim total As Double = 0
        For x = 0 To billList.Count - 1
            Dim billnum As New BillItems
            billnum = billList.Item(x)
            Dim lvItem As New ListViewItem
            lvItem.Text = 1 + x
            lvItem.SubItems.Add(billnum.qty)
            lvItem.SubItems.Add(billnum.ProductName)
            lvItem.SubItems.Add("$ " & (FormatNumber(billnum.Price * billnum.qty, 2)))
            ListView1.Items.Add(lvItem)
            total += (billnum.Price * billnum.qty)
        Next
        If billList.Count <= 0 Then
            discount = 0
            lblDiscountAmnt.Visible = False
            lblDiscount.Visible = False
        End If
        ''billList.Add(
        ''txtkeypadtotal.Text = ""
        'frmMain.totalAmt = frmMain.totalAmt + thisbutton.Tag
        total = total - discount

        lblNet.Text = (FormatNumber(total, 2))
        lblPst.Text = (FormatNumber(total * 0.13, 2))
        lblTotal.Text = (FormatNumber(total * 1.13, 2))

        If (cusDisplayType = 1) Then
            frmCusDisplay.ListView2.Items.Clear()
            For Each itm As ListViewItem In ListView1.Items

                frmCusDisplay.ListView2.Items.Add(itm.Clone())

            Next
            frmCusDisplay.txtTotal.Text = lblTotal.Text
            frmCusDisplay.txtSub.Text = lblNet.Text
            frmCusDisplay.txtHst.Text = lblPst.Text
        End If

    End Sub

    


    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged


        If txtSearch.Text.Length >= 3 Then

            Dim txtser As New CGDsql
            Dim searchArray As ArrayList
            Dim searchItem As New ProductItem
            searchArray = txtser.searchByKeyword(txtSearch.Text)
            'MsgBox(txtSearch.Text & " array size is " & searchArray.Count)

            ListView2.Items.Clear()

            For x = 0 To searchArray.Count - 1


                searchItem = searchArray.Item(x)
                'MsgBox(searchItem.ProductName)
                Dim lvitem As New ListViewItem
                lvitem.Text = x + 1
                lvitem.SubItems.Add(searchItem.StockQty)
                lvitem.SubItems.Add(searchItem.ProductName)
                lvitem.SubItems.Add(searchItem.ProductId)
                lvitem.SubItems.Add("$ " & (FormatNumber(searchItem.Price, 2)))
                lvitem.SubItems.Add(searchItem.remark)
                lvitem.SubItems.Add(searchItem.eof)
                ListView2.Items.Add(lvitem)
                'txtkeypadtotal.Text = ""
                'totalAmt = totalAmt + 2.05
            Next

        End If
    End Sub

    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        If ListView1.SelectedItems.Count < 1 Then
            lblstatus.Text = "You must select a item to void"
        Else
            Dim selNdx = ListView1.SelectedIndices(0)
            billList.RemoveAt(selNdx)
            ListView1.Items.RemoveAt(selNdx)
            Pricing()

        End If
    End Sub

    Private Sub btn_Qty_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Qty.Click
        If ListView1.SelectedItems.Count < 1 Then
            MessageBox.Show("You must select a item")
        Else
            Dim selNdx = ListView1.SelectedIndices(0)
            Dim userMsg As String
            Dim num As Integer
            Dim billqtychg As BillItems

            frmKeyPad.lblstatus.Text = "Enter Quantity"
            frmKeyPad.txtQuant.PasswordChar = ""
            frmKeyPad.txtQuant.Text = ""
            frmKeyPad.ShowDialog()
            userMsg = frmKeyPad.txtQuant.Text

            'userMsg = InputBox("Enter Number Of Quantity", "Enter Number Of Quantity 1-99", "", TopMost = True)

            Try
                num = userMsg
                billqtychg = billList.Item(selNdx)
                billList.RemoveAt(selNdx)
                ListView1.Items.RemoveAt(selNdx)

                billqtychg.qty = num
                billList.Add(billqtychg)
                Pricing()


            Catch InvalidCast As InvalidCastException
                lblstatus.Text = "Please enter a valid information"
            End Try
        End If

    End Sub


    Private Sub btnPayCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayCancel.Click
        'Dim updatebill As New CGDsql
        'updatebill.DeleteBill(currentBillID)
        CloseTabelView.Visible = False

    End Sub
    Dim currentBillID As Integer
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        lblstatus.Text = ""
        If (btnPrint.Text = "Log In") Then
            Dim sqlCon As New CGDsql
            frmKeyPad.lblstatus.Text = "Enter Password"
            frmKeyPad.txtQuant.PasswordChar = "•"
            frmKeyPad.txtQuant.Text = ""
            frmKeyPad.ShowDialog()
            Dim value As String = frmKeyPad.txtQuant.Text
            Dim user As User = sqlCon.getUser(value)
            If (user.ulevel > 0) Then
                lblUser.Text = user.name
                lblUser.Tag = user.id

                btnVoid.Enabled = True
                btn_Qty.Enabled = True
                btnPreSet.Enabled = True
                btnSpcial.Enabled = True
                btnKeypad.Enabled = True
                btnSearch.Enabled = True
                btnOptions.Enabled = True
                btnPrint.Text = "Finish Order"
            Else
                lblstatus.Text = "Incorrect User id entered."
            End If
        Else
            If billList.Count >= 1 Then
                Dim updatebill As New CGDsql
                currentBillID = (updatebill.getorderid) + 1
                'updatebill.CreateBill(currentBillID, billList)
                CloseTabelView.Show()
                lblBalanceDue.Text = lblTotal.Text
                'lblChange.Text = (500 - lblBalanceDue.Text)
                clsBill.user = lblUser.Tag
                clsBill.amex = 0
                clsBill.cash = 0
                clsBill.debit = 0
                clsBill.discAmount = discount
                clsBill.GiftCert = 0
                clsBill.HST = lblPst.Text
                clsBill.master = 0
                clsBill.other = 0
                clsBill.StoreCredit = 0
                clsBill.Subtotal = lblNet.Text
                clsBill.Total = lblTotal.Text
                clsBill.visa = 0
                lblPayment.Text = "-"
                txtInput.Focus()
            Else
                lblstatus.Text = "Please enter atleast one item"
            End If
        End If

    End Sub

    Private Sub btnPayFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayFinish.Click
        lblstatus.Text = "Change = " + lblChange.Text
        frmCusDisplay.lblchg.Text = "0.00"

        If (((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)) >= 0 Then
            clsBill.cash = clsBill.cash - lblChange.Text

            Dim updatebill As New CGDsql
            Dim takeitem As BillItems

            updatebill.FinishOrder(currentBillID, clsBill)
            updatebill.CreateBill(currentBillID, billList)
            For x = 0 To billList.Count - 1
                takeitem = billList.Item(x)
                updatebill.updateQuantity(takeitem.ProductId, (-1 * takeitem.qty))
            Next


            currentBillID = 0
            billList.Clear()
            lblDiscount.Visible = False
            lblDiscountAmnt.Visible = False

            Pricing()


            CloseTabelView.Visible = False
            txtInput.Focus()
        Else
            lblstatus.Text = "Plese Enter the correct Amount Given"

        End If


    End Sub

    Private Sub btnselSerch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnselSerch.Click
        If ListView2.SelectedItems.Count < 1 Then
            MessageBox.Show("You must select a item")
        Else
            Dim billitm As BillItems
            billitm.qty = 1
            billitm.ProductName = ListView2.SelectedItems(0).SubItems(2).Text.Trim
            billitm.ProductId = ListView2.SelectedItems(0).SubItems(3).Text
            billitm.Price = ListView2.SelectedItems(0).SubItems(4).Text
            billitm.txt = ListView2.SelectedItems(0).SubItems(5).Text
            billList.Add(billitm)

            If ListView2.SelectedItems(0).SubItems(6).Text > 0 Then
                billitm.qty = 1
                billitm.ProductName = "Oes"
                billitm.ProductId = 10107
                billitm.Price = ListView2.SelectedItems(0).SubItems(6).Text
                billList.Add(billitm)
                'billitm.
                'MsgBox(searchItem.remark)
                'billitm.txt = searchItem.remark
            End If

            Pricing()

        End If

    End Sub
    Dim clsBill As New closeBill
    Public discount As Double = 0
    Private Sub btnPayCash_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayCash.Click
        Dim userMsg As String
        Dim num As Double
        frmKeyPad.lblstatus.Text = "Cash Amount"
        frmKeyPad.txtQuant.PasswordChar = ""
        frmKeyPad.txtQuant.Text = ""
        frmKeyPad.ShowDialog()
        userMsg = frmKeyPad.txtQuant.Text

        'userMsg = InputBox("Enter Amount", "Enter Cash Amount", lblBalanceDue.Text, TopMost = True)
        Try
            num = userMsg
            clsBill.cash = num
            lblPayment.Text = lblPayment.Text & " Cash  $ " & num
            lblChange.Text = ((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)
            frmCusDisplay.lblchg.Text = lblChange.Text
            cashDrawerOpen()
            'prtCashDrawer()
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub btnPayVisa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayVisa.Click
        Dim userMsg As String
        Dim num As Double
        frmKeyPad.lblstatus.Text = "Visa Amount"
        frmKeyPad.txtQuant.PasswordChar = ""
        frmKeyPad.txtQuant.Text = lblBalanceDue.Text
        frmKeyPad.ShowDialog()
        userMsg = frmKeyPad.txtQuant.Text

        'userMsg = InputBox("Enter Amount", "Enter Visa Amount", lblBalanceDue.Text, TopMost = True)
        Try
            num = userMsg
            clsBill.visa = num
            lblPayment.Text = lblPayment.Text & " Visa  $ " & num
            lblChange.Text = ((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)
            frmCusDisplay.lblchg.Text = lblChange.Text
            cashDrawerOpen()
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub btnPayMC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayMC.Click
        Dim userMsg As String
        Dim num As Double
        frmKeyPad.lblstatus.Text = "MasterCard Amount"
        frmKeyPad.txtQuant.PasswordChar = ""
        frmKeyPad.txtQuant.Text = lblBalanceDue.Text
        frmKeyPad.ShowDialog()
        userMsg = frmKeyPad.txtQuant.Text
        'userMsg = InputBox("Enter Amount", "Enter MasterCard Amount", lblBalanceDue.Text, TopMost = True)
        Try
            num = userMsg
            clsBill.master = num
            lblPayment.Text = lblPayment.Text & " MasterCard  $ " & num
            lblChange.Text = ((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)
            frmCusDisplay.lblchg.Text = lblChange.Text
            cashDrawerOpen()
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim userMsg As String
        Dim num As Double
        frmKeyPad.lblstatus.Text = "GiftCard Amount"
        frmKeyPad.txtQuant.PasswordChar = ""
        frmKeyPad.txtQuant.Text = lblBalanceDue.Text
        frmKeyPad.ShowDialog()
        userMsg = frmKeyPad.txtQuant.Text
        'userMsg = InputBox("Enter Amount", "Enter GiftCertificate Amount", lblBalanceDue.Text, TopMost = True)
        Try
            num = userMsg
            clsBill.GiftCert = num
            lblPayment.Text = lblPayment.Text & " GiftCertificate  $ " & num
            lblChange.Text = ((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)
            frmCusDisplay.lblchg.Text = lblChange.Text
            cashDrawerOpen()
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub btnPayDebit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayDebit.Click
        Dim userMsg As String
        Dim num As Double
        frmKeyPad.lblstatus.Text = "Debit Amount"
        frmKeyPad.txtQuant.PasswordChar = ""
        frmKeyPad.txtQuant.Text = lblBalanceDue.Text
        frmKeyPad.ShowDialog()
        userMsg = frmKeyPad.txtQuant.Text
        'userMsg = InputBox("Enter Amount", "Enter Debit Amount", lblBalanceDue.Text, TopMost = True)
        Try
            num = userMsg
            clsBill.debit = num
            lblPayment.Text = lblPayment.Text & " Debit  $ " & num
            lblChange.Text = ((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)
            frmCusDisplay.lblchg.Text = lblChange.Text
            cashDrawerOpen()
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub btnPayOther_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayOther.Click
        Dim userMsg As String
        Dim num As Double
        frmKeyPad.lblstatus.Text = "Other Amount"
        frmKeyPad.txtQuant.PasswordChar = ""
        frmKeyPad.txtQuant.Text = lblBalanceDue.Text
        frmKeyPad.ShowDialog()
        userMsg = frmKeyPad.txtQuant.Text
        'userMsg = InputBox("Enter Amount", "Enter Other Amount", lblBalanceDue.Text, TopMost = True)
        Try
            num = userMsg
            clsBill.other = num
            lblPayment.Text = lblPayment.Text & " Other  $ " & num
            lblChange.Text = ((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)
            frmCusDisplay.lblchg.Text = lblChange.Text
            cashDrawerOpen()
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub btnPayAmex_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayAmex.Click
        Dim userMsg As String
        Dim num As Double
        frmKeyPad.lblstatus.Text = "Amex Amount"
        frmKeyPad.txtQuant.PasswordChar = ""
        frmKeyPad.txtQuant.Text = lblBalanceDue.Text
        frmKeyPad.ShowDialog()
        userMsg = frmKeyPad.txtQuant.Text
        'userMsg = InputBox("Enter Amount", "Enter Amex Amount", lblBalanceDue.Text, TopMost = True)
        Try
            num = userMsg
            clsBill.amex = num
            lblPayment.Text = lblPayment.Text & " Amex  $ " & num
            lblChange.Text = ((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)
            frmCusDisplay.lblchg.Text = lblChange.Text
            cashDrawerOpen()
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim userMsg As String
        Dim num As Double
        frmKeyPad.lblstatus.Text = "Store Credit Amount"
        frmKeyPad.txtQuant.PasswordChar = ""
        frmKeyPad.txtQuant.Text = lblBalanceDue.Text
        frmKeyPad.ShowDialog()
        userMsg = frmKeyPad.txtQuant.Text
        'userMsg = InputBox("Enter Amount", "Enter Store Credit Amount", lblBalanceDue.Text, TopMost = True)
        Try
            num = userMsg
            clsBill.StoreCredit = num
            lblPayment.Text = lblPayment.Text & " StoreCredit  $ " & num
            lblChange.Text = ((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)
            frmCusDisplay.lblchg.Text = lblChange.Text
            cashDrawerOpen()
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    '++++++++++++++++++++================================PRINTER================+++++++++++++++++++++++++
    Public Sub prtCashDrawer()
        'Open the cash drawer of the printer
        Dim P As New PrinterClass(Application.StartupPath)
        With P
            .RTL = False
            .WriteChars("")
            .EndDoc()
        End With
    End Sub
    Public Sub cashDrawerOpen()
        Dim prtOpenCash As Char() = (Chr(27) & Chr(112) & Chr(0) & Chr(25) & Chr(30))
        RawPrinterHelper.SendStringToPrinter(printerName, prtOpenCash)
    End Sub


    Public Sub printerPrint(ByVal Bill As ArrayList, ByVal printItem As closeBill)
        Dim P As New PrinterClass(Application.StartupPath)

        With P
            'Printing Logo
            .RTL = False
            '.PrintLogo()

            .AlignCenter()
            .BigFont()
            '.Bold = True
            .WriteLine(storName)
            If (storName2.Length() > 0) Then
                .WriteLine(storName2)
            End If
            .NormalFont()
            .WriteLine(storeAddress)
            .WriteLine(storeAddress2)
            .WriteLine(storePhone)

            .AlignRight()

            'Printing Date
            .FeedPaper(1)
            '.GotoSixth(1)
            .WriteChars("Date:  " & DateTime.Now.ToString & "    Chk#  " & printItem.orderid)
            '.WriteChars(DateTime.Now.ToString)
            .WriteLine("")



            'Printing Header
            .GotoSixth(1)
            .WriteChars("Qty")
            .GotoCol(5)
            .WriteChars("Description")
            .GotoSixth(7)
            .WriteChars("Unit")
            .GotoSixth(9)
            .WriteChars("Price")
            .WriteLine("")
            .WriteLine("------------------------------------------------------------------------------------------")
            '.DrawLine()
            '.FeedPaper(1)

            'Printing Items
            '.SmallFont()

            Dim i As Integer
            For i = 0 To Bill.Count - 1

                Dim billnum As New BillItems
                billnum = Bill.Item(i)
                .GotoSixth(1)
                '.AlignRight()
                .WriteChars(billnum.qty)
                .GotoCol(3)
                .AlignLeft()
                If (billnum.ProductName).Length > 24 Then
                    .WriteChars((billnum.ProductName).Substring(0, 24))
                Else
                    .WriteChars(billnum.ProductName)
                End If



                Dim uprnPrice As Double = billnum.Price
                uprnPrice = Math.Round(uprnPrice, 2)
                Dim upriceStr As String
                upriceStr = uprnPrice.ToString("####0.00")

                If upriceStr.Length >= 6 Then
                    .GotoCol(28)
                    '    upriceStr = ("   " + upriceStr)
                ElseIf upriceStr.Length = 5 Then
                    .GotoCol(29)
                Else
                    .GotoCol(30)
                    '    upriceStr = (" " + upriceStr)
                End If
                .WriteChars(upriceStr)

                .GotoSixth(8)
                Dim prnPrice As Double = billnum.Price * billnum.qty
                prnPrice = Math.Round(prnPrice, 2)
                Dim priceStr As String
                priceStr = prnPrice.ToString("####0.00        -")
                .AlignRight()
                .WriteLine(priceStr)


                If billnum.txt.Length > 1 Then
                    .AlignLeft()
                    .WriteLine(billnum.txt)
                End If

            Next

            'Printing Totals
            '.NormalFont()
            '.DrawLine()
            .WriteLine("------------------------------------------------------------------------------------------")
            If discount > 0 Then
                .GotoSixth(1)
                .WriteChars("Discounts")
                .GotoSixth(8)
                Dim prndis As String
                prndis = discount.ToString("####0.00        -")
                .AlignRight()
                .WriteLine(prndis)
            End If
            .GotoSixth(1)
            .WriteChars("Sub-Total")
            .GotoSixth(8)
            Dim prnsub As String
            prnsub = printItem.Subtotal.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(prnsub)

            .GotoSixth(1)
            .WriteChars(storeHST)
            .GotoSixth(8)
            Dim prnthst As String
            prnthst = printItem.HST.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(prnthst)
            '.DrawLine()
            .WriteLine("------------------------------------------------------------------------------------------")
            .GotoSixth(1)
            '.UnderlineOn()

            Dim prntotal As String
            prntotal = printItem.Total.ToString("####0.00        -")

            '.BigFont()
            .WriteChars("Total")
            '.UnderlineOff()
            .GotoSixth(8)
            .AlignRight()
            .WriteLine("$ " & prntotal)

            .FeedPaper(1)
            If printItem.cash > 0 Then
                Dim cashadd As Double
                cashadd = printItem.cash + printItem.cashtend
                Dim paidType As String
                paidType = cashadd.ToString("####0.00        -")
                '.BigFont()
                .WriteChars("Cash")
                '.UnderlineOff()
                .GotoSixth(8)
                .AlignRight()
                .WriteLine("$ " & paidType)
            End If
            If printItem.visa > 0 Then
                Dim paidType As String
                paidType = printItem.visa.ToString("####0.00        -")
                '.BigFont()
                .WriteChars("Visa")
                '.UnderlineOff()
                .GotoSixth(8)
                .AlignRight()
                .WriteLine("$ " & paidType)
            End If
            If printItem.amex > 0 Then
                Dim paidType As String
                paidType = printItem.amex.ToString("####0.00        -")
                '.BigFont()
                .WriteChars("Amex")
                '.UnderlineOff()
                .GotoSixth(8)
                .AlignRight()
                .WriteLine("$ " & paidType)
            End If
            If printItem.other > 0 Then
                Dim paidType As String
                paidType = printItem.other.ToString("####0.00        -")
                '.BigFont()
                .WriteChars("Other Payment")
                '.UnderlineOff()
                .GotoSixth(8)
                .AlignRight()
                .WriteLine("$ " & paidType)
            End If
            If printItem.debit > 0 Then
                Dim paidType As String
                paidType = printItem.debit.ToString("####0.00        -")
                '.BigFont()
                .WriteChars("Debit")
                '.UnderlineOff()
                .GotoSixth(8)
                .AlignRight()
                .WriteLine("$ " & paidType)
            End If
            If printItem.GiftCert > 0 Then
                Dim paidType As String
                paidType = printItem.GiftCert.ToString("####0.00        -")
                '.BigFont()
                .WriteChars("Gift Certificate")
                '.UnderlineOff()
                .GotoSixth(8)
                .AlignRight()
                .WriteLine("$ " & paidType)
            End If
            If printItem.StoreCredit > 0 Then
                Dim paidType As String
                paidType = printItem.StoreCredit.ToString("####0.00        -")
                '.BigFont()
                .WriteChars("Store Credit")
                '.UnderlineOff()
                .GotoSixth(8)
                .AlignRight()
                .WriteLine("$ " & paidType)
            End If
            Dim change As String
            change = printItem.cashtend.ToString("####0.00        -")
            '.BigFont()
            .WriteChars("Change")
            '.UnderlineOff()
            .GotoSixth(8)
            .AlignRight()
            .WriteLine("$ " & change)


            .AlignCenter()
            .NormalFont()
            .WriteLine("")
            .WriteLine(footer2)
            .WriteLine(footer3)

            '.WriteLine("")




            '.WriteLine("Visit our web site at www.ohmcomputer.com       .")

            '.NormalFont()
            '.WriteLine("")
            '.GotoSixth(1)
            '.WriteChars("Cash Tend")
            '.GotoSixth(8)
            'Dim prnCashTend As String
            'prnCashTend = printItem.cashtend.ToString("####0.00        -")
            '.AlignRight()
            '.WriteLine(prnCashTend)

            '.GotoSixth(1)
            '.WriteChars("Change")
            '.GotoSixth(8)
            'Dim prnchange As String
            'Dim xchg As Double
            'xchg = printItem.cashtend - printItem.Total
            'prnchange = xchg.ToString("####0.00        -")
            '.AlignRight()
            '.WriteLine(prnchange)
            '.WriteLine("")


            .BigFont()


            .AlignCenter()
            .WriteLine(footer4)
            .FeedPaper(3)
            .CutPaper() ' Can be used with real printer to cut the paper.


            'PrintLine()
            'Ending the session
            .EndDoc()
        End With


    End Sub

    Public Sub printerPrintSales(ByVal printItem As closeBill, ByVal txt As String)
        Dim P As New PrinterClass(Application.StartupPath)

        With P
            'Printing Logo
            .RTL = False
            '.PrintLogo()

            .AlignCenter()
            .BigFont()
            '.Bold = True
            .WriteLine(storName)
            If (storName2.Length() > 0) Then
                .WriteLine(storName2)
            End If
            .WriteLine("Sale Reports")
            .NormalFont()
            .WriteLine(storeAddress)
            .WriteLine(storePhone)

            .AlignLeft()

            'Printing Date
            .FeedPaper(1)
            '.GotoSixth(1)
            .WriteLine("Date:  " & DateTime.Now.ToString)
            .WriteLine("Rpt:  " & txt)
            '.WriteChars(DateTime.Now.ToString)
            .WriteLine("")
            .WriteLine("------------------------------------------------------------------------------------------")
            '.DrawLine()
            '.FeedPaper(1)
            .GotoSixth(1)
            .WriteChars("Cash")
            .GotoSixth(8)
            Dim cash As String
            cash = printItem.cash.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(cash)

            .GotoSixth(1)
            .WriteChars("Visa")
            .GotoSixth(8)
            Dim visa As String
            visa = printItem.visa.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(visa)

            .GotoSixth(1)
            .WriteChars("MC")
            .GotoSixth(8)
            Dim master As String
            master = printItem.master.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(master)


            .GotoSixth(1)
            .WriteChars("Amex")
            .GotoSixth(8)
            Dim amex As String
            amex = printItem.amex.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(amex)

            .GotoSixth(1)
            .WriteChars("Other")
            .GotoSixth(8)
            Dim other As String
            other = printItem.other.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(other)

            .GotoSixth(1)
            .WriteChars("Debit")
            .GotoSixth(8)
            Dim debit As String
            debit = printItem.debit.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(debit)

            .GotoSixth(1)
            .WriteChars("Gift Certificate")
            .GotoSixth(8)
            Dim gift As String
            gift = printItem.GiftCert.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(gift)

            .GotoSixth(1)
            .WriteChars("Store Credit")
            .GotoSixth(8)
            Dim scredit As String
            scredit = printItem.StoreCredit.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(scredit)

            .GotoSixth(1)
            .WriteChars("Discount")
            .GotoSixth(8)
            Dim disc As String
            disc = printItem.discAmount.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(disc)




            'Printing Items
            '.SmallFont()

            'Printing Totals
            '.NormalFont()
            '.DrawLine()
            .WriteLine("------------------------------------------------------------------------------------------")
            .GotoSixth(1)
            .WriteChars("Net Total")
            .GotoSixth(8)
            Dim prnsub As String
            prnsub = printItem.Subtotal.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(prnsub)

            .GotoSixth(1)
            .WriteChars("HST Total")
            .GotoSixth(8)
            Dim prnthst As String
            prnthst = printItem.HST.ToString("####0.00        -")
            .AlignRight()
            .WriteLine(prnthst)
            '.DrawLine()
            .WriteLine("------------------------------------------------------------------------------------------")
            .GotoSixth(1)
            '.UnderlineOn()

            Dim prntotal As String
            prntotal = printItem.Total.ToString("####0.00     -")


            .BigFont()
            .WriteChars("Grand Total")
            '.UnderlineOff()
            .GotoSixth(8)
            .AlignRight()
            .WriteLine("$ " & prntotal)
            '.WriteChars(FormatCurrency(printItem.Total))

            .WriteLine("")
            .BigFont()
            '.SetFont(9.5,"control",False
            '.WriteLine((Chr(27) & Chr(112) & Chr(0) & Chr(25) & Chr(30)))
            .AlignCenter()
            .WriteLine("Thank You!")
            .WriteLine("")
            .CutPaper() ' Can be used with real printer to cut the paper.
            .EndDoc()
        End With


    End Sub

    Private Sub btnPrintBill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintBill.Click
        lblstatus.Text = "Change = " + lblChange.Text
        frmCusDisplay.lblchg.Text = "0.00"
        'Dim x As New closeBill
        'x.orderid = currentBillID
        'x.Subtotal = lblNet.Text
        'x.HST = lblPst.Text
        'x.Total = lblTotal.Text
        'printerPrint(billList, x)



        If (((clsBill.amex + clsBill.cash + clsBill.debit + clsBill.GiftCert + clsBill.master + clsBill.other + clsBill.StoreCredit + clsBill.visa) - lblBalanceDue.Text)) >= 0 Then
            clsBill.cash = clsBill.cash - lblChange.Text

            Dim updatebill As New CGDsql
            Dim takeitem As BillItems

            updatebill.FinishOrder(currentBillID, clsBill)
            updatebill.CreateBill(currentBillID, billList)
            For x = 0 To billList.Count - 1
                takeitem = billList.Item(x)
                updatebill.updateQuantity(takeitem.ProductId, (-1 * takeitem.qty))
            Next
            clsBill.orderid = currentBillID
            clsBill.cashtend = lblChange.Text
            printerPrint(billList, clsBill)

            currentBillID = 0
            billList.Clear()
            lblDiscount.Visible = False
            lblDiscountAmnt.Visible = False

            Pricing()


            CloseTabelView.Visible = False
            txtInput.Focus()
        Else
            lblstatus.Text = "Plese Enter the correct Amount Given"
        End If

    End Sub

    Private Sub ListView1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListView1.MouseDoubleClick
        If ListView1.SelectedItems.Count < 1 Then
            MessageBox.Show("You must select a item")
        Else

            frmChgPrice.ShowDialog()
        End If


    End Sub


    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Try

            '            MsgBox(slideimages.Item(currentimg))
            ' MsgBox(currentimg & "  " & slideimages.Count)



            If currentimg < slideimages.Count Then

                Dim filename As String

                filename = ".\images\" & slideimages.Item(currentimg)
                ' MsgBox(filename)
                frmCusDisplay.PictureBox2.BackgroundImage = System.Drawing.Image.FromFile(filename)

                'MsgBox(slideimages.Item(currentimg))
                currentimg += 1
                'MsgBox(filename)
            Else
                currentimg = 0
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
