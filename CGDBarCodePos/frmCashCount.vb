﻿Option Strict Off
Public Class frmCashCount
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            Const CS_DBLCLKS As Int32 = &H8
            Const CS_NOCLOSE As Int32 = &H200
            cp.ClassStyle = CS_DBLCLKS Or CS_NOCLOSE
            Return cp
        End Get

    End Property
    Private Sub btnResume_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()

    End Sub

    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        Dim numTotal As Double
        Try
            numTotal = txttotal.Text

            If numTotal > 0 Then
                Dim clsday As closeDay
                clsday.Userid = 1001
                clsday.Status = "open"
                clsday.bill100 = txt100.Text
                clsday.bill50 = txt50.Text
                clsday.bill20 = txt20.Text
                clsday.bill10 = txt10.Text
                clsday.bill5 = txt5.Text
                clsday.coin2 = txt2.Text
                clsday.coin1 = txt1.Text
                clsday.coin25 = txtd25.Text
                clsday.coin10 = txtd10.Text
                clsday.coin5 = txtd05.Text
                clsday.cashTotal = txttotal.Text
                clsday.total = 0
                clsday.subtotal = 0
                clsday.hst = 0
                clsday.cash = 0
                clsday.visa = 0
                clsday.master = 0
                clsday.amex = 0
                clsday.other = 0
                clsday.debit = 0
                clsday.giftcert = 0
                clsday.store_credit = 0
                clsday.discount = 0

                Dim clsday2 As New CGDsql
                clsday2.openCloseDay(clsday)

                Me.Close()
            Else
                MsgBox("Please Enter Valid Information")
            End If

        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim total As Double
        Try

            total = (txt100.Text * 100) + (txt50.Text * 50) + (txt20.Text * 20) + (txt10.Text * 10) + (txt5.Text * 5) + (txt2.Text * 2) + (txt1.Text) + (txtd25.Text * 0.25) + (txtd10.Text * 0.1) + (txtd05.Text * 0.05)
            txttotal.Text = total

        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub frmCashCount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnClose.Visible = False
        btnOpen.Visible = False

        Dim chkstatus As New CGDsql
        If (chkstatus.getDayStatus = "open") Then
            btnClose.Visible = True


        ElseIf (chkstatus.getDayStatus = "close") Then
            btnOpen.Visible = True

        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim numTotal As Double
        Try
            numTotal = txttotal.Text

            If numTotal > 0 Then
                Dim clsday As closeDay
                clsday.Userid = 1001
                clsday.Status = "close"
                clsday.bill100 = txt100.Text
                clsday.bill50 = txt50.Text
                clsday.bill20 = txt20.Text
                clsday.bill10 = txt10.Text
                clsday.bill5 = txt5.Text
                clsday.coin2 = txt2.Text
                clsday.coin1 = txt1.Text
                clsday.coin25 = txtd25.Text
                clsday.coin10 = txtd10.Text
                clsday.coin5 = txtd05.Text
                clsday.cashTotal = txttotal.Text
                clsday.total = 0
                clsday.subtotal = 0
                clsday.hst = 0
                clsday.cash = 0
                clsday.visa = 0
                clsday.master = 0
                clsday.amex = 0
                clsday.other = 0
                clsday.debit = 0
                clsday.giftcert = 0
                clsday.store_credit = 0
                clsday.discount = 0

                Dim clsday2 As New CGDsql
                clsday2.openCloseDay(clsday)

                Me.Close()
            Else
                MsgBox("Please Enter Valid Information")
            End If

        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim aryBill As New ArrayList
            Dim bill As New BillItems
            Dim clsbill As New closeBill
            bill.qty = txtd05.Text
            bill.ProductName = " $.05 Coin "
            bill.Price = 0.05
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txtd10.Text
            bill.ProductName = " $.10 Coin "
            bill.Price = 0.1
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txtd25.Text
            bill.ProductName = " $.25 Coin "
            bill.Price = 0.25
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txt1.Text
            bill.ProductName = " $1.00 Coin "
            bill.Price = 1.0
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txt2.Text
            bill.ProductName = " $2.00 Coin "
            bill.Price = 2.0
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txt5.Text
            bill.ProductName = " $5.00 Bill "
            bill.Price = 5.0
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txt10.Text
            bill.ProductName = " $10.00 Bill "
            bill.Price = 10.0
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txt20.Text
            bill.ProductName = " $20.00 Bill "
            bill.Price = 20.0
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txt50.Text
            bill.ProductName = " $50.00 Bill "
            bill.Price = 50.0
            bill.txt = ""
            aryBill.Add(bill)

            bill.qty = txt100.Text
            bill.ProductName = " $100.00 Bill "
            bill.Price = 100.0
            bill.txt = ""
            aryBill.Add(bill)

            Dim amount As Double
            amount = txttotal.Text

            clsbill.HST = 0
            clsbill.orderid = 0
            clsbill.Subtotal = 0
            clsbill.Total = amount

            frmMain.printerPrint(aryBill, clsbill)

        Catch ex As Exception
            MsgBox("please check your information you entered")
        End Try
        'Dim printRept As New CGDsql



        'frmMain.SerialPort1.WriteLine(txt100.Text.PadLeft(3, Chr(32)) + " $100 Bill " + ((txt100.Text * 100).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txt50.Text.PadLeft(3, Chr(32)) + " $ 50 Bill " + ((txt50.Text * 50).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txt20.Text.PadLeft(3, Chr(32)) + " $ 20 Bill " + ((txt20.Text * 20).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txt10.Text.PadLeft(3, Chr(32)) + " $ 10 Bill " + ((txt10.Text * 10).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txt5.Text.PadLeft(3, Chr(32)) + " $  5 Bill " + ((txt5.Text * 5).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txt2.Text.PadLeft(3, Chr(32)) + " $  2 Coin " + ((txt2.Text * 2).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txt1.Text.PadLeft(3, Chr(32)) + " $  1 Coin " + ((txt1.Text * 1).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txtd25.Text.PadLeft(3, Chr(32)) + " $.25 Coin " + ((txtd25.Text * 0.25).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txtd10.Text.PadLeft(3, Chr(32)) + " $.10 Coin " + ((txtd10.Text * 0.1).ToString.PadLeft(6, Chr(32))))
        'frmMain.SerialPort1.WriteLine(txtd05.Text.PadLeft(3, Chr(32)) + " $.05 Coin " + ((txtd05.Text * 0.05).ToString.PadLeft(6, Chr(32))))




        'Dim amount As Double
        'amount = txttotal.Text
        'frmMain.SerialPort1.WriteLine("Total                          " & (FormatCurrency(amount)).PadLeft(10, Chr(32)))

        'If btnClose.Visible Then


        '    cashcnt = amount - printRept.getCashSale(day1, day2) - printRept.getLastTillAmount
        '    If cashcnt = 0 Then
        '    ElseIf cashcnt < 0 Then
        '        frmMain.SerialPort1.WriteLine("Cash Short                     " & (FormatCurrency(cashcnt)).PadLeft(10, Chr(32)))
        '    ElseIf cashcnt > 0 Then
        '        frmMain.SerialPort1.WriteLine("Cash Over                      " & (FormatCurrency(cashcnt)).PadLeft(10, Chr(32)))
        '    End If
        'End If



    End Sub
End Class