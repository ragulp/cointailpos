﻿Option Strict Off
Public Class frmDateChoose
    Public date1, date2, dateRange As String
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim today, tommrow, endDate As Date
        Dim day1, day2 As String
        Dim retbill As New CGDsql

        today = DateTimePicker1.Value.Date
        endDate = DateTimePicker2.Value.Date
        tommrow = DateTimePicker2.Value.Date.AddDays(1)
        day1 = today.ToString("yyyy-MM-dd") & " 06:00:00"
        day2 = tommrow.ToString("yyyy-MM-dd") & " 06:00:00"
        frmReport.printReportDate = (today.ToString("yyyy-MM-dd") & " - " & endDate.ToString("yyyy-MM-dd"))
        frmOptions.getsales(retbill.getTodaySale(day1, day2), retbill.getSalesTotal(day1, day2))


    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim today, tommrow, endDate As Date
        Dim day1, day2 As String
        Dim retbill As New CGDsql
        Dim sales As ArrayList
        Dim saleTotall As orders
        today = DateTimePicker1.Value.Date
        endDate = DateTimePicker2.Value.Date
        tommrow = DateTimePicker2.Value.Date.AddDays(1)
        day1 = today.ToString("yyyy-MM-dd") & " 00:01:00"
        day2 = tommrow.ToString("yyyy-MM-dd") & " 00:01:00"
        Dim reportDate As String = (today.ToString("yyyy-MM-dd") & " to " & endDate.ToString("yyyy-MM-dd"))
        sales = retbill.getSoldItems(day1, day2)
        saleTotall = retbill.getSalesTotal(day1, day2)



        Dim ds As New DataSet1
        Dim t As DataTable = ds.Tables.Add("DataTable6")

        t.Columns.Add("items", Type.GetType("System.String"))
        t.Columns.Add("costPrice", Type.GetType("System.Double"))
        t.Columns.Add("qty", Type.GetType("System.Int16"))
        t.Columns.Add("soldPrice", Type.GetType("System.Double"))
        t.Columns.Add("Revenue", Type.GetType("System.Double"))
        t.Columns.Add("rptDate", Type.GetType("System.String"))
        t.Columns.Add("Total", Type.GetType("System.Double"))
        t.Columns.Add("Hst", Type.GetType("System.Double"))
        t.Columns.Add("NetTotal", Type.GetType("System.Double"))
        t.Columns.Add("Cash", Type.GetType("System.Double"))
        t.Columns.Add("Visa", Type.GetType("System.Double"))
        t.Columns.Add("Amex", Type.GetType("System.Double"))
        t.Columns.Add("OtherPay", Type.GetType("System.Double"))
        t.Columns.Add("Debit", Type.GetType("System.Double"))
        t.Columns.Add("GiftCert", Type.GetType("System.Double"))
        t.Columns.Add("StoreCred", Type.GetType("System.Double"))
        t.Columns.Add("Discount", Type.GetType("System.Double"))
        t.Columns.Add("MC", Type.GetType("System.Double"))

        For x = 0 To sales.Count - 2
            Dim iSold As New ProductItem
            iSold = sales.Item(x)
            Dim r As DataRow

            r = t.NewRow
            r("items") = iSold.ProductName
            r("costPrice") = iSold.CostPrice
            r("qty") = iSold.StockQty
            r("soldPrice") = iSold.Price
            r("Revenue") = (iSold.Price - (iSold.CostPrice * iSold.StockQty))
            r("rptDate") = reportDate
            t.Rows.Add(r)

        Next
        If sales.Count > 1 Then


            Dim iSold2 As New ProductItem
            iSold2 = sales.Item(sales.Count - 1)
            Dim r2 As DataRow

            r2 = t.NewRow
            r2("items") = iSold2.ProductName
            r2("costPrice") = iSold2.CostPrice
            r2("qty") = iSold2.StockQty
            r2("soldPrice") = iSold2.Price
            r2("Revenue") = (iSold2.Price - (iSold2.CostPrice * iSold2.StockQty))

            r2("rptDate") = reportDate
            r2("Total") = saleTotall.total
            r2("HST") = saleTotall.hst
            r2("NetTotal") = saleTotall.subtotal
            r2("Cash") = saleTotall.cash
            r2("Visa") = saleTotall.visa
            r2("Amex") = saleTotall.amex
            r2("OtherPay") = saleTotall.other
            r2("Debit") = saleTotall.debit
            r2("GiftCert") = saleTotall.GiftCert
            r2("StoreCred") = saleTotall.StoreCredit
            r2("Discount") = saleTotall.discount
            r2("MC") = saleTotall.master

            t.Rows.Add(r2)





            Dim objRpt As New CrystalReport2
            objRpt.SetDataSource(ds.Tables("DataTable6"))
            frmRetportView.CrystalReportViewer1.ReportSource = objRpt
            frmRetportView.CrystalReportViewer1.Refresh()
            frmRetportView.ShowDialog()
        Else
            MsgBox("No Sale To Display")
        End If



    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        dateselected()
        Me.Close()
    End Sub

    Private Sub frmDateChoose_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dateselected()
    End Sub
    Private Sub dateselected()
        Dim today, tommrow, endDate As Date
        today = DateTimePicker1.Value.Date
        endDate = DateTimePicker2.Value.Date
        tommrow = DateTimePicker2.Value.Date.AddDays(1)
        date1 = today.ToString("yyyy-MM-dd") & " 00:01:00"
        date2 = tommrow.ToString("yyyy-MM-dd") & " 00:01:00"
        Dim dateRange As String = (today.ToString("yyyy-MM-dd") & " to " & endDate.ToString("yyyy-MM-dd"))
    End Sub
End Class