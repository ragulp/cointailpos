﻿Option Strict Off
Public Class frmAddProduct

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim Response As Integer
        Dim additem As New ProductItem
        Dim cgdslq As New CGDsql
        Try
            If txtBarcode.Text = "" Then
                txtBarcode.Text = "NoBarcode"
            End If
            additem.BarCode = txtBarcode.Text
            additem.SecondBarCode = txtBarcode2.Text
            additem.CostPrice = txtCost.Text

            additem.Price = txtPrice.Text
            additem.ProductName = txtName.Text
            additem.StockQty = txtQty.Text
            additem.Taxable = chkTaxable.CheckState
            additem.Refurb = chkRefurb.CheckState
            additem.ManufactureName = txtManufacture.Text
            additem.Description = txtDesc.Text
            additem.Category = lblCategory.Text
            additem.eof = txteof.Text
            additem.remark = cmbRemark.Text


            ' MsgBox(txtCateg.Items.Item)

            ' Displays a message box with the yes and no options.
            'MsgBox(additem.Taxable)




            cgdslq.addProduct(additem)

            Response = MsgBox(Prompt:="Item added Sucessfully, do you want to add another item", Buttons:=vbYesNo)

            ' If statement to check if the yes button was selected.
            If Response = vbYes Then
                txtBarcode.Text = ""
                txtBarcode2.Text = ""
                txtName.Text = ""
                txtManufacture.Text = ""
                txtCost.Text = ""
                txtDesc.Text = ""
                txtPrice.Text = ""
                txtQty.Text = ""
                chkRefurb.CheckState = CheckState.Unchecked
                chkTaxable.CheckState = CheckState.Unchecked

            Else
                ' The no button was selected.
                Me.Close()

            End If
        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub

    Private Sub frmAddProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtCateg.Items.Clear()
        txtCategSub.Items.Clear()
        Dim cate As New CGDsql

        Dim CateArray As New ArrayList
        CateArray = cate.getCategory()

        For count As Integer = 0 To CateArray.Count - 1
            txtCateg.Items.Add(CateArray(count))
        Next

        'If CateArray.Count > 0 Then
        '   txtCateg.SelectedIndex = 0
        'End If

        If lblCategory.Text > 0 Then
            Dim getitemcat As cate
            getitemcat = cate.getItemCat(lblCategory.Text)
            txtCateg.SelectedItem = getitemcat.cateogry
            txtCategSub.SelectedItem = getitemcat.subcategory
            txtCateg.Update()
            txtCategSub.Update()


        End If
    End Sub
    Dim cat_id As Integer
    Dim CateArraysub As ArrayList
    Private Sub txtCateg_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCateg.SelectedIndexChanged
        txtCategSub.Items.Clear()
        Dim cate As New CGDsql
        '
        'Dim CateArraysub As New ArrayList
        'MsgBox(txtCateg.SelectedItem.ToString)
        CateArraysub = cate.getCategorySub(txtCateg.SelectedItem.ToString)

        For count As Integer = 0 To CateArraysub.Count - 1
            Dim cateTit As cate
            cateTit = CateArraysub(count)
            txtCategSub.Items.Add(cateTit.subcategory)
        Next
        If CateArraysub.Count > 0 Then
            Dim cateTit1 As cate

            cateTit1 = CateArraysub(0)
            cat_id = cateTit1.catid
            ' lblCategory.Text = cateTit1.catid
            txtCategSub.SelectedIndex = 0

        End If


    End Sub

    Private Sub txtCategSub_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCategSub.SelectedIndexChanged
        Dim catSubID As cate
        catSubID = CateArraysub.Item(txtCategSub.SelectedIndex)
        'catSubID = cat_id + txtCategSub.SelectedIndex
        lblCategory.Text = catSubID.catid
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        Dim additem As New ProductItem
        Dim cgdslq As New CGDsql
        Try
            If txtBarcode.Text = "" Then
                txtBarcode.Text = "NoBarcode"
            End If
            additem.ProductId = lblProdID.Text
            additem.BarCode = txtBarcode.Text
            additem.SecondBarCode = txtBarcode2.Text
            additem.CostPrice = txtCost.Text

            additem.Price = txtPrice.Text
            additem.ProductName = txtName.Text
            additem.StockQty = txtQty.Text
            additem.Taxable = chkTaxable.CheckState
            additem.Refurb = chkRefurb.CheckState
            additem.ManufactureName = txtManufacture.Text
            additem.Description = txtDesc.Text
            additem.Category = lblCategory.Text
            additem.eof = txteof.Text
            additem.remark = cmbRemark.Text


            ' MsgBox(txtCateg.Items.Item)

            ' Displays a message box with the yes and no options.
            'MsgBox(additem.Taxable)




            cgdslq.updateProduct(additem)

            Me.Close()

        Catch InvalidCast As InvalidCastException
            MsgBox("Please enter a valid information")
        End Try
    End Sub
End Class