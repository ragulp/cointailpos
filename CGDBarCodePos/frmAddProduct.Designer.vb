﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddProduct
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.lblProductName = New System.Windows.Forms.Label
        Me.lblProductBarcode = New System.Windows.Forms.Label
        Me.lblPrice = New System.Windows.Forms.Label
        Me.lblQty = New System.Windows.Forms.Label
        Me.lblTaxable = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtBarcode = New System.Windows.Forms.TextBox
        Me.txtPrice = New System.Windows.Forms.TextBox
        Me.txtQty = New System.Windows.Forms.TextBox
        Me.chkTaxable = New System.Windows.Forms.CheckBox
        Me.lblTitle = New System.Windows.Forms.Label
        Me.chkRefurb = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtBarcode2 = New System.Windows.Forms.TextBox
        Me.lblBarcode2 = New System.Windows.Forms.Label
        Me.txtManufacture = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtCost = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtDesc = New System.Windows.Forms.RichTextBox
        Me.txtCateg = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txteof = New System.Windows.Forms.TextBox
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtCategSub = New System.Windows.Forms.ComboBox
        Me.lblCategory = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.cmbRemark = New System.Windows.Forms.ComboBox
        Me.lblProdID = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(255, 546)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(112, 46)
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(123, 546)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(112, 46)
        Me.btnAdd.TabIndex = 13
        Me.btnAdd.Text = "Add Item"
        Me.btnAdd.UseVisualStyleBackColor = True
        Me.btnAdd.Visible = False
        '
        'lblProductName
        '
        Me.lblProductName.AutoSize = True
        Me.lblProductName.Location = New System.Drawing.Point(19, 83)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.Size = New System.Drawing.Size(58, 13)
        Me.lblProductName.TabIndex = 0
        Me.lblProductName.Text = "Item Name"
        '
        'lblProductBarcode
        '
        Me.lblProductBarcode.AutoSize = True
        Me.lblProductBarcode.Location = New System.Drawing.Point(19, 140)
        Me.lblProductBarcode.Name = "lblProductBarcode"
        Me.lblProductBarcode.Size = New System.Drawing.Size(73, 13)
        Me.lblProductBarcode.TabIndex = 0
        Me.lblProductBarcode.Text = "Item Barcode "
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.Location = New System.Drawing.Point(19, 195)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(54, 13)
        Me.lblPrice.TabIndex = 0
        Me.lblPrice.Text = "Item Price"
        '
        'lblQty
        '
        Me.lblQty.AutoSize = True
        Me.lblQty.Location = New System.Drawing.Point(19, 243)
        Me.lblQty.Name = "lblQty"
        Me.lblQty.Size = New System.Drawing.Size(90, 13)
        Me.lblQty.TabIndex = 0
        Me.lblQty.Text = "Quantity on Hand"
        '
        'lblTaxable
        '
        Me.lblTaxable.AutoSize = True
        Me.lblTaxable.Location = New System.Drawing.Point(19, 501)
        Me.lblTaxable.Name = "lblTaxable"
        Me.lblTaxable.Size = New System.Drawing.Size(68, 13)
        Me.lblTaxable.TabIndex = 0
        Me.lblTaxable.Text = "Taxable Item"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(181, 76)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(157, 20)
        Me.txtName.TabIndex = 1
        '
        'txtBarcode
        '
        Me.txtBarcode.Location = New System.Drawing.Point(181, 133)
        Me.txtBarcode.Name = "txtBarcode"
        Me.txtBarcode.Size = New System.Drawing.Size(157, 20)
        Me.txtBarcode.TabIndex = 3
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(181, 188)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(157, 20)
        Me.txtPrice.TabIndex = 5
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(181, 240)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(157, 20)
        Me.txtQty.TabIndex = 7
        '
        'chkTaxable
        '
        Me.chkTaxable.AutoSize = True
        Me.chkTaxable.Location = New System.Drawing.Point(181, 500)
        Me.chkTaxable.Name = "chkTaxable"
        Me.chkTaxable.Size = New System.Drawing.Size(15, 14)
        Me.chkTaxable.TabIndex = 11
        Me.chkTaxable.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(94, 27)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(177, 20)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "Enter An item to Add"
        '
        'chkRefurb
        '
        Me.chkRefurb.AutoSize = True
        Me.chkRefurb.Location = New System.Drawing.Point(181, 524)
        Me.chkRefurb.Name = "chkRefurb"
        Me.chkRefurb.Size = New System.Drawing.Size(15, 14)
        Me.chkRefurb.TabIndex = 12
        Me.chkRefurb.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 525)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Special Item"
        '
        'txtBarcode2
        '
        Me.txtBarcode2.Location = New System.Drawing.Point(181, 162)
        Me.txtBarcode2.Name = "txtBarcode2"
        Me.txtBarcode2.Size = New System.Drawing.Size(157, 20)
        Me.txtBarcode2.TabIndex = 4
        '
        'lblBarcode2
        '
        Me.lblBarcode2.AutoSize = True
        Me.lblBarcode2.Location = New System.Drawing.Point(19, 169)
        Me.lblBarcode2.Name = "lblBarcode2"
        Me.lblBarcode2.Size = New System.Drawing.Size(69, 13)
        Me.lblBarcode2.TabIndex = 0
        Me.lblBarcode2.Text = "Special Price"
        '
        'txtManufacture
        '
        Me.txtManufacture.Location = New System.Drawing.Point(181, 102)
        Me.txtManufacture.Name = "txtManufacture"
        Me.txtManufacture.Size = New System.Drawing.Size(157, 20)
        Me.txtManufacture.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 109)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Manufacture Name"
        '
        'txtCost
        '
        Me.txtCost.Location = New System.Drawing.Point(181, 214)
        Me.txtCost.Name = "txtCost"
        Me.txtCost.Size = New System.Drawing.Size(157, 20)
        Me.txtCost.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 221)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Cost Price"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 294)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Description"
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(181, 269)
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(156, 86)
        Me.txtDesc.TabIndex = 8
        Me.txtDesc.Text = ""
        '
        'txtCateg
        '
        Me.txtCateg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtCateg.FormattingEnabled = True
        Me.txtCateg.Location = New System.Drawing.Point(181, 364)
        Me.txtCateg.Name = "txtCateg"
        Me.txtCateg.Size = New System.Drawing.Size(156, 21)
        Me.txtCateg.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(19, 372)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Category"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(19, 439)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "EnviFee"
        '
        'txteof
        '
        Me.txteof.Location = New System.Drawing.Point(180, 432)
        Me.txteof.Name = "txteof"
        Me.txteof.Size = New System.Drawing.Size(157, 20)
        Me.txteof.TabIndex = 10
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(112, 546)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(112, 46)
        Me.btnUpdate.TabIndex = 15
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        Me.btnUpdate.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(18, 401)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Sub-Category"
        '
        'txtCategSub
        '
        Me.txtCategSub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.txtCategSub.FormattingEnabled = True
        Me.txtCategSub.Items.AddRange(New Object() {"Cables", "Chargers", "CPUs", "Headsets", "Input Devices", "Laptop", "Lights", "Media", "Memory", "Monitors", "Mother Boards", "Networking", "Printers", "Refurb Computers", "Software", "Speakers", "Surveillance", "Tablets", "Toners Ink"})
        Me.txtCategSub.Location = New System.Drawing.Point(180, 393)
        Me.txtCategSub.Name = "txtCategSub"
        Me.txtCategSub.Size = New System.Drawing.Size(156, 21)
        Me.txtCategSub.TabIndex = 17
        '
        'lblCategory
        '
        Me.lblCategory.AutoSize = True
        Me.lblCategory.Location = New System.Drawing.Point(135, 401)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(19, 13)
        Me.lblCategory.TabIndex = 18
        Me.lblCategory.Text = "68"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(20, 465)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Remark"
        '
        'cmbRemark
        '
        Me.cmbRemark.FormattingEnabled = True
        Me.cmbRemark.Items.AddRange(New Object() {"No Warranty, No Exchange", "90 Days Store Warranty", "1Yr Manuf. Warranty", "7 Days Exchange Only", "1Yr Store Warranty"})
        Me.cmbRemark.Location = New System.Drawing.Point(180, 460)
        Me.cmbRemark.Name = "cmbRemark"
        Me.cmbRemark.Size = New System.Drawing.Size(162, 21)
        Me.cmbRemark.TabIndex = 21
        '
        'lblProdID
        '
        Me.lblProdID.AutoSize = True
        Me.lblProdID.Location = New System.Drawing.Point(344, 76)
        Me.lblProdID.Name = "lblProdID"
        Me.lblProdID.Size = New System.Drawing.Size(13, 13)
        Me.lblProdID.TabIndex = 22
        Me.lblProdID.Text = "0"
        '
        'frmAddProduct
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(429, 619)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblProdID)
        Me.Controls.Add(Me.cmbRemark)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblCategory)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtCategSub)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.txteof)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtCateg)
        Me.Controls.Add(Me.txtDesc)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtCost)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtManufacture)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtBarcode2)
        Me.Controls.Add(Me.lblBarcode2)
        Me.Controls.Add(Me.chkRefurb)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.chkTaxable)
        Me.Controls.Add(Me.txtQty)
        Me.Controls.Add(Me.txtPrice)
        Me.Controls.Add(Me.txtBarcode)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.lblTaxable)
        Me.Controls.Add(Me.lblQty)
        Me.Controls.Add(Me.lblPrice)
        Me.Controls.Add(Me.lblProductBarcode)
        Me.Controls.Add(Me.lblProductName)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnCancel)
        Me.Name = "frmAddProduct"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents lblProductName As System.Windows.Forms.Label
    Friend WithEvents lblProductBarcode As System.Windows.Forms.Label
    Friend WithEvents lblPrice As System.Windows.Forms.Label
    Friend WithEvents lblQty As System.Windows.Forms.Label
    Friend WithEvents lblTaxable As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtBarcode As System.Windows.Forms.TextBox
    Friend WithEvents txtPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents chkTaxable As System.Windows.Forms.CheckBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents chkRefurb As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtBarcode2 As System.Windows.Forms.TextBox
    Friend WithEvents lblBarcode2 As System.Windows.Forms.Label
    Friend WithEvents txtManufacture As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCost As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDesc As System.Windows.Forms.RichTextBox
    Friend WithEvents txtCateg As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txteof As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents txtCategSub As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbRemark As System.Windows.Forms.ComboBox
    Friend WithEvents lblProdID As System.Windows.Forms.Label
End Class
